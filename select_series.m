function select_series(varargin)
    handles             = guihandles;
    TPlan               = getappdata(handles.amb_interface,'TPlan');
    TPlan.image.Image   = TPlan.image.(sprintf(['Image_' num2str(varargin{1}.Value)]));
    TPlan.image.Nvoxels = size(TPlan.image.Image);
%  
    setappdata(handles.amb_interface,'TPlan',TPlan);
%
    ax=getappdata(handles.amb_interface,'ax');
%
%
    handles.amb_vis_02.XLim=[1 TPlan.image.Nvoxels(3)];
    handles.amb_dos_02.XLim=[1 TPlan.image.Nvoxels(3)];
    handles.amb_vis_03.XLim=[1 TPlan.image.Nvoxels(3)];
    handles.amb_dos_03.XLim=[1 TPlan.image.Nvoxels(3)];
    %
    setappdata(handles.amb_interface,'ax',ax);
    imview_adjust_pos(handles);
end