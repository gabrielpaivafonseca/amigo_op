handles = guihandles;
TPlan=getappdata(handles.amb_interface,'TPlan');

%
Doses             =  fieldnames(TPlan.parameters);
Structures        =  fieldnames(TPlan.parameters.(sprintf(Doses{1})));
Parameters        =  fieldnames(TPlan.parameters.(sprintf(Doses{1})).(sprintf(Structures{1})).DVH);
%
fid=fopen('clini_par.txt','w');

%
fprintf(fid,'%s ',Structures{:}); fprintf(fid,'\n');
fprintf(fid,'%s ',Doses{:});      fprintf(fid,'\n');
%
for j=3:size(Parameters,1)
   fprintf(fid,'%s ',Parameters{j});
   for k=1:size(Structures,1)
      
      for i=1:size(Doses,1)
          fprintf(fid,'%.02f ',TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.(sprintf(Parameters{j})));
      end
      
   end
   %
   fprintf(fid,'\n');
end
fclose(fid);