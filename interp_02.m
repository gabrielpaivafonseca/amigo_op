% get TPS points
P=TPlan.struct.contours.Item_1.Points;
%
% find limits
P(find(P(:,1)==9999),:)=[];
[~,idx] = sort(P(:,3));
P=P(idx,:);
%
Li=find(abs(diff(P(:,3))>0));
Li=[0;Li;size(P,1)];
%
%
N_slices = size(Li,1);
Expected = abs((P(Li(end)-1,3)-P(2,3))/TPlan.image.Resolution(3));
disp(['Available slices ' num2str(N_slices) '  Expected ' num2str(Expected)]);
%
%
%
% go trough the slices looking for gaps
P2=[];
P3=[];
for i=1:size(Li,1)-2
    % is there a gap  ?
    if abs(P(Li(i)+1,3)-P(Li(i+1)+1,3)) ~= TPlan.image.Resolution(3)  
       % go point by point find clossest in the next slice and draw a line
       for j=Li(i)+1:Li(i+1)
         M=[];
         M(:,1)=P(:,1)-P(j,1);
         M(:,2)=P(:,2)-P(j,2); 
         M=sqrt(sum(M.^2,2));
         id=find(M==min(M(Li(i+1)+1:Li(i+2))));
         %
         x=[P(j,1) P(id,1)];
         y=[P(j,2) P(id,2)];
         z=[P(j,3) P(id,3)];
         %
         zq=P(j,3)+TPlan.image.Resolution(3):...
             TPlan.image.Resolution(3):P(Li(i+1)+1,3)-TPlan.image.Resolution(3);
         %
         xq = interp1(z,x,zq);   
         yq = interp1(z,y,zq);
         %
         % plot3(xq,yq,zq,'ro'); drawnow;
         %
         point=horzcat(xq',yq',zq');
         P2=vertcat(P2,point);
       end
       %
       for j=Li(i+1)+1:Li(i+2)
         M=[];
         M(:,1)=P(:,1)-P(j,1);
         M(:,2)=P(:,2)-P(j,2); 
         M=sqrt(sum(M.^2,2));
         id=find(M==min(M(Li(i)+1:Li(i+1))));
         %
         x=[P(id,1) P(j,1)];
         y=[P(id,2) P(j,2)];
         z=[P(id,3) P(j,3)];
         %
         zq=P(j,3)-TPlan.image.Resolution(3):...
             -TPlan.image.Resolution(3):P(Li(i)+1,3)+TPlan.image.Resolution(3);
         %
         xq = interp1(z,x,zq);   
         yq = interp1(z,y,zq);
         %
         % plot3(xq,yq,zq,'go'); drawnow;
         %
         point=horzcat(xq',yq',zq');
         P3=vertcat(P3,point);
       end
       %
    end
end

% concatenate
P_int=vertcat(P,P2);
% sort
[~,idx] = sort(P_int(:,3));
P_int=P_int(idx,:);
% covert to voxels
PointsIm=bsxfun(@minus,P_int,TPlan.image.ImagePositionPatient);
PointsIm=bsxfun(@rdivide,PointsIm,TPlan.image.Resolution);  
%
% create mask
%
% find slices
Li=find(abs(diff(P_int(:,3))>0));
Li=[0;Li;size(P_int,1)];
%
Mask=zeros(TPlan.image.Nvoxels);
for i=1:size(Li,1)-1
     Mask(:,:,round(PointsIm(Li(i)+1,3))+1)=poly2mask(PointsIm(Li(i)+1:Li(i+1),1)+1,PointsIm(Li(i)+1:Li(i+1),2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
end
%
%
P_int=P3;
% sort
[~,idx] = sort(P_int(:,3));
P_int=P_int(idx,:);
% covert to voxels
PointsIm=bsxfun(@minus,P_int,TPlan.image.ImagePositionPatient);
PointsIm=bsxfun(@rdivide,PointsIm,TPlan.image.Resolution);  
%
% find slices
Li=find(abs(diff(P_int(:,3))>0));
Li=[0;Li;size(P_int,1)];
%
for i=1:size(Li,1)-1
     Mask(:,:,round(PointsIm(Li(i)+1,3))+1)=Mask(:,:,round(PointsIm(Li(i)+1,3))+1)+poly2mask(PointsIm(Li(i)+1:Li(i+1),1)+1,PointsIm(Li(i)+1:Li(i+1),2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
end
Mask=logical(Mask);
