function [result] = read_meshtal(varargin)
tic
[file folder] = uigetfile;
fid=fopen([folder file],'r');
f=textscan(fid,'%s',3,'Delimiter','\n');
result.nps=str2num(f{1}{3}(54:end));
%
out=0;
while ~feof(fid)
    f=textscan(fid,'%s',1,'Delimiter','\n');
    tally=strfind(f{1},'Tally Number','ForceCellOutput',true);
    if tally{1}>0
        out=out+1;
        result.(sprintf('r%.0f',out)).tally_number=str2num(f{1}{1}(20:end));
    end
    dim=strfind(f{1},'Tally bin boundaries:','ForceCellOutput',true);
    if dim{1}>0
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).x=str2num(f{1}{1}(17:end));
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).y=str2num(f{1}{1}(17:end));
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).z=str2num(f{1}{1}(17:end));           
    end
    %
    r=strfind(f{1},'Tally Results:','ForceCellOutput',true);
    if r{1}>0
      ori=zeros(3,1);
    %
      if     isempty(strfind(f{1}{1},'X (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).x,2)-1;
         ori(1)=1;
      elseif isempty(strfind(f{1}{1},'Y (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).y,2)-1;
         ori(2)=1;
      elseif isempty(strfind(f{1}{1},'Z (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).z,2)-1;
         ori(3)=1;
      end
     %
      if     isempty(strfind(f{1}{1},'X (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).x,2)-1;
         ori(1)=1;
      elseif isempty(strfind(f{1}{1},'Y (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).y,2)-1;
         ori(2)=1;
      elseif isempty(strfind(f{1}{1},'Z (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).z,2)-1;
         ori(3)=1;
      end
      %
      if     ori(1)==0 
         slices=size(result.(sprintf('r%.0f',out)).x,2)-1;
      elseif ori(2)==0 
         slices=size(result.(sprintf('r%.0f',out)).y,2)-1;
      elseif ori(3)==0
         slices=size(result.(sprintf('r%.0f',out)).z,2)-1;
      end
      f=textscan(fid,'%s',1,'Delimiter','\n'); 
      [result.(sprintf('r%.0f',out)).Result,result.(sprintf('r%.0f',out)).Uncert] ...
                                     = meshtal(fid,l,c,slices);
    end        
end
%
fclose('all');
toc
end


function [tally,uncer] = meshtal(fid,l,c,slices)
%
try
   w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
   set(w_bar.hand,'Visible',1,'Value',10);
catch
   % only works if amigo is open
end

tally=zeros(l,c+1,slices);
uncer=zeros(l,c+1,slices);
for k=1:slices
   tally(:,:,k)=cell2mat(textscan(fid,repmat('%f',[1,c+1]),'CollectOutput',1));
   textscan(fid,'%s',2,'Delimiter','\n');
   uncer(:,:,k)=cell2mat(textscan(fid,repmat('%f',[1,c+1]),'CollectOutput',1));
   if k~=slices
    textscan(fid,'%s',4,'Delimiter','\n');
   end
   try
     set(w_bar.hand,'Value',k/slices*100-5);
   end
end
%
tally(:,1,:)=[];
uncer(:,1,:)=[];
try
  set(w_bar.hand,'Visible',0,'Value',0);
end
end