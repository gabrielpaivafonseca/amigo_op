function [Dose] = calc_dose_nist(Coef_NIST,Spectrum)

% Normalize to 1
Spectrum(:,2) = Spectrum(:,2)/sum(Spectrum(:,2));
%
Dose.H2O=0;
Dose.Mat=0;
for i=1:size(Spectrum,1)
      % interpolate and add dose
      D=abs(Coef_NIST.Coef(:,1)-Spectrum(i,1));
      idx=find(D==min(D),1);
      %
      if D(idx)==0
          Dose.Mat=Dose.Mat + Coef_NIST.Coef(idx,3)*Spectrum(i,1)*Spectrum(i,2);
          Dose.H2O=Dose.H2O + Coef_NIST.H2O(idx,3)*Spectrum(i,1)*Spectrum(i,2);
      else 
          % need to interpolate
          %
          % find the segment 
          if Coef_NIST.Coef(idx,1)>Spectrum(i)
              Id = idx-1;
          else
              Id = idx;
          end
          %
          % Log-Log Interpolation material
          m(1)      = (log10(Coef_NIST.Coef(Id,3)/Coef_NIST.Coef(Id+1,3))) ...
                      /(log10(Coef_NIST.Coef(Id,1)/Coef_NIST.Coef(Id+1,1)));
          Const(1)  = Coef_NIST.Coef(Id,3)/Coef_NIST.Coef(Id,1)^m(1);
          %
          Coef_NIST.Coef_int  =  Const(1)*Spectrum(i)^m(1);
          %
          Dose.Mat=Dose.Mat + Coef_NIST.Coef_int*Spectrum(i,1)*Spectrum(i,2);
          %
          % Log-Log Interpolation H2O
          m(1)      = (log10(Coef_NIST.H2O(Id,3)/Coef_NIST.H2O(Id+1,3))) ...
                      /(log10(Coef_NIST.H2O(Id,1)/Coef_NIST.H2O(Id+1,1)));
          Const(1)  = Coef_NIST.H2O(Id,3)/Coef_NIST.H2O(Id,1)^m(1);
          %
          Coef_NIST.Coef_int  =  Const(1)*Spectrum(i)^m(1);
          %
          Dose.H2O=Dose.H2O + Coef_NIST.Coef_int*Spectrum(i,1)*Spectrum(i,2);
      end
end
disp(['Dwater = ' num2str(Dose.H2O) '      Dmed = ' num2str(Dose.Mat) '      Ratio (Dmed/Dwat = ' num2str(Dose.Mat/Dose.H2O)]);
end