function [ ] = cat_update(varargin)
% addNewPositionCallback(h(1),@(p) assignin('base','xy',p));
TPlan=getappdata(gcf,'TPlan');
cath=getappdata(gcf,'Catheters');
%
%
if getappdata(gcf,'Cat_plot')==1
    % otherwise it will go to an ifinity loop ... ax uptades 2 that updates
    % 1 ....
    setappdata(gcf,'Cat_plot',2); %#ok<*NASGU>
    try
    f=gco; ref=str2num(f.Parent.Tag);
    %
    %
    Points=getPosition(cath(ref(3)));
    if      size(Points,1) > size(TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd,1);
               TPlan=add_new_point(TPlan,Points,ref(1),ref(2));
    elseif  size(Points,1) < size(TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,2]),1);
               TPlan=del_point(TPlan,Points,ref(1),ref(2));
    end
    %
    if     ref(2)==1
        TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,1:2)=varargin{:}; 
        setPosition(cath(ref(3)+1),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,2]));
        setPosition(cath(ref(3)+2),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,1]));
        drawnow;
    elseif ref(2)==2
         TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,2])=varargin{:}; 
         setPosition(cath(ref(3)-1),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[1,2]));
         setPosition(cath(ref(3)+1),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,1]));
         drawnow;
    elseif ref(2)==3
        TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,1])=varargin{:}; 
        setPosition(cath(ref(3)-2),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[1,2]));
        setPosition(cath(ref(3)-1),TPlan.plan.catheter.(sprintf('Cat_%.0f',ref(1))).PointsEd(:,[3,2]));
        drawnow;
    end  
    catch  
      disp('error updating catheter position cat_update.m - click over it to update');  
    end
    setappdata(gcf,'Cat_plot',1);
end   
setappdata(gcf,'TPlan',TPlan);
end




%
%
function [TPlan] = add_new_point(TPlan,Points,cat,ax)
%
current_slices =  getappdata(gcf,'slices');
size(Points,1)
PointsEd=TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd;
id=1;
for i=1:size(Points,1)
  if     ax == 1
      idx=find(PointsEd(:,1)==Points(i,1) & PointsEd(:,2)==Points(i,2),1);
      if isempty(idx)==1
         P=[];
         P(1:id,1:3)=PointsEd(1:id,1:3);
         %
         P(id+1,3)  =current_slices(1);
         P(id+1,1)  =Points(i,1); %#ok<*AGROW>
         P(id+1,2)  =Points(i,2);
         %
         P(id+2:size(PointsEd,1)+1,1:3)=PointsEd(id+1:end,1:3);
      else
          id=idx;
      end
  elseif ax == 2
      idx=find(PointsEd(:,2)==Points(i,2) & PointsEd(:,3)==Points(i,1),1);
      if isempty(idx)==1
         P=[];
         P(1:id,1:3)=PointsEd(1:id,1:3);
         %
         P(id+1,1)  =current_slices(2);
         P(id+1,2)  =Points(i,2); 
         P(id+1,3)  =Points(i,1);
         %
         P(id+2:size(PointsEd,1)+1,1:3)=PointsEd(id+1:end,1:3);
      else
          id=idx;
      end
  elseif ax == 3
      idx=find(PointsEd(:,1)==Points(i,2) & PointsEd(:,3)==Points(i,1),1);
      if isempty(idx)==1
         P=[];
         P(1:id,1:3)=PointsEd(1:id,1:3);
         %
         P(id+1,2)  =current_slices(3);
         P(id+1,1)  =Points(i,2); 
         P(id+1,3)  =Points(i,1);
         %
         P(id+2:size(PointsEd,1)+1,1:3)=PointsEd(id+1:end,1:3);
      else
          id=idx;
      end      
  end
end
% 
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd=P;
setappdata(gcf,'TPlan',TPlan);
end







%
%
function [TPlan] = del_point(TPlan,Points,cat,ax)
%
PointsEd=TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd;
%
for i=1:size(PointsEd,1)
  if  ax == 1
      idx=find(PointsEd(i,1)==Points(:,1) & PointsEd(i,2)==Points(:,2),1);
      if isempty(idx)==1
         break;
      end
  elseif ax == 2
      idx=find(PointsEd(:,2)==Points(i,2) & PointsEd(:,3)==Points(i,1),1);
      if isempty(idx)==1
         break;
      end
  elseif ax == 3
      idx=find(PointsEd(:,1)==Points(i,2) & PointsEd(:,3)==Points(i,1),1);
      if isempty(idx)==1
         break;
      end    
  end
end
% 
PointsEd(i,:)=[];
%
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd=[];
TPlan.plan.catheter.(sprintf('Cat_%.0f',cat)).PointsEd=PointsEd;
setappdata(gcf,'TPlan',TPlan);
end





