function [ ] = brush_amb(varargin)
%z
global draw_countour
g     = varargin{3};
ax    = varargin{4};
fig   = varargin{5};
%l     = varargin{6};
C     = round(get(ax,'CurrentPoint'));
s     = round(draw_countour.size/2);
id    = draw_countour.id;
%
h     = getappdata(fig,'ax');
l(1,1:2)=h.dos01.Parent.XLim;
l(2,1:2)=h.dos01.Parent.YLim;
%tic
%
%
if C(1,2)>=l(2,1) && C(1,2)<=l(2,2) && C(1,1)>=l(1,1) && C(1,1)<=l(1,2)
  set(fig, 'PointerShapeCData', ones(16, 16)*nan);
  set(fig, 'Pointer', 'custom');
  g(1).Visible='on';
  g(2).Visible='on';
  if draw_countour.on ==1
      TPlan=getappdata(fig,'TPlan');
      sli= getappdata(fig,'slices');
      if C(1,1)-s<l(1,1); C(1,1)=l(1,1)+s; end 
      if C(1,1)+s>l(1,2); C(1,1)=l(1,2)-s; end 
      if C(1,2)-s<l(2,1); C(1,2)=l(2,1)+s; end 
      if C(1,2)+s>l(2,2); C(1,2)=l(2,2)-s; end 
      % TPlan.struct.Restore=TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask;
      C=round(C);
      ref=double(TPlan.image.Image(C(1,2),C(1),sli(1)));
      r  =getappdata(fig,'brush_range')/100;
      range=[ref-abs(ref*r) ref+abs(ref*r)];
      Im=TPlan.image.Image(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1));
      Im(Im<=range(1) | Im>=range(2))=0; Im=logical(Im);
      if draw_countour.subtract==0
          TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))=TPlan.struct.contours.Item_3.Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1)) ...
                                                    + Im;
      else
          a=TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))-Im;
          a(a<0)=0; a=logical(a);
          TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(C(1,2)-s:C(1,2)+s,C(1)-s:C(1)+s,sli(1))=a;
      end
      TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask( TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask<0)=0;
      TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask=logical( TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask);
      %
     % drawnow;
     % h.fig01_contour.CData(:,:,1)=.5*double(TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(:,:,sli(1)));
     % h.fig01_contour.CData(:,:,2)=.3*double(TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(:,:,sli(1)));
     % h.fig01_contour.CData(:,:,3)=.2*double(TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(:,:,sli(1)));
      %
     % drawnow
      setappdata(fig,'TPlan',TPlan); 
      update_contours(1,TPlan,sli,getappdata(fig,'ax'),getappdata(fig,'trans_contour'));
  end
    g(1).Vertices(1,1)  = C(1)    -s;    g(1).Vertices(2:3,1) = C(1)   +s;   g(1).Vertices(4,1)   = C(1)    -s;
    g(1).Vertices(1,2)  = C(1,2)  -s;    g(1).Vertices(2,2)   = C(1,2) -s;   g(1).Vertices(3:4,2) = C(1,2)  +s;
    %
    g(2).Vertices(1,1)  = C(1)    -1;    g(2).Vertices(2:3,1) = C(1)   +1;   g(2).Vertices(4,1)   = C(1)    -1;
    g(2).Vertices(1,2)  = C(1,2)  -1;    g(2).Vertices(2,2)   = C(1,2) -1;   g(2).Vertices(3:4,2) = C(1,2)  +1;
else
   g(1).Visible='off';
   g(2).Visible='off';
   set(fig, 'Pointer', 'default');
end
%toc
end

