function [  ] = find_body()
fig_r          =  findobj('Tag','amb_interface');
TPlan          =  getappdata(fig_r,'TPlan');
handles        = guihandles; 
%
set(fig_r,'pointer','watch');      % Busy indication   
%
answer=inputdlg('Minimum HU','Struct',1,{'-300'});
%
if isempty(answer)==1
    return;
end
%
BW=logical(TPlan.image.Image);  
BW(TPlan.image.Image<str2num(answer{1}))=0;
%
for i=1:size(BW,3)
   BW(:,:,i)=imfill(BW(:,:,i),'holes');
end
%
CC = bwconncomp(BW);
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
BW=logical(zeros(size(BW)));
BW(CC.PixelIdxList{idx}) = 1;
%
id  = handles.cont_pop_menu.Value;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask=BW;
%
%
set(fig_r,'pointer','default');
setappdata(fig_r,'TPlan',TPlan);
end
