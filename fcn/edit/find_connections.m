function [  ] = find_connections()
fig_r          =  findobj('Tag','amb_interface');
f              =  getappdata(fig_r,'ax');  
TPlan          =  getappdata(fig_r,'TPlan');
handles        =  guihandles(fig_r);
%
set(fig_r,'pointer','watch');      % Busy indication   
current_slices =  double(getappdata(fig_r,'slices'));
%
% set mouse to default in case draw cont. is active
% 
h=impoint(f.dos01.Parent);    
p=round(getPosition(h));    
p(3)=current_slices(1);                             
delete(h); 
%
if strcmp(handles.view_DCM.Checked,'on')==1 % Image
    answer=inputdlg('Range (HU) of the reference value','Struct',1,{'200'});
    %
    l(1)=double(TPlan.image.Image(p(2),p(1),p(3)))-str2num(answer{1});
    l(2)=double(TPlan.image.Image(p(2),p(1),p(3)))+str2num(answer{1});
    %
    BW=logical(TPlan.image.Image);  
    BW(TPlan.image.Image<l(1) | TPlan.image.Image>l(2))=0; 
    %
    CC = bwconncomp(BW,6);
    %
    V_ID = sub2ind(size(BW), p(2), p(1), p(3));
    St=[];
    for i=1:size(CC.PixelIdxList,2)
       if find(CC.PixelIdxList{i}==V_ID,1)>0
           St=CC.PixelIdxList{i}(:);
           break;
       end
    end
else % material or density map - no range
    BW=logical(TPlan.plan.Mat_HU);  
    BW(TPlan.plan.Mat_HU~= TPlan.plan.Mat_HU(p(2),p(1),p(3)))=0; 
    %
    CC = bwconncomp(BW,6);
    %
    V_ID = sub2ind(size(BW), p(2), p(1), p(3));
    St=[];
    for i=1:size(CC.PixelIdxList,2)
       if find(CC.PixelIdxList{i}==V_ID,1)>0
           St=CC.PixelIdxList{i}(:);
           break;
       end
    end 
end
%
id  = handles.cont_pop_menu.Value;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask(St)=1;
%
set(fig_r,'pointer','default');
setappdata(fig_r,'TPlan',TPlan);
end

