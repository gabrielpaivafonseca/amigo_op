function [] = interpolate_contour(varargin)
% receives a logical matrix and interpolate the slices 
%
handles = guihandles; 
TPlan=getappdata(handles.amb_interface,'TPlan');
%%
% contour_int=TPlan.struct.contours.(sprintf('Item_%.0f',handles.cont_pop_menu.Value)).Mask;
%
% v=[]; x=[];
% for i=1:TPlan.image.Nvoxels(3)
%     if isempty(find(contour_int(:,:,i)>0, 1))==0
%         v(end+1)=i; %#ok<*AGROW>
%         if isempty(x)==1
%             x=double(contour_int(:,:,i));
%         else
%             x=cat(3,x,double(contour_int(:,:,i)));
%         end
%     end
% end
% %
% %
% h = ones(30,30) /25;
% %
% x=permute(x,[3 1 2]);
% for i=1:TPlan.image.Nvoxels(3)
%     if isempty(find(contour_int(:,:,i)>0, 1))==1 && i>min(v) && i<max(v)
%        Im=squeeze((interp1(v,x,i)))*1000;
%        Im=imfilter(Im,h);
%        Im=Im/max(Im(:));
%        Im(Im<.5)=0;
%        contour_int(:,:,i)=Im;
%        % find index
%        IDX=find((v-i)>0,1);
%        v(IDX+1:end+1)=v(IDX:end);
%        v(IDX)=i;
%        %
%        x(IDX+1:end+1,:,:)= x(IDX:end,:,:);
%        x(IDX,:,:)        = permute(contour_int(:,:,i),[3 1 2]);
%        %
%     end
% end
% %%
% contour_int(contour_int<1)=0;
% contour_int=logical(contour_int);
%

% get TPS points
P=TPlan.struct.contours.(sprintf('Item_%.0f',handles.cont_pop_menu.Value)).Points;
Mask=TPlan.struct.contours.(sprintf('Item_%.0f',handles.cont_pop_menu.Value)).Mask;
%
% find limits
P(find(P(:,1)==9999),:)=[];
[~,idx] = sort(P(:,3));
P=P(idx,:);
%
Li=find(abs(diff(P(:,3))>0));
Li=[0;Li;size(P,1)];
%
%
N_slices = size(Li,1)-2;
Expected = abs((P(Li(end),3)-P(1,3))/TPlan.image.Resolution(3));
disp(['Available slices ' num2str(N_slices) '  Expected ' num2str(Expected)]);
%
%
%
% go trough the slices looking for gaps
P2=[];
P3=[];
for i=1:size(Li,1)-2
    % is there a gap  ?
    if abs(P(Li(i)+1,3)-P(Li(i+1)+1,3)) ~= TPlan.image.Resolution(3)  
       % go point by point find clossest in the next slice and draw a line
       for j=Li(i)+1:Li(i+1)
         M=[];
         M(:,1)=P(:,1)-P(j,1);
         M(:,2)=P(:,2)-P(j,2); 
         M=sqrt(sum(M.^2,2));
         id=find(M==min(M(Li(i+1)+1:Li(i+2))));
         %
         x=[P(j,1) P(id(1),1)];
         y=[P(j,2) P(id(1),2)];
         z=[P(j,3) P(id(1),3)];
         %
         zq=P(j,3)+TPlan.image.Resolution(3):...
             TPlan.image.Resolution(3):P(Li(i+1)+1,3)-TPlan.image.Resolution(3);
         %
         if diff(z)~=0 %&& diff(x)~=0
             xq = interp1(z,x,zq);   
             yq = interp1(z,y,zq);
             %
%              plot3(x,y,z,'bo'); drawnow;
%              plot3(xq,yq,zq,'ro'); drawnow;
             %
             point=horzcat(xq',yq',zq');
             P2=vertcat(P2,point);
         end
       end
       %
       for j=Li(i+1)+1:Li(i+2)
         M=[];
         M(:,1)=P(:,1)-P(j,1);
         M(:,2)=P(:,2)-P(j,2); 
         M=sqrt(sum(M.^2,2));
         id=find(M==min(M(Li(i)+1:Li(i+1))));
         %
         x=[P(id(1),1) P(j,1)];
         y=[P(id(1),2) P(j,2)];
         z=[P(id(1),3) P(j,3)];
         %
         zq=P(j,3)-TPlan.image.Resolution(3):...
             -TPlan.image.Resolution(3):P(Li(i)+1,3)+TPlan.image.Resolution(3);
         %
         if diff(z)~=0 && diff(x)~=0
             xq = interp1(z,x,zq);   
             yq = interp1(z,y,zq);
             %
%              plot3(xq,yq,zq,'go'); drawnow;
             %
             point=horzcat(xq',yq',zq');
             P3=vertcat(P3,point);
         end
       end
       %
    end
end

% concatenate
if isempty(P2)==1; disp('No gaps found'); return; end
%
P_int=P2;
% sort
[~,idx] = sort(P_int(:,3));
P_int=P_int(idx,:);
% create mask
%
P_int(isnan(P_int(:,1))==1,:)=[];
P_int(isnan(P_int(:,2))==1,:)=[];
% find slices
Li=find(abs(diff(P_int(:,3))>0));
Li=[0;Li;size(P_int,1)];
%
% covert to voxels
PointsIm=bsxfun(@minus,P_int,TPlan.image.ImagePositionPatient);
PointsIm=bsxfun(@rdivide,PointsIm,TPlan.image.Resolution);  
%
%
%Mask=zeros(TPlan.image.Nvoxels);
for i=1:size(Li,1)-2
     d=sqrt(sum((PointsIm(Li(i)+1:Li(i+1),1:2).^2),2));
   %  Mask(:,:,round(PointsIm(Li(i)+1,3))+1)=Mask(:,:,round(PointsIm(Li(i)+1,3))+1)+poly2mask(PointsIm(Li(i)+1:Li(i+1),1)+1,PointsIm(Li(i)+1:Li(i+1),2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
     Mask(:,:,round(PointsIm(Li(i)+1,3))+1)=poly2mask(PointsIm(Li(i)+1:Li(i+1),1)+1,PointsIm(Li(i)+1:Li(i+1),2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));

end
%
% concatenate
P_int=P3;
% sort
[~,idx] = sort(P_int(:,3));
P_int=P_int(idx,:);
% covert to voxels
PointsIm=bsxfun(@minus,P_int,TPlan.image.ImagePositionPatient);
PointsIm=bsxfun(@rdivide,PointsIm,TPlan.image.Resolution);  
%
% create mask
%
% find slices
Li=find(abs(diff(P_int(:,3))>0));
Li=[0;Li;size(P_int,1)];
%
for i=1:size(Li,1)-1
     Mask(:,:,round(PointsIm(Li(i)+1,3))+1)=poly2mask(PointsIm(Li(i)+1:Li(i+1),1)+1,PointsIm(Li(i)+1:Li(i+1),2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
end
%

for i=1:size(Mask,3)
    M=zeros(TPlan.image.Nvoxels(1:2));
    CC = bwconncomp(Mask(:,:,i),4);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    id=find(numPixels>5);
    for j=1:size(id,2)
       M(CC.PixelIdxList{ind2sub(CC.ImageSize,id(j))})=1;
    end
    Mask(:,:,i)=logical(M);
end


id=length(fieldnames(TPlan.struct.contours))+1;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Mask=Mask;
TPlan.struct.contours.(sprintf('Item_%.0f',id)).Name=[TPlan.struct.contours.(sprintf('Item_%.0f',handles.cont_pop_menu.Value)).Name '_Int'];
setappdata(handles.amb_interface,'TPlan',TPlan);
%
end