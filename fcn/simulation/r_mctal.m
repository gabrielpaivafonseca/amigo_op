function [Pred_images] = r_mctal(varargin)
%
%
if nargin==0
  [~,PathName]=uigetfile('*.*');
else
  PathName    = varargin{1};
  Pred_images = varargin{2};
end
%
% Try to recover partial data or dwell pos info
p=strfind(PathName,filesep);
if exist([PathName(1:p(end-1)) PathName(p(end-1)+1:p(end)-1) '.mat'],'file')>0
   load([PathName(1:p(end-1)) PathName(p(end-1)+1:p(end)-1) '.mat']);           % partial results      
elseif exist([PathName filesep 'Prediction.mat'],'file')>0
   load([PathName filesep 'Prediction.mat']);                                   % information about dwell pos if partial results are not available 
end


list=dir([PathName '*.mct']);
%
if size(list,1)==0
    return;
end
%
if isstruct(Pred_images)==0 
   Pred_images=struct;
end

tic
for i=1:size(list,1)
  %  list(i).name
    fid=fopen([PathName  list(i).name],'r');
    Res=[]; fgetl(fid);  fgetl(fid);
    w=[]; w=strtrim(list(i).name(3:end)); w(end-3:end)=[];
    if isfield(Pred_images,'no_scat')==0 || isfield(Pred_images.no_scat,['i' w])==0
        %
        while ~feof(fid)
             a=fgetl(fid);    
             if strncmp(a,'s',1)==1
                   l1=str2num(a(2:end));
                   b=str2num(fgetl(fid));
                   Ref(1,1)=b(1); Ref(1,2)=b(2)-b(1);
             elseif strncmp(a,'c',1)==1
                   l2=str2num(a(2:end));
                   c=fgetl(fid);
                   b=str2num(fgetl(fid));
                   Ref(2,1)=b(1); Ref(2,2)=b(2)-b(1);
             end
    %               
             if size(a,2)>=4 && strncmp(a,'vals',4)==1
               % textscan is faster !
                Res=cell2mat(textscan(fid,repmat('%f',[1,8]),'CollectOutput',1));
                break;
             end
        end
        fclose('all');
        Res(:,[2,4,6,8])=[];
        Res=reshape(Res',[],1);
        Pred_images.no_scat.(sprintf(['i' w])).image_1=reshape(Res(1:l1*l2),l1,l2);         
        Pred_images.no_scat.(sprintf(['i' w])).image_2=reshape(Res(l1*l2+1:l1*l2*2),l1,l2);
        %
        % convert to int16 --- to save memory
        Pred_images.no_scat.(sprintf(['i' w])).conv_1=min(Pred_images.no_scat.(sprintf(['i' w])).image_1(:)/100);
        Pred_images.no_scat.(sprintf(['i' w])).conv_2=min(Pred_images.no_scat.(sprintf(['i' w])).image_2(:)/100);
        Pred_images.no_scat.(sprintf(['i' w])).image_1=int16(Pred_images.no_scat.(sprintf(['i' w])).image_1/Pred_images.no_scat.(sprintf(['i' w])).conv_1);
        Pred_images.no_scat.(sprintf(['i' w])).image_2=int16(Pred_images.no_scat.(sprintf(['i' w])).image_2/Pred_images.no_scat.(sprintf(['i' w])).conv_2);
        %
    else
       fclose('all'); 
    end
    %
end
toc
fclose('all');
Pred_images.no_scat=orderfields(Pred_images.no_scat);
save([PathName(1:p(end-1)) PathName(p(end-1)+1:p(end)-1) '.mat'],'Pred_images');
end
% 



