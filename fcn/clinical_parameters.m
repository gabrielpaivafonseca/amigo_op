function [clin_par] = clinical_parameters()
% List contours available
%
handles = guihandles; TPlan=getappdata(handles.amb_interface,'TPlan');
Vol  = TPlan.image.Resolution(1)*TPlan.image.Resolution(2)*TPlan.image.Resolution(3)/1000; %cc
%
str=[];
for i=1:length(fieldnames(TPlan.struct.contours))
    str{end+1}=TPlan.struct.contours.(sprintf('Item_%d',i)).Name;
end
%
[s,~] = listdlg('PromptString','Select contours of interest:',...
                'SelectionMode','multiple',...
                'ListString',str);
%
if isempty(s)==1
    clin_par=[];
    return;
end
leg=[];
%
if isfield(TPlan.plan,'presc_dose')==0
    TPlan.plan.presc_dose=0;
end
answer=inputdlg('Prescription dose','Dose',1,{num2str(TPlan.plan.presc_dose)});
presc=str2num(answer{1});
%
cla(handles.amb_dose_eval);
%
[p,~] = listdlg('PromptString','Dose:',  'Selectionmode','multiple','ListString',handles.dose_menu.String(2:end));
p     = p+1;
%
%
%
% Exclude air 
if isfield(TPlan.plan,'Mat_HU')==1 && isempty(find(TPlan.plan.Mat_HU==1,1))==0
    % 
    answer = questdlg('Use material map to remove air?', ...
	'DAir contribution', ...
	'Yes','No','Yes');
% Handle response
switch answer
    case 'Yes'
      for j=1:size(p,2)
        TPlan.dose.doses.(sprintf(handles.dose_menu.String{p(j)}))(TPlan.plan.Mat_HU==1)=0;
      end
end
    %
end
%
for i=1:size(s,2)
    Mask=TPlan.struct.contours.(sprintf('Item_%d',s(i))).Mask;
    for j=1:size(p,2)
        clin_par.(sprintf(handles.dose_menu.String{p(j)})).(sprintf('%s',regexprep((sprintf('%s',str{s(i)})),'\W',''))).DVH=calculate_dvh(TPlan.dose.doses.(sprintf(handles.dose_menu.String{p(j)})),Mask,Vol,handles,presc);
        leg{end+1}=[str{s(i)} ' - ' strrep(handles.dose_menu.String{p(j)},'_', ' ')];
    end
end
legend(leg);
%
% load TPlan again to avoid saving dose maps without air
TPlan=getappdata(handles.amb_interface,'TPlan');
%
% display parameters 
TPlan.parameters  =  clin_par;
%
TPlan.parameters.Structures = [];
%
% organize it to export
Doses             =  fieldnames(TPlan.parameters);
Structures        =  fieldnames(TPlan.parameters.(sprintf(Doses{1})));
Parameters        =  fieldnames(TPlan.parameters.(sprintf(Doses{1})).(sprintf(Structures{1})).DVH);
%
%
DVH=[];
for k=1:size(Structures,1)
  for i=1:size(Doses,1)-1
       DVH(1:size(TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.center,2),i*2-1,k)  = TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.center;
       DVH(1:size(TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.center,2),i*2,k)    = TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.int;    
  end
end
%
%
for j=3:size(Parameters,1)
   for k=1:size(Structures,1)     
      for i=1:size(Doses,1)-1
        Clin_par(j-2,i,k) = TPlan.parameters.(sprintf(Doses{i})).(sprintf(Structures{k})).DVH.(sprintf(Parameters{j}));
      end
   end
   %
end
%
for i=1:size(Structures,1)
    TPlan.parameters.Structures.(sprintf(Structures{i})).Clin_Parameters                           = array2table(Clin_par(:,:,i));
    TPlan.parameters.Structures.(sprintf(Structures{i})).Clin_Parameters.Properties.RowNames       = Parameters(3:end);
    TPlan.parameters.Structures.(sprintf(Structures{i})).Clin_Parameters.Properties.VariableNames  = Doses(1:end-1);
    %
    Idx=max(max(DVH(:,1:2:end,i)))/(DVH(2,1,i)-DVH(1,1,i));
    if Idx > size(DVH,1)
        Idx = size(DVH,1);
    end
    %
    TPlan.parameters.Structures.(sprintf(Structures{i})).DVH(:,:)                                  = DVH(1:Idx,:,i);
end
%
% display
setappdata(handles.amb_interface,'TPlan',TPlan); 
disp_clinical_parameters(TPlan);
%
end

function [] = disp_clinical_parameters(TPlan)
fil       = fieldnames(TPlan.parameters);
contours  = fieldnames(TPlan.parameters.(sprintf('%s',fil{1})));
par       = fieldnames(TPlan.parameters.(sprintf('%s',fil{1})).(sprintf('%s',contours{1})).DVH);
%
clinic_p=[];
for i=1:length(contours)
    clinic_p{end+3}=['<html><font color="red" size="6">' contours{i} '</font></html>']; %#ok<*AGROW>
    for j=3:length(par)  
      clinic_p{end+1}=['<html><font color="blue" size="6">' par{j} '</font></html>'];
      for k=1:size(fil,1)-1  
       clinic_p{end+1}     = [fil{k} '  ' num2str(TPlan.parameters.(sprintf(fil{k})).(sprintf('%s',contours{i})).DVH.(sprintf('%s',par{j})),'%.3f')]; 
       %clinic_p{end+1}   = ['  ' fil{k} '  '];
      end
    end
end
%    
set(findobj('Tag','clinical_par_list'),'String',clinic_p,'Value',1);
end    
            
            
            
function [DVH] = calculate_dvh(Dose,Mask,Vol,handles,presc)
%
        Dose(Mask==0)=0;           
        Dose=reshape(Dose,1,[]);        
        Dose(Dose==0)=[]; 
        %
        edges = 0:0.01:max(Dose);
        N = histcounts(Dose,edges,'Normalization', 'probability');
        DVH.center=edges+0.05; DVH.center(end)=[];
        DVH.int=cumsum(N,'reverse')*100;
        %
        DVH.center(DVH.int<0.01)=[];
        DVH.int(DVH.int<0.01)=[];
        % normalize
        DVH.int=DVH.int/DVH.int(1)*100;
        %
        axes(handles.amb_dose_eval);
        plot(DVH.center,DVH.int,'--'); hold on;
        ylim([0 103]); drawnow;
        %
        DVH.V400=interp1(DVH.center,DVH.int,presc*4);
        DVH.V300=interp1(DVH.center,DVH.int,presc*3);
        DVH.V200=interp1(DVH.center,DVH.int,presc*2);
        DVH.V150=interp1(DVH.center,DVH.int,presc*1.5);
        DVH.V100=interp1(DVH.center,DVH.int,presc);
        DVH.V95=interp1(DVH.center,DVH.int,presc*0.95);
        DVH.V90=interp1(DVH.center,DVH.int,presc*0.9);
        DVH.V85=interp1(DVH.center,DVH.int,presc*0.85);
        DVH.V50=interp1(DVH.center,DVH.int,presc*0.50);
        %
        DVH.V5Gy=interp1(DVH.center,DVH.int,5);
        DVH.V10Gy=interp1(DVH.center,DVH.int,10);
        %
        DVH.Vol=Vol*size(Dose,2);
        %
        %       
        DVH.D100=DVH.center(find(round(DVH.int*100)/100<99.99,1))/presc*100;
        %
        r=find(round(DVH.int*10000)/10000>90);
        l(1)=r(end);                   l(2)=find(round(DVH.int*1000)/1000<90,1);
        %
%         figure;
%         plot(DVH.int(l(1):l(2)),DVH.center(l(1):l(2)));
        %
        %
        % DVH.D90 =interp1(DVH.int(l(2)-1:l(2)),DVH.center(l(2)-1:l(2)),90)/presc*100;
        %
        r=find(round(DVH.int*10000)/10000==90,1);
        if isempty(r)==1
            l=[];
            r=find(round(DVH.int*10000)/10000>90);
            l(1)=r(end);                   l(2)=find(round(DVH.int*1000)/1000<90,1);
            DVH.D90 =interp1(DVH.int(l(2)-1:l(2)),DVH.center(l(2)-1:l(2)),90)/presc*100;
        else
            DVH.D90 =DVH.center(r)/presc*100;
        end  
        %
        r=find(round(DVH.int*10000)/10000==85,1);
        if isempty(r)==1
            r=find(round(DVH.int*10000)/10000>85);
            l(1)=r(end);                   l(2)=find(round(DVH.int*1000)/1000<85,1);
            DVH.D85 =interp1(DVH.int(l(2)-1:l(2)),DVH.center(l(2)-1:l(2)),85)/presc*100;
        else
            DVH.D85 =DVH.center(r)/presc*100;
        end
        %
        DVH.presc=presc;
        %
        Dose = sort(Dose,'descend');
        %
        if round(0.1/Vol)<size(Dose,2)
          DVH.OAR01cc = mean(Dose(1:round(0.1/Vol)))/presc*100;
        else
          DVH.OAR01cc = mean(Dose(1:end))/presc*100;  
        end
        %
        if round(1/Vol)<size(Dose,2)
           DVH.OAR1cc  = mean(Dose(1:round(1/Vol)))/presc*100;
        else
           DVH.OAR1cc  = mean(Dose(1:end))/presc*100;  
        end
        %
        if round(2/Vol)<size(Dose,2)
          DVH.OAR2cc  = mean(Dose(1:round(2/Vol)))/presc*100;
        else
          DVH.OAR2cc  = mean(Dose(1:end))/presc*100;   
        end
end