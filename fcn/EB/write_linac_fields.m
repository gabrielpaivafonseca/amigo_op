function [] = write_linac_fields()
% create one MCNP input for each control point
%
global EB_p
% pick a folder 
folder = uigetdir;
%
if isnumeric(folder)==1 
    return;
end
%

for k=1:EB_p.NBeams
   % check each control point
   for j=1:EB_p.(sprintf('Beam_%d',k)).NumberOfControlPoints
       fid = fopen(sprintf('%s B_%d_CP_%d.inp',[folder filesep],k,j),'w');
       %
       fprintf(fid,'LINAC SIMPLE MODEL WITH MLC\n');
       %%
       write_linac_mlc_cells(fid);
       %%
       fprintf(fid,'c\n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,' 998 0 -27 #1                                                   imp:p=1 imp:e=1\n');
       fprintf(fid,'c ROOM\n');
       fprintf(fid,' 999 0  27                                                      imp:p=0 imp:e=0\n');
       fprintf(fid,'c\n');
       %
       fprintf(fid,'\n');
       %%
       write_linac_surfaces(fid)
       %%
       %
       fprintf(fid,'\n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c Source \n');
       fprintf(fid,'mode p e\n'); 
       fprintf(fid,'ssr old = 9999 tr=7\n');
       fprintf(fid,'c \n');
       %
       %%
       write_linac_materials(fid);
       %%
       write_linac_tr(fid,EB_p.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)));
       %%
       fclose(fid);
       %
   end
end

end


function [] = write_linac_mlc_cells(fid)
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c LINAC HEAD - CUBE FILLED WIT AIR MLC AND JAW \n');
       fprintf(fid,'  1 0   1  -2  3  -4   5  -6               trcl=1     fill=1002 imp:p=1 imp:e=1\n');
       fprintf(fid,'c COLLIMATOR 1 - X \n');
       fprintf(fid,'  2 1000  -0.0012    7  -8  -9      trcl=2   fill=1001   u=1002 imp:p=1 imp:e=1\n');
       fprintf(fid,'  3 1000  -0.0012    #2                                  u=1002 imp:p=1 imp:e=1\n');
       fprintf(fid,'c JAW 1 - X \n');
       fprintf(fid,'  4 1001 -17.85   11 -12 13 -14  15 -16         trcl=3   u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c JAW 2 - X \n');
       fprintf(fid,'  5 1001 -17.85   21 -22 23 -24  25 -26         trcl=4   u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c JAW 3 - Y \n');
       fprintf(fid,'  6 1001 -17.85   31 -32 33 -34  35 -36         trcl=5   u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c JAW 4 - Y \n');
       fprintf(fid,'  7 1001 -17.85   41 -42 43 -44  45 -46         trcl=6   u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c MLC HALF-LEAF PAIR  UPPER RIGHT \n');
       fprintf(fid,' 20 1001 -17.85  101 -102  103 -104 105 -106    trcl=20  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 21 1001 -17.85  101 -102  104 -107 105 -106    trcl=21  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=22:39 
        if mod(i,2)==0   
         fprintf(fid,' %d like 20 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 21 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       %
       fprintf(fid,'c MLC FULL-LEAF PAIR  UPPER RIGHT \n');
       fprintf(fid,' 40 1001 -17.85 151 -152  153 -154 155 -156     trcl=40  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 41 1001 -17.85 151 -152  154 -157 155 -156     trcl=41  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=42:49 
        if mod(i,2)==0   
         fprintf(fid,' %d like 40 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 41 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c MLC HALF-LEAF PAIR  LOWER RIGHT \n');
       fprintf(fid,' 50 1001 -17.85 201 -202  203 -204 205 -206     trcl=50  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 51 1001 -17.85 201 -202 -203  207 205 -206     trcl=51  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=52:69 
        if mod(i,2)==0   
         fprintf(fid,' %d like 50 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 51 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c MLC FULL-LEAF PAIR  LOWER RIGHT \n');
       fprintf(fid,'70 1001 -17.85 251 -252  253 -254 255 -256      trcl=70  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'71 1001 -17.85 251 -252 -253  257 255 -256      trcl=71  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=72:79 
        if mod(i,2)==0   
         fprintf(fid,' %d like 70 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 71 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c MLC HALF-LEAF PAIR  UPPER left \n');
       fprintf(fid,' 80 1001 -17.85 301 -302  303 -304 305 -306     trcl=80  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 81 1001 -17.85 301 -302  304 -307 305 -306     trcl=81  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=82:99 
        if mod(i,2)==0   
         fprintf(fid,' %d like 80 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 81 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c MLC FULL-LEAF PAIR  UPPER left \n');
       fprintf(fid,' 100 1001 -17.85 351 -352  353 -354 355 -356   trcl=100  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 101 1001 -17.85 351 -352  354 -357 355 -356   trcl=101  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=102:109 
        if mod(i,2)==0   
         fprintf(fid,' %d like 100 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 101 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c ---------------------------------------------------------------------------80\n');
       fprintf(fid,'c MLC HALF-LEAF PAIR  LOWER left\n');
       fprintf(fid,' 110 1001 -17.85 401 -402  403 -404 405 -406   trcl=110  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 111 1001 -17.85 401 -402 -403  407 405 -406   trcl=111  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=112:129 
        if mod(i,2)==0   
         fprintf(fid,' %d like 110 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 111 but trcl=%d\n',i,i);   
        end
       end
       fprintf(fid,'c \n');
       fprintf(fid,'c MLC FULL-LEAF PAIR  LOWER left  \n');
       fprintf(fid,' 130 1001 -17.85 451 -452  453 -454 455 -456  trcl=130  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,' 131 1001 -17.85 451 -452 -453  457 455 -456  trcl=131  u=1001 imp:p=1 imp:e=1\n');
       fprintf(fid,'c \n');
       for i=132:139 
        if mod(i,2)==0   
         fprintf(fid,' %d like 130 but trcl=%d\n',i,i);
        else
         fprintf(fid,' %d like 131 but trcl=%d\n',i,i);   
        end
       end
       %
       
fprintf(fid,'c ---------------------------------------------------------------------------80\n');
fprintf(fid,'c \n');
fprintf(fid,' 150 1000 -0.1 #4 #5 #6 #7 #20  #21  #22  #23  #24  #25  #26  #27  #28\n');
fprintf(fid,'                           #29  #30  #31  #32  #33  #34  #35  #36  #37  #38\n');
fprintf(fid,'                           #39  #40  #41  #42  #43  #44  #45  #46  #47  #48\n');
fprintf(fid,'                           #49  #50  #51  #52  #53  #54  #55  #56  #57  #58\n');
fprintf(fid,'                           #59  #60  #61  #62  #63  #64  #65  #66  #67  #68\n');
fprintf(fid,'                           #69  #70  #71  #72  #73  #74  #75  #76  #77  #78\n');
fprintf(fid,'                           #79  #80  #81  #82  #83  #84  #85  #86  #87  #88\n');
fprintf(fid,'                           #89  #90  #91  #92  #93  #94  #95  #96  #97  #98\n');
fprintf(fid,'                           #99  #100 #101 #102 #103 #104 #105 #106 #107 #108\n');
fprintf(fid,'                           #109 #110 #111 #112 #113 #114 #115 #116 #117 #118\n');
fprintf(fid,'                           #119 #120 #121 #122 #123 #124 #125 #126 #127 #128\n');
fprintf(fid,'                           #129 #130 #131 #132 #133 #134 #135 #136 #137 #138\n');
fprintf(fid,'                           #139                           u=1001 imp:p=1 imp:e=1\n');
%fprintf(fid,'c \n'); 
end


function [] = write_linac_surfaces(fid)
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c LINAC HEAD\n');
    fprintf(fid,'c\n');
    fprintf(fid,'1 px   -30.0\n');
    fprintf(fid,'2 px    30.0\n');
    fprintf(fid,'3 py   -30.0\n');
    fprintf(fid,'4 py    30.0\n');
    fprintf(fid,'5 pz   -60.0\n');
    fprintf(fid,'6 pz   -20.0\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c Collimator\n');
    fprintf(fid,'7 pz    -59\n');
    fprintf(fid,'8 pz    -15\n');
    fprintf(fid,'9 cz     29.0\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c JAW 1 - X\n');
    fprintf(fid,'c\n');
    fprintf(fid,'11 px    0.0\n');
    fprintf(fid,'12 px   12.0\n');
    fprintf(fid,'13 py  -10.0\n');
    fprintf(fid,'14 py   10.0\n');
    fprintf(fid,'15 pz   -7.8\n');
    fprintf(fid,'16 pz    0.0\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c JAW 2 - X\n');
    fprintf(fid,'c\n');
    fprintf(fid,'21 px  -12.0\n');
    fprintf(fid,'22 px    0.0\n');
    fprintf(fid,'23 py  -10.0\n');
    fprintf(fid,'24 py   10.0\n');
    fprintf(fid,'25 pz   -7.8\n');
    fprintf(fid,'26 pz    0.0\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c JAW 3 - Y\n');
    fprintf(fid,'c\n');
    fprintf(fid,'31 px  -10.00\n');
    fprintf(fid,'32 px   10.00\n');
    fprintf(fid,'33 py    0.00\n');
    fprintf(fid,'34 py   12.00\n');
    fprintf(fid,'35 pz   -7.77 \n');
    fprintf(fid,'36 pz    0.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c JAW 4 - Y\n');
    fprintf(fid,'c\n');
    fprintf(fid,'41 px  -10.00\n');
    fprintf(fid,'42 px   10.00\n');
    fprintf(fid,'43 py  -12.00\n');
    fprintf(fid,'44 py    0.00\n');
    fprintf(fid,'45 pz   -7.77\n');
    fprintf(fid,'46 pz    0.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - Upper Right\n');
    fprintf(fid,'c\n');
    fprintf(fid,'101 px    0.00\n');
    fprintf(fid,'102 px   10.00\n');
    fprintf(fid,'103 py    0.00\n');
    fprintf(fid,'104 py    0.25\n');
    fprintf(fid,'105 pz   -7.77\n');
    fprintf(fid,'106 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'107 py    0.50\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - Upper Right\n');
    fprintf(fid,'c\n');
    fprintf(fid,'151 px    0.00\n');
    fprintf(fid,'152 px   10.00\n');
    fprintf(fid,'153 py    5.00\n');
    fprintf(fid,'154 py    5.50\n');
    fprintf(fid,'155 pz   -7.77\n');
    fprintf(fid,'156 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'157 py    6.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - lower Right\n');
    fprintf(fid,'c\n');
    fprintf(fid,'201 px    0.00\n');
    fprintf(fid,'202 px   10.00\n');
    fprintf(fid,'203 py   -0.25\n');
    fprintf(fid,'204 py    0.00\n');
    fprintf(fid,'205 pz   -7.77\n');
    fprintf(fid,'206 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'207 py   -0.50\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - lower Right\n');
    fprintf(fid,'c\n');
    fprintf(fid,'251 px    0.00\n');
    fprintf(fid,'252 px   10.00\n');
    fprintf(fid,'253 py   -5.50\n');
    fprintf(fid,'254 py   -5.00\n');
    fprintf(fid,'255 pz   -7.77\n');
    fprintf(fid,'256 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'257 py   -6.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - Upper left\n');
    fprintf(fid,'c\n');
    fprintf(fid,'301 px  -10.00\n');
    fprintf(fid,'302 px    0.00\n');
    fprintf(fid,'303 py    0.00\n');
    fprintf(fid,'304 py    0.25\n');
    fprintf(fid,'305 pz   -7.77\n');
    fprintf(fid,'306 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'307 py    0.50\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - Upper left\n');
    fprintf(fid,'c\n');
    fprintf(fid,'351 px  -10.00\n');
    fprintf(fid,'352 px    0.00\n');
    fprintf(fid,'353 py    5.00\n');
    fprintf(fid,'354 py    5.50\n');
    fprintf(fid,'355 pz   -7.77\n');
    fprintf(fid,'356 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'357 py    6.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - lower left\n');
    fprintf(fid,'c\n');
    fprintf(fid,'401 px  -10.00\n');
    fprintf(fid,'402 px    0.00\n');
    fprintf(fid,'403 py   -0.25\n');
    fprintf(fid,'404 py    0.00\n');
    fprintf(fid,'405 pz   -7.77\n');
    fprintf(fid,'406 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'407 py   -0.50\n');
    fprintf(fid,'c\n');
    fprintf(fid,'c MLC - HALF-LEAF - PAIR - lower left\n');
    fprintf(fid,'c\n');
    fprintf(fid,'451 px  -10.00\n');
    fprintf(fid,'452 px    0.00\n');
    fprintf(fid,'453 py   -5.50\n');
    fprintf(fid,'454 py   -5.00\n');
    fprintf(fid,'455 pz   -7.77\n');
    fprintf(fid,'456 pz    0.00\n');
    fprintf(fid,'c\n');
    fprintf(fid,'457 py   -6.00\n');
    fprintf(fid,'c ---------------------------------------------------------------------------80\n');
    fprintf(fid,'27 s 3.5 3.5 0 300\n');
end

function [] = write_linac_materials(fid)
    %
    
    
    
    
    
    %%
    fprintf(fid,'c composition of air d= 0.0012\n');
    fprintf(fid,'m1000  6000. -0.000124\n');
    fprintf(fid,'       7000. -0.755270\n');
    fprintf(fid,'       8000. -0.231780\n');
    fprintf(fid,'      18000. -0.012827\n');
    %
    fprintf(fid,'c composition of the collimators d=17.85\n');
    fprintf(fid,'m1001  26000. -0.925\n');
    fprintf(fid,'       28000. -0.025\n');
    fprintf(fid,'       29000. -0.025\n');
    fprintf(fid,'       74000. -0.025\n');
    %
end

%
function [] = write_linac_tr(fid,field)
fprintf(fid,'c ---------------------------------------------------------------------------80\n');
fprintf(fid,'c LINAC HEAD - GANTRY\n');
fprintf(fid,'tr1*    %3.2f  %3.2f  %3.2f  &\n',field.TR_1(1), field.TR_1(2), field.TR_1(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_1(4), field.TR_1(5), field.TR_1(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_1(7), field.TR_1(8), field.TR_1(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_1(10),field.TR_1(11),field.TR_1(12));
%
fprintf(fid,'c LINAC HEAD - COLLIMATOR\n');
fprintf(fid,'tr2*    %3.2f  %3.2f  %3.2f  &\n',field.TR_2(1), field.TR_2(2), field.TR_2(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_2(4), field.TR_2(5), field.TR_2(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_2(7), field.TR_2(8), field.TR_2(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_2(10),field.TR_2(11),field.TR_2(12));
fprintf(fid,'c ---------------------------------------------------------------------------80\n');
fprintf(fid,'c JAW 1 - X\n');
fprintf(fid,'tr3*    %3.2f  %3.2f  %3.2f  &\n',field.TR_3(1), field.TR_3(2), field.TR_3(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_3(4), field.TR_3(5), field.TR_3(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_3(7), field.TR_3(8), field.TR_3(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_3(10),field.TR_3(11),field.TR_3(12));
fprintf(fid,'c\n');
fprintf(fid,'c JAW 2 - X\n');
fprintf(fid,'tr4*    %3.2f  %3.2f  %3.2f  &\n',field.TR_4(1), field.TR_4(2), field.TR_4(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_4(4), field.TR_4(5), field.TR_4(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_4(7), field.TR_4(8), field.TR_4(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_4(10),field.TR_4(11),field.TR_4(12));
fprintf(fid,'c JAW 3 - Y\n');
fprintf(fid,'tr5*    %3.2f  %3.2f  %3.2f  &\n',field.TR_5(1), field.TR_5(2), field.TR_5(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_5(4), field.TR_5(5), field.TR_5(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_5(7), field.TR_5(8), field.TR_5(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_5(10),field.TR_5(11),field.TR_5(12));
fprintf(fid,'c JAW 4 - Y\n');
fprintf(fid,'tr6*    %3.2f  %3.2f  %3.2f  &\n',field.TR_6(1), field.TR_6(2), field.TR_6(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_6(4), field.TR_6(5), field.TR_6(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_6(7), field.TR_6(8), field.TR_6(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_6(10),field.TR_6(11),field.TR_6(12));
fprintf(fid,'c ---------------------------------------------------------------------------80\n');
fprintf(fid,'c SOURCE TR\n');
fprintf(fid,'tr7*    %3.2f  %3.2f  %3.2f  &\n',field.TR_7(1), field.TR_7(2), field.TR_7(3));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_7(4), field.TR_7(5), field.TR_7(6));
fprintf(fid,'        %3.2f  %3.2f  %3.2f  &\n',field.TR_7(7), field.TR_7(8), field.TR_7(9));
fprintf(fid,'        %3.2f  %3.2f  %3.2f -1\n',field.TR_7(10),field.TR_7(11),field.TR_7(12));
fprintf(fid,'c ---------------------------------------------------------------------------80\n');
%
if   isfield(field,'MLC')==0
       MLC(1:60)  =  10;
       MLC(61:120)= -10;
else   % need to sort it to follow MCNP order -- easier to create TR but does not match tps
       MLC(61:90) = field.MLC(31:60)/10;
       MLC(91:120)= flip(field.MLC(1:30))/10;
       MLC(1:30)  = field.MLC(91:120)/10;
       MLC(31:60) = flip(field.MLC(61:90))/10;
end
%
fprintf(fid,'tr20*    %.4f 0.0000    -46.9700\n',MLC(1));  
fprintf(fid,'tr21*    %.4f 0.0000    -46.9700\n',MLC(2));  
fprintf(fid,'tr22*    %.4f 0.5000    -46.9700\n',MLC(3));  
fprintf(fid,'tr23*    %.4f 0.5000    -46.9700\n',MLC(4));  
fprintf(fid,'tr24*    %.4f 1.0000    -46.9700\n',MLC(5));  
fprintf(fid,'tr25*    %.4f 1.0000    -46.9700\n',MLC(6));  
fprintf(fid,'tr26*    %.4f 1.5000    -46.9700\n',MLC(7));  
fprintf(fid,'tr27*    %.4f 1.5000    -46.9700\n',MLC(8));  
fprintf(fid,'tr28*    %.4f 2.0000    -46.9700\n',MLC(9));  
fprintf(fid,'tr29*    %.4f 2.0000    -46.9700\n',MLC(10));  
fprintf(fid,'tr30*    %.4f 2.5000    -46.9700\n',MLC(11));  
fprintf(fid,'tr31*    %.4f 2.5000    -46.9700\n',MLC(12));  
fprintf(fid,'tr32*    %.4f 3.0000    -46.9700\n',MLC(13));  
fprintf(fid,'tr33*    %.4f 3.0000    -46.9700\n',MLC(14));  
fprintf(fid,'tr34*    %.4f 3.5000    -46.9700\n',MLC(15));  
fprintf(fid,'tr35*    %.4f 3.5000    -46.9700\n',MLC(16));  
fprintf(fid,'tr36*    %.4f 4.0000    -46.9700\n',MLC(17));  
fprintf(fid,'tr37*    %.4f 4.0000    -46.9700\n',MLC(18));  
fprintf(fid,'tr38*    %.4f 4.5000    -46.9700\n',MLC(19));  
fprintf(fid,'tr39*    %.4f 4.5000    -46.9700\n',MLC(20));  
fprintf(fid,'tr40*    %.4f 0.0000    -46.9700\n',MLC(21));  
fprintf(fid,'tr41*    %.4f 0.0000    -46.9700\n',MLC(22));  
fprintf(fid,'tr42*    %.4f 1.0000    -46.9700\n',MLC(23));  
fprintf(fid,'tr43*    %.4f 1.0000    -46.9700\n',MLC(24));  
fprintf(fid,'tr44*    %.4f 2.0000    -46.9700\n',MLC(25));  
fprintf(fid,'tr45*    %.4f 2.0000    -46.9700\n',MLC(26));  
fprintf(fid,'tr46*    %.4f 3.0000    -46.9700\n',MLC(27));  
fprintf(fid,'tr47*    %.4f 3.0000    -46.9700\n',MLC(28));  
fprintf(fid,'tr48*    %.4f 4.0000    -46.9700\n',MLC(29));  
fprintf(fid,'tr49*    %.4f 4.0000    -46.9700\n',MLC(30));  
fprintf(fid,'tr50*    %.4f -0.0000   -46.9700\n',MLC(31));  
fprintf(fid,'tr51*    %.4f -0.0000   -46.9700\n',MLC(32));  
fprintf(fid,'tr52*    %.4f -0.5000   -46.9700\n',MLC(33));  
fprintf(fid,'tr53*    %.4f -0.5000   -46.9700\n',MLC(34));  
fprintf(fid,'tr54*    %.4f -1.0000   -46.9700\n',MLC(35));  
fprintf(fid,'tr55*    %.4f -1.0000   -46.9700\n',MLC(36));  
fprintf(fid,'tr56*    %.4f -1.5000   -46.9700\n',MLC(37));  
fprintf(fid,'tr57*    %.4f -1.5000   -46.9700\n',MLC(38));  
fprintf(fid,'tr58*    %.4f -2.0000   -46.9700\n',MLC(39));  
fprintf(fid,'tr59*    %.4f -2.0000   -46.9700\n',MLC(40));  
fprintf(fid,'tr60*    %.4f -2.5000   -46.9700\n',MLC(41));  
fprintf(fid,'tr61*    %.4f -2.5000   -46.9700\n',MLC(42));  
fprintf(fid,'tr62*    %.4f -3.0000   -46.9700\n',MLC(43));  
fprintf(fid,'tr63*    %.4f -3.0000   -46.9700\n',MLC(44));  
fprintf(fid,'tr64*    %.4f -3.5000   -46.9700\n',MLC(45));  
fprintf(fid,'tr65*    %.4f -3.5000   -46.9700\n',MLC(46));  
fprintf(fid,'tr66*    %.4f -4.0000   -46.9700\n',MLC(47));  
fprintf(fid,'tr67*    %.4f -4.0000   -46.9700\n',MLC(48));  
fprintf(fid,'tr68*    %.4f -4.5000   -46.9700\n',MLC(49));  
fprintf(fid,'tr69*    %.4f -4.5000   -46.9700\n',MLC(50));  
fprintf(fid,'tr70*    %.4f -0.0000   -46.9700\n',MLC(51));  
fprintf(fid,'tr71*    %.4f -0.0000   -46.9700\n',MLC(52));  
fprintf(fid,'tr72*    %.4f -1.0000   -46.9700\n',MLC(53));  
fprintf(fid,'tr73*    %.4f -1.0000   -46.9700\n',MLC(54));  
fprintf(fid,'tr74*    %.4f -2.0000   -46.9700\n',MLC(55));  
fprintf(fid,'tr75*    %.4f -2.0000   -46.9700\n',MLC(56));  
fprintf(fid,'tr76*    %.4f -3.0000   -46.9700\n',MLC(57));  
fprintf(fid,'tr77*    %.4f -3.0000   -46.9700\n',MLC(58));  
fprintf(fid,'tr78*    %.4f -4.0000   -46.9700\n',MLC(59));  
fprintf(fid,'tr79*    %.4f -4.0000   -46.9700\n',MLC(60)); 
fprintf(fid,'tr80*    %.4f  0.0000   -46.9700\n',MLC(61));
fprintf(fid,'tr81*    %.4f  0.0000   -46.9700\n',MLC(62));  
fprintf(fid,'tr82*    %.4f  0.5000   -46.9700\n',MLC(63));
fprintf(fid,'tr83*    %.4f  0.5000   -46.9700\n',MLC(64));  
fprintf(fid,'tr84*    %.4f  1.0000   -46.9700\n',MLC(65));  
fprintf(fid,'tr85*    %.4f  1.0000   -46.9700\n',MLC(66));  
fprintf(fid,'tr86*    %.4f  1.5000   -46.9700\n',MLC(67));  
fprintf(fid,'tr87*    %.4f  1.5000   -46.9700\n',MLC(68));  
fprintf(fid,'tr88*    %.4f  2.0000   -46.9700\n',MLC(69));  
fprintf(fid,'tr89*    %.4f  2.0000   -46.9700\n',MLC(70));  
fprintf(fid,'tr90*    %.4f  2.5000   -46.9700\n',MLC(71));  
fprintf(fid,'tr91*    %.4f  2.5000   -46.9700\n',MLC(72));  
fprintf(fid,'tr92*    %.4f  3.0000   -46.9700\n',MLC(73));  
fprintf(fid,'tr93*    %.4f  3.0000   -46.9700\n',MLC(74));  
fprintf(fid,'tr94*    %.4f  3.5000   -46.9700\n',MLC(75));  
fprintf(fid,'tr95*    %.4f  3.5000   -46.9700\n',MLC(76));  
fprintf(fid,'tr96*    %.4f  4.0000   -46.9700\n',MLC(77));  
fprintf(fid,'tr97*    %.4f  4.0000   -46.9700\n',MLC(78));  
fprintf(fid,'tr98*    %.4f  4.5000   -46.9700\n',MLC(79));  
fprintf(fid,'tr99*    %.4f  4.5000   -46.9700\n',MLC(80));  
fprintf(fid,'tr100*   %.4f  0.0000   -46.9700\n',MLC(81));  
fprintf(fid,'tr101*   %.4f  0.0000   -46.9700\n',MLC(82));  
fprintf(fid,'tr102*   %.4f  1.0000   -46.9700\n',MLC(83));  
fprintf(fid,'tr103*   %.4f  1.0000   -46.9700\n',MLC(84));  
fprintf(fid,'tr104*   %.4f  2.0000   -46.9700\n',MLC(85));  
fprintf(fid,'tr105*   %.4f  2.0000   -46.9700\n',MLC(86));  
fprintf(fid,'tr106*   %.4f  3.0000   -46.9700\n',MLC(87));  
fprintf(fid,'tr107*   %.4f  3.0000   -46.9700\n',MLC(88));  
fprintf(fid,'tr108*   %.4f  4.0000   -46.9700\n',MLC(89));  
fprintf(fid,'tr109*   %.4f  4.0000   -46.9700\n',MLC(90));  
fprintf(fid,'tr110*   %.4f  -0.0000  -46.9700\n',MLC(91));  
fprintf(fid,'tr111*   %.4f  -0.0000  -46.9700\n',MLC(92));  
fprintf(fid,'tr112*   %.4f  -0.5000  -46.9700\n',MLC(93));  
fprintf(fid,'tr113*   %.4f  -0.5000  -46.9700\n',MLC(94));  
fprintf(fid,'tr114*   %.4f  -1.0000  -46.9700\n',MLC(95));  
fprintf(fid,'tr115*   %.4f  -1.0000  -46.9700\n',MLC(96));  
fprintf(fid,'tr116*   %.4f  -1.5000  -46.9700\n',MLC(97));  
fprintf(fid,'tr117*   %.4f  -1.5000  -46.9700\n',MLC(98));  
fprintf(fid,'tr118*   %.4f  -2.0000  -46.9700\n',MLC(99));  
fprintf(fid,'tr119*   %.4f  -2.0000  -46.9700\n',MLC(100));  
fprintf(fid,'tr120*   %.4f  -2.5000  -46.9700\n',MLC(101));  
fprintf(fid,'tr121*   %.4f  -2.5000  -46.9700\n',MLC(102));  
fprintf(fid,'tr122*   %.4f  -3.0000  -46.9700\n',MLC(103));  
fprintf(fid,'tr123*   %.4f  -3.0000  -46.9700\n',MLC(104));  
fprintf(fid,'tr124*   %.4f  -3.5000  -46.9700\n',MLC(105));  
fprintf(fid,'tr125*   %.4f  -3.5000  -46.9700\n',MLC(106));  
fprintf(fid,'tr126*   %.4f  -4.0000  -46.9700\n',MLC(107));  
fprintf(fid,'tr127*   %.4f  -4.0000  -46.9700\n',MLC(108));  
fprintf(fid,'tr128*   %.4f  -4.5000  -46.9700\n',MLC(109));  
fprintf(fid,'tr129*   %.4f  -4.5000  -46.9700\n',MLC(110));  
fprintf(fid,'tr130*   %.4f  -0.0000  -46.9700\n',MLC(111));  
fprintf(fid,'tr131*   %.4f  -0.0000  -46.9700\n',MLC(112));  
fprintf(fid,'tr132*   %.4f  -1.0000  -46.9700\n',MLC(113));  
fprintf(fid,'tr133*   %.4f  -1.0000  -46.9700\n',MLC(114));  
fprintf(fid,'tr134*   %.4f  -2.0000  -46.9700\n',MLC(115));  
fprintf(fid,'tr135*   %.4f  -2.0000  -46.9700\n',MLC(116));  
fprintf(fid,'tr136*   %.4f  -3.0000  -46.9700\n',MLC(117));  
fprintf(fid,'tr137*   %.4f  -3.0000  -46.9700\n',MLC(118));  
fprintf(fid,'tr138*   %.4f  -4.0000  -46.9700\n',MLC(119));  
fprintf(fid,'tr139*   %.4f  -4.0000  -46.9700\n',MLC(120));
%
end



%