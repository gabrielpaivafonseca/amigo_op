function [] = show_field(LEAF,JAW,BEAM,FIELD)
% display JAWA and MLC positions from a treatment plan
% do it foolowing 3 independen steps
% 
% 1 - linear position
% 2 - rotation of the colimator (around its axes)
% 3 - rotation of the gantry around the isocenter
%
%% 1 step 
if isfield(BEAM,'MLC')        % adjust mlc positions
 for i=1:size(BEAM.MLC,1)/2
   % Side 1
    LEAF(i,1).Vertices(:,1)= LEAF(i,1).Vertices(:,1)+BEAM.MLC(i)/10;
   % side 2
    LEAF(i,2).Vertices(:,1)= LEAF(i,2).Vertices(:,1)+BEAM.MLC(size(BEAM.MLC,1)/2+i)/10;
 end
else
  %  
  for i=1:size(LEAF,1)         % Move MLC out of the field
   % Side 1
    LEAF(i,1).Vertices(:,1)= LEAF(i,1).Vertices(:,1)-20;
   % side 2
    LEAF(i,2).Vertices(:,1)= LEAF(i,2).Vertices(:,1)+20;
  end
 end   
 % Jaw X
 JAW(1,1).Vertices(:,1)=JAW(1,1).Vertices(:,1)+BEAM.Jaw01(1)/10*0.634; % Plan gives the size of the field at the isocenter
 JAW(1,2).Vertices(:,1)=JAW(1,2).Vertices(:,1)+BEAM.Jaw01(2)/10*0.634; % 0.634 correts the size considering the jaw position
 %
 % Jaw Y
 JAW(2,1).Vertices(:,2)=JAW(2,1).Vertices(:,2)+BEAM.Jaw03(1)/10*0.721;
 JAW(2,2).Vertices(:,2)=JAW(2,2).Vertices(:,2)+BEAM.Jaw03(2)/10*0.721;
%
%

% --------------------------------------------------------------
%% 2 step
rotate(LEAF(:),[0 0 1],BEAM.CollimatorAngle,[0 0 0]);
rotate(JAW(:),[0 0 1],BEAM.CollimatorAngle, [0 0 0]);
%

%% 3 step --------------------------------------------------------
rotate(LEAF(:),[0 1 0],BEAM.GantryAngle,[0 0 0])
rotate(JAW(:), [0 1 0],BEAM.GantryAngle,[0 0 0]) 

% show projection of the field
FIELD.Vertices = [  0                 0                100;
                   BEAM.Jaw01(1)/10  BEAM.Jaw03(1)/10    0;
                   BEAM.Jaw01(2)/10  BEAM.Jaw03(1)/10    0;
                   BEAM.Jaw01(2)/10  BEAM.Jaw03(2)/10    0;
                   BEAM.Jaw01(1)/10  BEAM.Jaw03(2)/10    0];
               %
rotate(FIELD,[0 0 1],BEAM.CollimatorAngle,[0 0 0]);               
rotate(FIELD,[0 1 0],BEAM.GantryAngle,[0 0 0])               


% check user options to make parts visible ... it is important to draw
% everything and then make it invisible since user can change the options
% at any moment.
handles = guihandles;
TPlan=getappdata(handles.amb_interface,'TPlan');
if handles.EB_view_JAWx.UserData==0 
    TPlan.plan.Jaw(1).Visible = 'off';
    TPlan.plan.Jaw(3).Visible = 'off';
end
%
if handles.EB_view_JAWy.UserData==0
    TPlan.plan.Jaw(2).Visible = 'off';
    TPlan.plan.Jaw(4).Visible = 'off';
end
%
if handles.EB_view_MLC.UserData==0
   for i=1:size(TPlan.plan.MLC,1)
    TPlan.plan.MLC(i,1).Visible = 'off';
    TPlan.plan.MLC(i,2).Visible = 'off';
   end
end
%
if handles.EB_view_field.UserData ==0
    TPlan.plan.field.Visible     = 'off';
end
setappdata(handles.amb_interface,'TPlan',TPlan);
drawnow;
%
end