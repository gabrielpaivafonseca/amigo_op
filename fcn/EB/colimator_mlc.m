function [] = colimator_mlc(TPlan)
%
handles = guihandles;
axes(handles.EB_vis_01);
%
    v = [   0   0   47.35; 
            0   0   53.3;
        -16.6   0   53.3;
        -16.6   0   47.35; 
        -16.6 0.5   47.35;
        -16.6 0.5   53.3;
            0 0.5   53.3;
            0 0.5   47.35];
    %
    f = [1 2 3 4; 
         3 4 5 6;
         5 6 7 8; 
         1 2 7 8; 
         1 4 5 8; 
         2 3 6 7];
    %
    %
    %
    %
    %cla;
    cont=1;
    for i=1:10
      v1=v;
      v1(:,2)=v(:,2)-19.5+(i-1);
      v1(1:4,2)=v1(1:4,2)-0.5;
      P(cont,1)=patch('Faces',f,'Vertices',v1,'FaceColor','red','EdgeAlpha',0.3);
      v1(:,1)=v(:,1)+16.6;
      P(cont,2)=patch('Faces',f,'Vertices',v1,'FaceColor','red','EdgeAlpha',0.3);
      cont=cont+1;
    end
    %
    for i=1:40
      v1=v;
      v1(:,2)=v(:,2)-10+(i-1)*0.5;
      P(cont,1)=patch('Faces',f,'Vertices',v1,'FaceColor','g','EdgeAlpha',0.3);
      v1(:,1)=v(:,1)+16.6;
      P(cont,2)=patch('Faces',f,'Vertices',v1,'FaceColor','g','EdgeAlpha',0.3);
      cont=cont+1;
    end
    %
    for i=1:10
      v1=v;
      v1(:,2)=v(:,2)+10+(i-1);
      v1(5:8,2)=v1(5:8,2)+0.5;
      P(cont,1)=patch('Faces',f,'Vertices',v1,'FaceColor','red','EdgeAlpha',0.3);
      v1(:,1)=v(:,1)+16.6;
      P(cont,2)=patch('Faces',f,'Vertices',v1,'FaceColor','red','EdgeAlpha',0.3);
      cont=cont+1;
    end
    %
    %
    % clear view and draw again considering the origin
    if isfield(TPlan.plan,'MLC')==1
      delete(TPlan.plan.MLC);
    end
    %
    if isfield(TPlan.plan,'Jaw')==1
      delete(TPlan.plan.Jaw);
    end
    %
    if isfield(TPlan.plan,'field')==1
      delete(TPlan.plan.field);
    end
    %
    TPlan.plan.MLC=P;

    % colimators
    %
    v = [ 0 -20 55.59; 
          0 -20 63.39; 
        -20 -20 63.39; 
        -20 -20 55.59; 
        -20  20 55.59; 
        -20  20 63.39; 
          0  20 63.39; 
          0  20 55.59];
    %  
    f = [1 2 3 4; 
         3 4 5 6; 
         5 6 7 8; 
         1 2 7 8; 
         1 4 5 8; 
         2 3 6 7];
    %
    TPlan.plan.Jaw(1,1)=patch('Faces',f,'Vertices',v,'FaceColor',[0.6 0.6 0.6],'EdgeAlpha',0.3,'FaceAlpha',0.4);
    %
    v1=v;
    v1(:,1)=v1(:,1)+20;
    TPlan.plan.Jaw(1,2)=patch('Faces',f,'Vertices',v1,'FaceColor',[0.6 0.6 0.6],'EdgeAlpha',0.3,'FaceAlpha',0.4);
    %
    v(:,[1 2])=v(:,[2 1]);
    v(:,3)    =v(:,3)+8.72;
    TPlan.plan.Jaw(2,1)=patch('Faces',f,'Vertices',v,'FaceColor',[0.1 0.6 0.6],'EdgeAlpha',0.3,'FaceAlpha',0.4);
    %
    v1=v;
    v1(:,2)=v1(:,2)+20;
    TPlan.plan.Jaw(2,2)=patch('Faces',f,'Vertices',v1,'FaceColor',[0.1 0.6 0.6],'EdgeAlpha',0.3,'FaceAlpha',0.4);
    %
    % display field defined by the jaws
    v = [  0   0  100;
         -10 -10    0;
          10 -10    0;
          10  10    0;
         -10  10    0];
    % 
    f = [ 1 2 3;
          1 3 4;
          1 4 5;
          1 2 5];
    %
    TPlan.plan.field=patch('Faces',f,'Vertices',v,'FaceColor',[1 1 0],'EdgeAlpha',1,'FaceAlpha',0.15,'EdgeColor','y','LineWidth',1.5);
    
    %
    setappdata(handles.amb_interface,'TPlan',TPlan);
end