function [ ] = patch_dosecontrast_update( varargin )
set(findobj('Tag','amb_interface'),'WindowButtonMotionFcn', @update_dosecontrast);
end

function [ ] = update_dosecontrast( varargin )
%
handles = guihandles;
ax      = getappdata(handles.amb_interface,'ax');
pt      = getappdata(handles.amb_interface,'view_patch');
mouse_p = get(handles.amb_dos_contrast,'CurrentPoint');
x=xlim;
%
if strcmp(get(gco,'Tag'),'DoseContrast_01')==1
    P=pt.dosecontrast_min_box.Position;
    P(1)=mouse_p(1,1)-P(3)/2;
    if P(1)>pt.dosecontrast_max_box.Position(1)
       P(1)=pt.dosecontrast_max_box.Position(1)-1;
    elseif P(1)<x(1)
       P(1)=x(1)-P(3)/2;
    end
    pt.dosecontrast_min_box.Position=P;
    pt.dosecontrast_min_lin.XData=[P(1)+P(3)/2 P(1)+P(3)/2];
else
    P=pt.dosecontrast_max_box.Position;
    P(1)=mouse_p(1,1)-P(3)/2;
    if P(1)<pt.dosecontrast_min_box.Position(1)
       P(1)=pt.dosecontrast_min_box.Position(1)+1;
    elseif P(1)>x(2)
       P(1)=x(2)-P(3)/2;       
    end
    pt.dosecontrast_max_box.Position=P;
    pt.dosecontrast_max_lin.XData=[P(1)+P(3)/2 P(1)+P(3)/2];

end
l(1)=pt.dosecontrast_min_box.Position(1)+pt.dosecontrast_min_box.Position(3)/2;
l(2)=pt.dosecontrast_max_box.Position(1)+pt.dosecontrast_max_box.Position(3)/2;
%
set(handles.amb_dos_01,      'CLim',[l(1) l(2)]);
set(handles.amb_dos_02,      'CLim',[l(1) l(2)]);
set(handles.amb_dos_03,      'CLim',[l(1) l(2)]);
set(handles.amb_dos_contrast2,'CLim',[l(1) l(2)]);
%
end