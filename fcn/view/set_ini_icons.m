function  set_ini_icons(varargin)
% Contour on
% 
drawnow;
handles = guihandles; 
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'View_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.contour_on_off,'String',str);
set(handles.show_material,'String',str);
%
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'x_mark.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.contour_all_off,'String',str);
set(handles.show_mat_off,'String',str);
set(handles.ct_cal_delete,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'iso_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.isodose_view,'String',str,'UserData',0);
%
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'save.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.save_mathu,'String',str,'UserData',0);
set(handles.Save_materials,'String',str,'UserData',0);
set(handles.ct_cal_save,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'open.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.load_mathu,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'mat_map.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.create_mat_map,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'density.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.creat_dens_map,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'materials.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="150"/></html>'];
set(handles.edit_materials,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'ct_Cal.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="150"/></html>'];
set(handles.edit_ct_cal,'String',str,'UserData',0);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'new_file.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.new_ct_cal,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'refresh_01.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.save_cat_change,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cube.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.set_phantom_size,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'vox.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.set_dose_grid,'String',str);
%
% 
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.draw_on,'String',str);
handles.draw_sub.Value=0;
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_paint.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.draw_sub,'String',str);
handles.draw_sub.Value=0;
%

iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'execute.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.imedit_do,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'undo.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.imedit_undo,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'erase.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.imedit_erase,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'draw_off.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.draw_on,'String',str);
handles.draw_sub.Value=0;
%


%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_add.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.cont_add,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_rename.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.cont_rename,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'cont_delete.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.cont_delete,'String',str);

%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_x.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_x,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_y.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_y,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_z.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_z,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_MLC.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_MLC,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_cla.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_cla,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_settings.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_settings,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_field.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_field,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_JAWx.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_JAWx,'String',str);
%
iconFilename = [getappdata(handles.amb_interface,'icons_fol') 'EB_view_JAWy.png'];   iconFilename = strrep(iconFilename, '\', '/');
str{1}=['<html><img src="file:/' iconFilename '" height="32" width="32"/></html>'];
set(handles.EB_view_JAWy,'String',str);



end