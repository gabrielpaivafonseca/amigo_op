function [  ] = update_view( varargin )
   mouse_p=get(gca,'CurrentPoint');
   fig_r=findobj('Tag','amb_interface');
   %
   h = getappdata(fig_r,'view_patch');
   f = getappdata(fig_r,'sli_update');
   %
   if        f(1)==1 % axes 1 vertical
       x=xlim;
       if      mouse_p(1,1)>x(2)
            mouse_p(1,1)=x(2);
       elseif  mouse_p(1,1)<x(1)
            mouse_p(1,1)=x(1);
       end
       %
       move_fig01_ver(h,mouse_p);
       move_fig03_hor(h,mouse_p(:,[2,1,3]));
       %
       update_images(2,round([1 mouse_p(1,1) 1]));
       %
   elseif    f(1)==2 % axes 1 hor
       y=ylim;
       if      mouse_p(1,2)>y(2)
            mouse_p(1,2)=y(2);
       elseif  mouse_p(1,2)<y(1)
            mouse_p(1,2)=y(1);
       end
       %
       move_fig01_hor(h,mouse_p);
       move_fig02_hor(h,mouse_p);
       update_images(3,round([1 1 mouse_p(1,2)]));
       %
   elseif        f(2)==1 % axes 2 vertical
       x=xlim;
       if      mouse_p(1,1)>x(2)
            mouse_p(1,1)=x(2);
       elseif  mouse_p(1,1)<x(1)
            mouse_p(1,1)=x(1);
       end
       move_fig02_ver(h,mouse_p);
       move_fig03_ver(h,mouse_p);
       update_images(1,round([mouse_p(1,1) 1 1]));
   elseif    f(2)==2 % axes 2 hor
       y=ylim;
       if      mouse_p(1,2)>y(2)
            mouse_p(1,2)=y(2);
       elseif  mouse_p(1,2)<y(1)
            mouse_p(1,2)=y(1);
       end
       %
       move_fig02_hor(h,mouse_p);
       move_fig01_hor(h,mouse_p);
       update_images(3,round([1 1 mouse_p(1,2)]));
       %
   elseif        f(3)==1 % axes 3 vertical
       x=xlim;
       if      mouse_p(1,1)>x(2)
            mouse_p(1,1)=x(2);
       elseif  mouse_p(1,1)<x(1)
            mouse_p(1,1)=x(1);
       end
       move_fig03_ver(h,mouse_p);
       move_fig02_ver(h,mouse_p);
       update_images(1,round([mouse_p(1,1) 1 1]));
   elseif    f(3)==2 % axes 3 hor
       y=ylim;
       if      mouse_p(1,2)>y(2)
            mouse_p(1,2)=y(2);
       elseif  mouse_p(1,2)<y(1)
            mouse_p(1,2)=y(1);
       end
       move_fig03_hor(h,mouse_p);
       move_fig01_ver(h,mouse_p(:,[2,1,3])); 
       update_images(2,round([1 mouse_p(1,2) 1]));
   end
   drawnow;
end

function move_fig01_ver(h,mouse_p)
       P1=get(h.slice_rec_up01,'Position'); P1(1)=mouse_p(1)-P1(3)/2;
       P2=get(h.slice_rec_bt01,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up01,'Position',P1);
       set(h.slice_rec_bt01,'Position',P2);
       set(h.slice_lin_ve01,'XData',[mouse_p(1,1) mouse_p(1,1)]);
end
%
function move_fig01_hor(h,mouse_p)
       P1=get(h.slice_rec_lf01,'Position'); P1(2)=mouse_p(1,2)-P1(4)/2;
       P2=get(h.slice_rec_ri01,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf01,'Position',P1);
       set(h.slice_rec_ri01,'Position',P2);
       set(h.slice_lin_ho01,'YData',[mouse_p(1,2) mouse_p(1,2)]);
end

function move_fig02_ver(h,mouse_p)
       P1=get(h.slice_rec_up02,'Position'); P1(1)=mouse_p(1)-P1(3)/2;
       P2=get(h.slice_rec_bt02,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up02,'Position',P1);
       set(h.slice_rec_bt02,'Position',P2);
       set(h.slice_lin_ve02,'XData',[mouse_p(1,1) mouse_p(1,1)]);
end

function move_fig02_hor(h,mouse_p)
       P1=get(h.slice_rec_lf02,'Position'); P1(2)=mouse_p(1,2)-P1(4)/2;
       P2=get(h.slice_rec_ri02,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf02,'Position',P1);
       set(h.slice_rec_ri02,'Position',P2);
       set(h.slice_lin_ho02,'YData',[mouse_p(1,2) mouse_p(1,2)]);
end

function move_fig03_ver(h,mouse_p)
       P1=get(h.slice_rec_up03,'Position'); P1(1)=mouse_p(1)-P1(3)/2;
       P2=get(h.slice_rec_bt03,'Position'); P2(1)=P1(1);
       set(h.slice_rec_up03,'Position',P1);
       set(h.slice_rec_bt03,'Position',P2);
       set(h.slice_lin_ve03,'XData',[mouse_p(1,1) mouse_p(1,1)]);
end

function move_fig03_hor(h,mouse_p)
       P1=get(h.slice_rec_lf03,'Position'); P1(2)=mouse_p(1,2)-P1(4)/2;
       P2=get(h.slice_rec_ri03,'Position'); P2(2)=P1(2);
       set(h.slice_rec_lf03,'Position',P1);
       set(h.slice_rec_ri03,'Position',P2);
       set(h.slice_lin_ho03,'YData',[mouse_p(1,2) mouse_p(1,2)]);
end