function [] = adjust_contrast_axes(handles,TPlan,exe)
%
% contrast
ax=getappdata(handles.amb_interface,'ax');
pt = getappdata(handles.amb_interface,'view_patch');
h=handles.amb_vis_contrast; 
if exe==1    % new
   cla(h); 
   if      strcmp(handles.view_DCM.Checked,'on')==1 
     I=reshape(single(TPlan.image.Image(:)),[],1);
     [nelements,centers] = hist(I,300); nelements=nelements/max(nelements(centers>-900))+0.5; 
   elseif  strcmp(handles.view_mat_map.Checked,'on')==1
     I=reshape(single(TPlan.plan.Mat_HU(:)),[],1);  
     [nelements,centers] = hist(I,300); nelements=nelements/max(nelements(centers>0))+0.5; 
   elseif  strcmp(handles.view_dens_map.Checked,'on')==1
     I=reshape(single(TPlan.plan.Density(:)),[],1); 
     [nelements,centers] = hist(I(I>0.1),300); nelements=nelements/max(nelements(centers>0.1))+0.5; 
   end
   shift=+abs(min(centers));
   if min(centers)<0
       centers=centers+shift;
   end
   xy=[];   x=[1 max(centers)];                xy(1,:)=(1:diff(x)/128:max(centers));
   imagesc(handles.amb_vis_contrast2,xy,'AlphaData',0.9);              hold(h,'on'); 
   area(h, centers,nelements,'FaceColor','b','EdgeColor','k'); drawnow;
   set(h,'Color','none','Visible','on','xcolor','k','ycolor','k','Position',[0.001 0.75 0.174 0.08]); axis on; box on;
   set(handles.amb_vis_contrast2,'Visible','on','xcolor','k','ycolor','k','Position',[0.001 0.75 0.174 0.08]); axis on; box on;
   %
   ax.contrast.scale= get(handles.amb_vis_contrast2,'Child');
   ax.contrast.bar  = get(h,'Child');
   ax.contrast.shift= shift;
   %
   set(ax.contrast.bar,'ButtonDownFcn',@set_contrast_axes_limit)
   set(handles.amb_vis_contrast,'ButtonDownFcn',@set_contrast_axes_limit);
   setappdata(handles.amb_interface,'ax',ax);
elseif exe==2 % restore
   centers=ax.contrast.bar.XData;
   Y=ax.contrast.bar.YData-0.5;
   Y=Y/max(Y)+0.5;
   ax.contrast.bar.YData=Y;
   xy=[];   x=[1 max(centers)];                xy(1,:)=(1:diff(x)/128:max(centers)); 
elseif exe==3 % adjust limits
   centers=[]; 
   centers(1)=pt.contrast_min_lin.XData(1);%-ax.contrast.shift;
   centers(2)=pt.contrast_max_lin.XData(1);%-ax.contrast.shift;
   %
   X=ax.contrast.bar.XData;
   Y=ax.contrast.bar.YData-0.5;
   Y=Y/max(Y(X>centers(1)))+0.5;
   ax.contrast.bar.YData=Y;
   xy=[];   x=[centers(1) centers(2)];             xy(1,:)=(x(1):diff(x)/128:x(2)); 
end      
%
set(ax.contrast.scale,'CData',xy);  
shift=ax.contrast.shift;
if  strcmp(handles.view_dens_map.Checked,'on')==1
   set(h,'xlim',[min(centers) max(centers)], ... 
      'XTickLabel',num2cell((round((-shift+diff(x)*0.05:(max(centers)-min(centers))/5:(max(centers)-shift-diff(x)*0.05))*10))/10), ...
      'XTick',((0+diff(x)*0.05):(max(centers)-min(centers))/5:(max(centers)-diff(x)*0.05)),'YTick',[],'ylim',[0.5 1.5]);
else
   set(h,'xlim',[min(centers) max(centers)], ... 
      'XTickLabel',num2cell(round(-shift+diff(x)*0.05:(max(centers)-min(centers))/5:(max(centers)-shift-diff(x)*0.05))), ...
      'XTick',round((0+diff(x)*0.05):(max(centers)-min(centers))/5:(max(centers)-diff(x)*0.05)),'YTick',[],'ylim',[0.5 1.5]); 
end
  %
l(1)=min(centers)-shift; l(2)=max(centers)-shift;
%
set(handles.amb_vis_01,        'CLim',[l(1) l(2)]);
set(handles.amb_vis_02,        'CLim',[l(1) l(2)]);
set(handles.amb_vis_03,        'CLim',[l(1) l(2)]);
set(handles.amb_vis_contrast2, 'CLim',[l(1)+ax.contrast.shift l(2)+ax.contrast.shift]);
%
%
if isempty(pt) == 1;                return; end
if isvalid(pt.contrast_max_box)==0; return; end
%
%
pt.contrast_min_box.Position(1)=l(1)+ax.contrast.shift-diff(l)*0.03;
pt.contrast_min_box.Position(3)=diff(l)*0.06;
pt.contrast_max_box.Position(1)=l(2)+ax.contrast.shift-diff(l)*0.03;
pt.contrast_max_box.Position(3)=diff(l)*0.06;
pt.contrast_min_lin.XData=[l(1)+ax.contrast.shift l(1)+ax.contrast.shift];
pt.contrast_max_lin.XData=[l(2)+ax.contrast.shift l(2)+ax.contrast.shift];
end


function [] = set_contrast_axes_limit(varargin)
persistent chk
if isempty(chk)
      chk = 1;
      pause(0.5); %Add a delay to distinguish single click from a double click
      if chk == 1
         % fprintf(1,'\nI am doing a single-click.\n\n');
          chk = [];
      end
else
      chk = [];
      %
      handles = guihandles;
      TPlan =getappdata(handles.amb_interface,'TPlan');
      adjust_contrast_axes(handles,TPlan,3);
end
end