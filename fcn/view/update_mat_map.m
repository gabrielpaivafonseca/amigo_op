function [  ] = update_mat_map(fig,TPlan,current_slices,ax,alp)
% update contours
%
% fig_r          =  findobj('Tag','amb_interface');
% TPlan          =  getappdata(ax.gui,'TPlan');
% current_slices =  getappdata(ax.gui,'slices');
%
%
menu_cont=ax.material_pop_menu;
Cont_list=get(menu_cont,'UserData');
if isempty(find(Cont_list(:,1)==1,1))==1
    fig=4; 
    for i=1:size(Cont_list,1)
       menu_cont.String{i}(19:25)='#000000';
    end
    setappdata(ax.gui,'plot_materials',0);
else
    setappdata(ax.gui,'plot_materials',1);
end % set all to zero
%
if fig==1 || fig==4 % update all
    I =zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2),3);
    Im=zeros(TPlan.image.Nvoxels(1),TPlan.image.Nvoxels(2));
    for i=1:size(Cont_list,1)
        if Cont_list(i,1)==1
          Im(:)=0;
          Im(TPlan.image.Image(:,:,current_slices(1))>=Cont_list(i,2) ...
              & TPlan.image.Image(:,:,current_slices(1))<=Cont_list(i,3))=1;
          %
          I(:,:,1)=I(:,:,1)+Im*Cont_list(i,4);  
          I(:,:,2)=I(:,:,2)+Im*Cont_list(i,5); 
          I(:,:,3)=I(:,:,3)+Im*Cont_list(i,6); 
        end
        set(ax.fig01_contour,'CData',I,'AlphaData',alp*logical(rgb2gray(I)),'Tag','material_xy'); 
    end  
end    
% %
if fig==2 || fig==4 % update all
    I = zeros(TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3),3);
    Im= zeros(TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3));
    for i=1:size(Cont_list,1)
        if Cont_list(i,1)==1
          Im(:)=0; 
          Im(squeeze(TPlan.image.Image(:,current_slices(2),:))>=Cont_list(i,2) ...
             & squeeze(TPlan.image.Image(:,current_slices(2),:))<=Cont_list(i,3))=1;
         %
          I(:,:,1)=I(:,:,1)+Im*Cont_list(i,4);  
          I(:,:,2)=I(:,:,2)+Im*Cont_list(i,5); 
          I(:,:,3)=I(:,:,3)+Im*Cont_list(i,6); 
        end
        set(ax.fig02_contour,'CData',I,'AlphaData',alp*logical(rgb2gray(I)),'Tag','material_yz'); 
    end  
end  
% %
if fig==3 || fig==4 % update all
    I = zeros(TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3),3);
    Im= zeros(TPlan.image.Nvoxels(2),TPlan.image.Nvoxels(3));
    for i=1:size(Cont_list,1)
        if Cont_list(i,1)==1
          Im(:)=0; 
          Im(squeeze(TPlan.image.Image(current_slices(3),:,:))>=Cont_list(i,2) ...
             & squeeze(TPlan.image.Image(current_slices(3),:,:))<=Cont_list(i,3))=1;
          %  
          I(:,:,1)=I(:,:,1)+Im*Cont_list(i,4);  
          I(:,:,2)=I(:,:,2)+Im*Cont_list(i,5); 
          I(:,:,3)=I(:,:,3)+Im*Cont_list(i,6); 
        end
        set(ax.fig03_contour,'CData',I,'AlphaData',alp*logical(rgb2gray(I)),'Tag','material_xz'); 
    end  
end  

end

