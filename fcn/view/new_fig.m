%%  Display matlab figure
figure;
fontsize=14;   font='Arial';
scrsz = get(0,'ScreenSize');              % half size of the screen
set(gcf,'Position',[scrsz(3)/14 scrsz(4)/14 scrsz(3)/1.2 scrsz(4)/1.2]);
set(gcf, 'color','w');
set(gcf, 'DefaultTextFontSize', fontsize); 
set(gcf, 'DefaultAxesFontSize', fontsize); 
set(gcf, 'DefaultAxesFontName', font);
set(gcf, 'DefaultTextFontName', font);
% set(gca, 'LineWidth', 2);
% set(gca, 'Box', 'on');
% xlabel x
% ylabel y
% title  Title

