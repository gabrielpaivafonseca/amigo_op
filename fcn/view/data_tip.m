function [ ] = data_tip( varargin )
fig_r          =  findobj('Tag','amb_interface');
TPlan          =  getappdata(fig_r ,'TPlan');
sli            =  getappdata(fig_r ,'slices');
dose           =  getappdata(fig_r ,'show_dose');
%
h              =  gca;
t              =  findobj('Tag','amb_info');
%
output_txt={};
C = get(h,'CurrentPoint');   
%
x=xlim(gca);    y=ylim(gca);
pos(1)=C(1);    pos(2)=C(1,2);
%
pos=round(pos);
if (pos(1)>0 && pos(1)<x(2)) &&(pos(2)>0 && pos(2)<y(2)) && ...
      (strcmp(h.Tag,'amb_dos_01')==1 || strcmp(h.Tag,'amb_dos_02')==1 ||  strcmp(h.Tag,'amb_dos_03')==1)  
     
  set(fig_r ,'pointer','cross');
  if strcmp(h.Tag,'amb_dos_01')
     output_txt{end+1}=['Voxel ' num2str(pos(2),'%d') ' ' num2str(pos(1),'%d') ' ' num2str(sli(1),'%d')];
     %
     if isfield(TPlan.image,'Image')==1
       output_txt{end+1}=[' HU: ' num2str(TPlan.image.Image(pos(2),pos(1),sli(1)),'%d')];               %#ok<*NASGU>
     end
     %
     if strcmp(dose,'none')==0
        output_txt{end+1}=[dose ': ' num2str(TPlan.dose.(sprintf(dose)).Dose(pos(2),pos(1),sli(1)),'%3.2f')];   %#ok<*NASGU>  
     end
     if isfield(TPlan.dose,dose)==1 && isfield(TPlan.dose.(sprintf(dose)),'uncert')==1
       output_txt{end+1}=['Uncert: ' num2str(TPlan.dose.(sprintf(dose)).uncert(pos(2),pos(1),sli(1)),'%3.2f') '%'];   %#ok<*NASGU>
     end
  %
     if isfield(TPlan.plan,'Density')==1
       output_txt{end+1}=[' Dens.: ' num2str(TPlan.plan.Density(pos(2),pos(1),sli(1)),'%3.2f') ' g/cm3'];   %#ok<*NASGU>
     end  
  %
  %
     if isfield(TPlan.plan,'Mat_HU')==1
       id=TPlan.plan.Mat_HU(pos(2),pos(1),sli(1));
       if id==0
          nam='none'; 
       else
          nam=TPlan.plan.materials.(sprintf('mat%.0f',id)).name;
       end
       output_txt{end+1}=[' Mat.: ' nam];   %#ok<*NASGU>
     end  
  %
  %
  elseif strcmp(h.Tag,'amb_dos_02')
     output_txt{end+1}=['Voxel ' num2str(pos(2),'%d') ' ' num2str(sli(2),'%d') ' ' num2str(pos(1),'%d')];
     %
     if isfield(TPlan.image,'Image')==1
       output_txt{end+1}=[' HU: ' num2str(TPlan.image.Image(pos(2),sli(2),pos(1)),'%d')];               %#ok<*NASGU>
     end
     %
     if strcmp(dose,'none')==0
        output_txt{end+1}=[dose ': ' num2str(TPlan.dose.(sprintf(dose)).Dose(pos(2),sli(2),pos(1)),'%3.2f')];   %#ok<*NASGU>
     end
    if isfield(TPlan.dose,dose)==1 && isfield(TPlan.dose.(sprintf(dose)),'uncert')==1
       output_txt{end+1}=['Uncert: ' num2str(TPlan.dose.(sprintf(dose)).uncert(pos(2),sli(2),pos(1)),'%3.2f') '%'];   %#ok<*NASGU>
     end
     %
     if isfield(TPlan.plan,'Density')==1
       output_txt{end+1}=[' Dens.: ' num2str(TPlan.plan.Density(pos(2),sli(2),pos(1)),'%3.2f') ' g/cm3'];   %#ok<*NASGU>
     end  
  %
  %
     if isfield(TPlan.plan,'Mat_HU')==1
       id=TPlan.plan.Mat_HU(pos(2),sli(2),pos(1));
       if id==0
          nam='none'; 
       else
          nam=TPlan.plan.materials.(sprintf('mat%.0f',id)).name;
       end
       output_txt{end+1}=[' Mat.: ' nam];   %#ok<*NASGU>
     end  
  %   
  %
  elseif strcmp(h.Tag,'amb_dos_03')
     output_txt{end+1}=['Voxel ' num2str(sli(2),'%d') ' ' num2str(pos(2),'%d') ' ' num2str(pos(1),'%d')];
     %
     if isfield(TPlan.image,'Image')==1
       output_txt{end+1}=[' HU: ' num2str(TPlan.image.Image(sli(3),pos(2),pos(1)),'%d')];               %#ok<*NASGU>
     end
     %
     %
     if strcmp(dose,'none')==0
        output_txt{end+1}=[dose ': ' num2str(TPlan.dose.(sprintf(dose)).Dose(sli(3),pos(2),pos(1)),'%3.2f')];   %#ok<*NASGU>
     end
     if isfield(TPlan.dose,dose)==1 && isfield(TPlan.dose.(sprintf(dose)),'uncert')==1
       output_txt{end+1}=['Uncert: ' num2str(TPlan.dose.(sprintf(dose)).uncert(sli(3),pos(2),pos(1)),'%3.2f') '%'];   %#ok<*NASGU>
     end
     %

  %
     if isfield(TPlan.plan,'Density')==1
       output_txt{end+1}=[' Dens.: ' num2str(TPlan.plan.Density(sli(3),pos(2),pos(1)),'%3.2f') ' g/cm3'];   %#ok<*NASGU>
     end  
  %
  %
     if isfield(TPlan.plan,'Mat_HU')==1
       id=TPlan.plan.Mat_HU(sli(3),pos(2),pos(1));
       if id==0
          nam='none'; 
       else
          nam=TPlan.plan.materials.(sprintf('mat%.0f',id)).name;
       end
       output_txt{end+1}=[' Mat.: ' nam];   %#ok<*NASGU>
     end  
  %  
  %
  end
  %
  %
else
     set(fig_r ,'pointer','default');
end
t.String=strjoin(output_txt);
end

