function [ ] = patch_update( varargin )
   h=findobj('Tag','amb_interface');
   setappdata(h,'Track',1);
   if     strcmp(get(gco,'Tag'),'Fig01_01')==1
       setappdata(h,'sli_update',[1 0 0]);
   elseif strcmp(get(gco,'Tag'),'Fig01_02')==1
       setappdata(h,'sli_update',[2 0 0]);
   elseif strcmp(get(gco,'Tag'),'Fig02_01')==1
       setappdata(h,'sli_update',[0 1 0]);
   elseif strcmp(get(gco,'Tag'),'Fig02_02')==1
       setappdata(h,'sli_update',[0 2 0]);   
   elseif strcmp(get(gco,'Tag'),'Fig03_01')==1
       setappdata(h,'sli_update',[0 0 1]);
   elseif strcmp(get(gco,'Tag'),'Fig03_02')==1
       setappdata(h,'sli_update',[0 0 2]);         
   end
   set(h,'WindowButtonMotionFcn', @update_view);
end

