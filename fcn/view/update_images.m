function [   ] = update_images(fig,sli)
fig_r          =  findobj('Tag','amb_interface');
handles        =  guihandles(fig_r);
h              =  getappdata(fig_r,'ax');
p              =  getappdata(fig_r,'view_patch');
TPlan          =  getappdata(fig_r,'TPlan');
current_slices =  double(getappdata(fig_r,'slices'));
%
if strcmp(handles.view_DCM.Checked,'on')==1 % show dcm images
    if     fig==1  || fig==4
        set(h.fig01,'CData',TPlan.image.Image(:,:,sli(1)));
        set(p.slice_text01,'String',[num2str(sli(1)) ' (' num2str((sli(1)-1)*TPlan.image.Resolution(3)+TPlan.image.ImagePositionPatient(3),'%.1f') ' mm)']);
        current_slices(1)=sli(1);
    end
    if fig==2  || fig==4
        set(h.fig02,'CData',squeeze(TPlan.image.Image(:,sli(2),:)));
        set(p.slice_text02,'String',[num2str(sli(2)) ' (' num2str((sli(2)-1)*TPlan.image.Resolution(1)+TPlan.image.ImagePositionPatient(1),'%.1f') ' mm)']);
        current_slices(2)=sli(2);
    end
    if fig==3  || fig==4
        set(h.fig03,'CData',squeeze(TPlan.image.Image(sli(3),:,:)));
        set(p.slice_text03,'String',[num2str(sli(3)) ' (' num2str((sli(3)-1)*TPlan.image.Resolution(2)+TPlan.image.ImagePositionPatient(2),'%.1f') ' mm)']);
        current_slices(3)=sli(3);
    end
elseif strcmp(handles.view_mat_map.Checked,'on')==1  % Material map
    if     fig==1  || fig==4
        set(h.fig01,'CData',TPlan.plan.Mat_HU(:,:,sli(1)));
        set(p.slice_text01,'String',[num2str(sli(1)) ' (' num2str((sli(1)-1)*TPlan.image.Resolution(3)+TPlan.image.ImagePositionPatient(3),'%.1f') ' mm)']);
        current_slices(1)=sli(1);
    end
    if fig==2  || fig==4
        set(h.fig02,'CData',squeeze(TPlan.plan.Mat_HU(:,sli(2),:)));
        set(p.slice_text02,'String',[num2str(sli(2)) ' (' num2str((sli(2)-1)*TPlan.image.Resolution(1)+TPlan.image.ImagePositionPatient(1),'%.1f') ' mm)']);
        current_slices(2)=sli(2);
    end
    if fig==3  || fig==4
        set(h.fig03,'CData',squeeze(TPlan.plan.Mat_HU(sli(3),:,:)));
        set(p.slice_text03,'String',[num2str(sli(3)) ' (' num2str((sli(3)-1)*TPlan.image.Resolution(2)+TPlan.image.ImagePositionPatient(2),'%.1f') ' mm)']);
        current_slices(3)=sli(3);
    end
elseif strcmp(handles.view_dens_map.Checked,'on')==1 % Density map  
    if     fig==1  || fig==4
        set(h.fig01,'CData',TPlan.plan.Density(:,:,sli(1)));
        set(p.slice_text01,'String',[num2str(sli(1)) ' (' num2str((sli(1)-1)*TPlan.image.Resolution(3)+TPlan.image.ImagePositionPatient(3),'%.1f') ' mm)']);
        current_slices(1)=sli(1);
    end
    if fig==2  || fig==4
        set(h.fig02,'CData',squeeze(TPlan.plan.Density(:,sli(2),:)));
        set(p.slice_text02,'String',[num2str(sli(2)) ' (' num2str((sli(2)-1)*TPlan.image.Resolution(1)+TPlan.image.ImagePositionPatient(1),'%.1f') ' mm)']);
        current_slices(2)=sli(2);
    end
    if fig==3  || fig==4
        set(h.fig03,'CData',squeeze(TPlan.plan.Density(sli(3),:,:)));
        set(p.slice_text03,'String',[num2str(sli(3)) ' (' num2str((sli(3)-1)*TPlan.image.Resolution(2)+TPlan.image.ImagePositionPatient(2),'%.1f') ' mm)']);
        current_slices(3)=sli(3);
    end
end
setappdata(h.gui,'slices',current_slices);
%
%
% Update contour
if getappdata(h.gui,'plot_contour')==1
    trans=getappdata(h.gui,'trans_contour');
    if isempty(trans)==1
        trans=0.5;
        setappdata(h.gui,'trans_contour',trans);
    end
    update_contours(fig,TPlan,current_slices,h,trans);
end
%
if strcmp(getappdata(h.gui,'show_dose'),'none')==0
%
    trans=getappdata(h.gui,'trans_dose');
    if isempty(trans)==1
        trans=0.5;
        setappdata(h.gui,'trans_dose',trans);
    end
    update_dose(fig,TPlan,current_slices,h,trans,getappdata(fig_r,'show_dose'));
end

iso = getappdata(fig_r,'isod_settings');
if  isempty(iso)==0 && iso.view==1
    update_isodoses(fig,TPlan,current_slices,h);
end

% show material map -- will overlap structures plot ...
if isempty(find(h.material_pop_menu.UserData(:,1)==1,1))==0 ...
              &&  strcmp(h.section_mathu.State,'on')==1
    %
    trans=getappdata(h.gui,'trans_material');
    if isempty(trans)==1
        trans=0.5;
        setappdata(h.gui,'trans_material',trans);
    end    
      update_mat_map(fig,TPlan,current_slices,h,trans);     
end

