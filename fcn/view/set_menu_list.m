function set_menu_list(varargin)
% this function adjust the menu size, font and text. Interface uses java
% and HTML for this.
% 
%
handles = guihandles;
% Adjust menu
drawnow; pause(0.1);
jFrame = get(gcf,'JavaFrame');
jMenuBar = jFrame.fHG2Client.getMenuBar;
jMenuBar.setPreferredSize(java.awt.Dimension(20,50));
jMenuBar.revalidate; % refresh/update the displayed toolbar
%
color = java.awt.Color.white;
jMenuBar.setBackground(color);
jMenuBar.getParent.getParent.setBackground(color);
jtbc = jMenuBar.getComponents;
% force rendering
for idx = 1 : length(jtbc)
    jtbc(idx).doClick;   drawnow;   pause(0.1);
    jtbc(idx).doClick;   drawnow;   pause(0.1);          
end
drawnow;
jtbc(2).doClick;  drawnow; 
jtbc(2).doClick;  drawnow; 
%
for idx = 1 : length(jtbc)
    jtbc(idx).setOpaque(true);
    jtbc(idx).setBackground(color);
    for childIdx = 1 : length(jtbc(idx).getMenuComponents)
        jtbc(idx).getMenuComponent(childIdx-1).setBackground(color);
        jtbc(idx).getMenuComponent(childIdx-1).setOpaque(true);
        jtbc2=[];
        jtbc2 = jtbc(idx).getMenuComponents;
        if length(jtbc2)>=1
         for idx2=1:length(jtbc2)
          try   
           for childIdx2 = 1 : length(jtbc2(idx2).getMenuComponents)
             jtbc2(idx2).getMenuComponent(childIdx2-1).setBackground(color);
             jtbc2(idx2).getMenuComponent(childIdx2-1).setOpaque(true);
           end
          end
         end
        end
    end
end
jMenuBar.revalidate; % refresh/update the displayed toolbar
%
ic_folder=getappdata(gcf,'icons_fol');
% First level
txt1 = '<html><FONT color="black" size="6" face="Cambria">File         </FONT></u>';   set(handles.menu_file,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">View         </FONT></u>';   set(handles.menu_view,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Simulation   </FONT></u>';   set(handles.menu_sim,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Ext. BEAM    </FONT></u>';   set(handles.menu_EB,      'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Export       </FONT></u>';   set(handles.export_menu,  'Label',txt1);
%
% Second level
txt1 = '<html><FONT color="black" size="6" face="Cambria">Open   </FONT></u>';         set(handles.menu_open, 'Label',txt1);
jFrame = get(handles.amb_interface,'JavaFrame');
jMenuBar = jFrame.fHG2Client.getMenuBar;
jarFile = fullfile(matlabroot,'/java/jar/mwt.jar'); 
jFileMenu = jMenuBar.getComponent(0);                  jSave = jFileMenu.getMenuComponent(0);
jSave.setIcon(javax.swing.ImageIcon([ic_folder 'load.png']));
%     % 
txt1 = '<html><FONT color="black" size="6" face="Cambria">Save   </FONT></u>';         set(handles.amb_save, 'Label',txt1);
jFrame = get(handles.amb_interface,'JavaFrame');
jMenuBar = jFrame.fHG2Client.getMenuBar;
jarFile = fullfile(matlabroot,'/java/jar/mwt.jar'); 
jFileMenu = jMenuBar.getComponent(0);                  jSave = jFileMenu.getMenuComponent(1);
jSave.setIcon(javax.swing.ImageIcon([ic_folder 'save_2.png']));
%  


drawnow;
jtbc(1).doClick;  drawnow; 
jtbc(1).doClick;  drawnow; 
%
%
txt1 = '<html><FONT color="black" size="6" face="Cambria">Axes           </FONT></u>';         set(handles.menu_view_axes, 'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Plan           </FONT></u>';         set(handles.menu_plan,      'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Transparency   </FONT></u>';         set(handles.menu_transp,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Contrast       </FONT></u>';         set(handles.menu_contrast,  'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Isodose        </FONT></u>';         set(handles.menu_isodose,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Image          </FONT></u>';         set(handles.menu_view_image,'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">CT/MR/US       </FONT></u>';         set(handles.view_DCM,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Material map   </FONT></u>';         set(handles.view_mat_map,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Density        </FONT></u>';         set(handles.view_dens_map,  'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Exp points     </FONT></u>';         set(handles.exp_points,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Struct         </FONT></u>';         set(handles.import_struct,  'Label',txt1);


txt1 = '<html><FONT color="black" size="6" face="Cambria">Tools          </FONT></u>';         set(handles.menu_tools,     'Label',txt1);
% Simulation
txt1 = '<html><FONT color="black" size="6" face="Cambria">Phantom          </FONT></u>';         set(handles.menu_sim_voxels, 'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dose Grid        </FONT></u>';         set(handles.menu_sim_grid,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Score            </FONT></u>';         set(handles.menu_sim_tally,  'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Voxels           </FONT></u>';         set(handles.menu_voxels,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Density          </FONT></u>';         set(handles.menu_sim_dens,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Source           </FONT></u>';         set(handles.menu_source,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">NPS              </FONT></u>';         set(handles.menu_sim_nps,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">RUN MCNP         </FONT></u>';         set(handles.start_simulation,'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">RUN TOPAS        </FONT></u>';         set(handles.start_topas,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Resolution       </FONT></u>';         set(handles.change_res,      'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Disp. Mat. Dens. </FONT></u>';         set(handles.disp_mat_den,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Mat2Contour      </FONT></u>';         set(handles.mat2cont,        'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dose operations  </FONT></u>';         set(handles.d_operations,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Exp. Dose DCM    </FONT></u>';         set(handles.export_dose_dcm, 'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Delete dose      </FONT></u>';         set(handles.del_dose,        'Label',txt1);
%
txt1 = '<html><FONT color="black" size="6" face="Cambria">Treatment Plan   </FONT></u>';         set(handles.import_tretment_plan,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dose             </FONT></u>';         set(handles.import_dose,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">DECT             </FONT></u>';         set(handles.import_DECT,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">XY               </FONT></u>';         set(handles.view_xy,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">YZ               </FONT></u>';         set(handles.view_yz,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">XZ               </FONT></u>';         set(handles.view_xz,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">3D               </FONT></u>';         set(handles.view_3d_axes,           'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Catheters        </FONT></u>';         set(handles.view_catheters,         'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dwell Positions  </FONT></u>';         set(handles.view_dwell,             'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Structures       </FONT></u>';         set(handles.cont_transp,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dose             </FONT></u>';         set(handles.dose_transp,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Materials        </FONT></u>';         set(handles.mat_transp,             'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Set Limits       </FONT></u>';         set(handles.set_contrast_limits,    'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Restore Image    </FONT></u>';         set(handles.restore_contrast,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Restore Dose     </FONT></u>';         set(handles.restore_dosecontrast,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Settings 01      </FONT></u>';         set(handles.isod_01,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Settings 02      </FONT></u>';         set(handles.isod_02,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Settings 03      </FONT></u>';         set(handles.isod_03,                'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">User defined     </FONT></u>';         set(handles.phantom_user,           'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Whole CT         </FONT></u>';         set(handles.phantom_all,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">User defined     </FONT></u>';         set(handles.dosegrid_user,          'Label',txt1);

txt1 = '<html><FONT color="black" size="6" face="Cambria">Whole CT         </FONT></u>';         set(handles.dosegrid_all,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dw,m             </FONT></u>';         set(handles.score_dwm,          'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dm,m             </FONT></u>';         set(handles.score_dmm,          'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">DOSE             </FONT></u>';         set(handles.score_dose,         'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Mean Energy      </FONT></u>';         set(handles.score_energy,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Yes              </FONT></u>';         set(handles.vox_on,             'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">No               </FONT></u>';         set(handles.vox_off,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Library          </FONT></u>';         set(handles.dens_lib,           'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">CT               </FONT></u>';         set(handles.dens_ct,            'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">GammaMed Plus    </FONT></u>';         set(handles.source_gmp,         'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">MicroSelectron v2</FONT></u>';         set(handles.source_mselectronv2,'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">External beam    </FONT></u>';         set(handles.EB_source,          'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">no_source        </FONT></u>';         set(handles.no_source,          'Label',txt1);

%
%
txt1 = '<html><FONT color="black" size="6" face="Cambria">Workspace        </FONT></u>';         set(handles.workspace_menu,     'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">All              </FONT></u>';         set(handles.export_all,         'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Image            </FONT></u>';         set(handles.export_im,          'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Doses            </FONT></u>';         set(handles.export_doses,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Plan             </FONT></u>';         set(handles.export_plan,        'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Struct           </FONT></u>';         set(handles.export_st,          'Label',txt1);
%

%
txt1 = '<html><FONT color="black" size="6" face="Cambria">Figures          </FONT></u>';         set(handles.Untitled_21,        'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Image            </FONT></u>';         set(handles.exp_fig_image,      'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Dose             </FONT></u>';         set(handles.exp_fig_dose,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Histogram        </FONT></u>';         set(handles.exp_fig_hist,       'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Integral         </FONT></u>';         set(handles.exp_fig_integral,   'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Profiles         </FONT></u>';         set(handles.export_profile,     'Label',txt1);
%
%% External beam
txt1 = '<html><FONT color="black" size="6" face="Cambria">Show Isocenter    </FONT></u>';        set(handles.EB_show_isocenter,  'Label',txt1);

%% IrIS - these functios are not open source and only available for internal users


txt1 = '<html><FONT color="black" size="6" face="Cambria">IrIS             </FONT></u>';         set(handles.IrIS_menu,          'Label',txt1);
txt1 = '<html><FONT color="black" size="6" face="Cambria">Read Seq.        </FONT></u>';         set(handles.read_irIS_seq,      'Label',txt1);

%
% check = which('read_IrIS.m');
% if isempty(check)==1
    set(handles.IrIS_menu,'Visible','off');
% end
end