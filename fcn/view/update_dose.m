function [  ] = update_dose(fig,TPlan,current_slices,ax,alp,dose)
% update contours
%
% fig_r          =  findobj('Tag','amb_interface');
% TPlan          =  getappdata(fig_r,'TPlan');
% current_slices =  getappdata(fig_r,'slices');
%
%
if fig==1  || fig==4 % update all
    I(:,:,1)=TPlan.dose.(sprintf('%s',dose)).Dose(:,:,current_slices(1));       
%    set(ax.dos01,'CData',I,'AlphaData',alp*logical(I),'Tag','dose_xy'); 
    set(ax.dos01,'CData',I,'AlphaData',alp,'Tag','dose_xy');
end    
%
I=[];
if fig==2 || fig==4% update all
    I(:,:,1)=squeeze(TPlan.dose.(sprintf('%s',dose)).Dose(:,current_slices(2),:));  
%    set(ax.dos02,'CData',I,'AlphaData',alp*logical(I),'Tag','dose_yz'); 
    set(ax.dos02,'CData',I,'AlphaData',alp,'Tag','dose_yz'); 
end  
%
I=[];
if fig==3 || fig ==4
    I(:,:,1)=squeeze(TPlan.dose.(sprintf('%s',dose)).Dose(current_slices(3),:,:));     
%    set(ax.dos03,'CData',I,'AlphaData',alp*logical(I),'Tag','dose_xz');   
    set(ax.dos03,'CData',I,'AlphaData',alp,'Tag','dose_xz');
end  

end

