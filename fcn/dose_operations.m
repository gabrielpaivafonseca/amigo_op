%%% Create dose operation 
function dose_operations
   if isempty(isvalid(findobj('Name','dose_op')))==0
       delete(findobj('Name','dose_op'));
   end
   % main figure
   fi         = figure;  fi.Name    = 'dose_op'; 
   set(fi, 'Units', 'normalized', 'Position', [0.2,0.3,0.7,0.3],'menubar','none','Color',[1 1 1]);
   %
   % list menu - doses
   List_doses = uicontrol(fi,'Style','listbox','Units','normalized','Tag','l_info','Position',[0.01,0.1,0.175,0.85],'Str','');
   %
   handles = guihandles(findobj('Name','AMIGO')); TPlan=getappdata(handles.amb_interface,'TPlan');
   List_doses.String = handles.dose_menu.String(2:end);
   %
   %
   operation_list = uicontrol('Style','edit','String','#MyVariable$ =','FontSize',14,'Units','normalized','Position',[0.2 0.25 0.75 0.7]);
   %
   % pushbuttom to add dose  
   add            = uicontrol('Style','pushbutton','String','ADD','Units','normalized','Position',[0.45 0.1 0.2 0.1],'Callback',{@add_dose,List_doses,operation_list},'BackgroundColor','b','ForegroundColor','w');
   %
   % run operation  
   calculate      = uicontrol('Style','pushbutton','String','Calculate','Units','normalized','Position',[0.75 0.1 0.2 0.1],'Callback',{@calculate_dose,operation_list,handles},'BackgroundColor','r','ForegroundColor','w');
   %
   %
   drawnow;
%
end


function add_dose(varargin)
D_name = ['#' varargin{3}.String{varargin{3}.Value} '$ '];
% # will be replaced with full path later on
varargin{4}.String = [varargin{4}.String D_name];
end

function calculate_dose(varargin)
handles   = varargin{4};
TPlan     = getappdata(handles.amb_interface,'TPlan');
operation = strrep(varargin{3}.String,'#','TPlan.dose.');
operation = strrep(operation,'$','.Dose');
operation = [operation ';'];
%
try
   eval(operation);
catch
    errordlg('Check the expression');
    return;
end
%
%
f_0      = strfind(varargin{3}.String,'#');
f_1      = strfind(varargin{3}.String,'$');
var_name = strtrim(varargin{3}.String(f_0(1)+1:f_1-1));
%
TPlan.dose.(sprintf(var_name)).Dose(isnan(TPlan.dose.(sprintf(var_name)).Dose))=-999;
TPlan.dose.(sprintf(var_name)).Dose(isinf(TPlan.dose.(sprintf(var_name)).Dose))=-999;
%
setappdata(handles.amb_interface,'TPlan',TPlan);
close(findobj('name','dose_op'));
uiresume;
%
end