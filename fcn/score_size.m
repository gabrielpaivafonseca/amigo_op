function [ ] = score_size(varargin)
fig_r          =  findobj('Tag','amb_interface');
p=varargin{:};
score_pos=getappdata(fig_r,'score_region');
%
%
if isempty(getappdata(fig_r,'score_rec'))==1
    setappdata(fig_r,'score_rec',1);
end
%
h=gca;
% 
if p(1)<1; p(1)=1; end
if p(2)<1; p(2)=1; end
% drawnow;
if getappdata(fig_r,'score_rec')==1
   setappdata(fig_r,'score_rec',2); 
   %
   % update here
   if      strcmp(h.Tag,'amb_dos_01')
      score_pos.xy_pos=p;
      score_pos.yz_pos=[score_pos.yz_pos(1) p(2) score_pos.yz_pos(3) p(4)];
      score_pos.xz_pos=[score_pos.xz_pos(1) p(1) score_pos.xz_pos(3) p(3)];
      setPosition(score_pos.yz,score_pos.yz_pos);
      setPosition(score_pos.xz,score_pos.xz_pos);
   elseif  strcmp(h.Tag,'amb_dos_02')
      score_pos.xy_pos=[score_pos.xy_pos(1) p(2) score_pos.xy_pos(3) p(4)];
      score_pos.yz_pos=p;
      score_pos.xz_pos=[p(1) score_pos.xz_pos(2) p(3) score_pos.xz_pos(4)]; 
      setPosition(score_pos.xy,score_pos.xy_pos);
      setPosition(score_pos.xz,score_pos.xz_pos);
   elseif  strcmp(h.Tag,'amb_dos_03')
      score_pos.xy_pos=[p(2) score_pos.xy_pos(2) p(4) score_pos.xy_pos(4)];
      score_pos.yz_pos=[p(1) score_pos.yz_pos(2) p(3) score_pos.yz_pos(4)]; 
      score_pos.xz_pos=p;
      setPosition(score_pos.xy,score_pos.xy_pos);
      setPosition(score_pos.yz,score_pos.yz_pos);
   end
   %
   setappdata(fig_r,'score_rec',1);
end
setappdata(fig_r,'score_region',score_pos);
% save simulation data
TPlan=getappdata(fig_r,'TPlan');
TPlan.plan.dosegrid(1,1:2)=[score_pos.xy_pos(1) score_pos.xy_pos(1)+score_pos.xy_pos(3)]; 
TPlan.plan.dosegrid(2,1:2)=[score_pos.xy_pos(2) score_pos.xy_pos(2)+score_pos.xy_pos(4)];
TPlan.plan.dosegrid(3,1:2)=[score_pos.yz_pos(1) score_pos.yz_pos(1)+score_pos.yz_pos(3)];
TPlan.plan.dosegrid=round(TPlan.plan.dosegrid);
setappdata(fig_r,'TPlan',TPlan);