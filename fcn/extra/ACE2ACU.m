% get RS file
[File,Folder] = uigetfile({'RTS*.dcm;RS*.dcm'},'RS STruct');
n_rs          = dicominfo([Folder File]);
%
% get,RP file
[File,Folder] = uigetfile({'RTP*.dcm;RP*.dcm'},'RP Plan');
n_plan        = dicominfo([Folder File]);
%
% find out how many catheters and how many contours
%
% Correct dwell times
% Kerma=n_plan.SourceSequence.Item_1.ReferenceAirKermaRate/40700;
Kerma = 1;
%%
for i=1:size(fieldnames(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence),1)
%
ref_time = n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ChannelTotalTime / n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).FinalCumulativeTimeWeight;
n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ChannelTotalTime = n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ChannelTotalTime*ref_time;
cum_t=0;
n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).FinalCumulativeTimeWeight=n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ChannelTotalTime;
for j=1:n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).NumberOfControlPoints
    %
    if n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).CumulativeTimeWeight>cum_t
     ref   = n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).CumulativeTimeWeight;
     t     = ref - cum_t;
     cum_t = ref;
     if j==2
        n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).CumulativeTimeWeight = t*ref_time*Kerma;
     else
        n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).CumulativeTimeWeight = t*ref_time*Kerma ...
                    + n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j-2)).CumulativeTimeWeight; 
     end
    elseif j>1
        n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).CumulativeTimeWeight = n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j-1)).CumulativeTimeWeight;
    end
    %
    n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).ControlPointRelativePosition = ...
        n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.(sprintf('Item_%d',j)).ControlPointRelativePosition+3.5;
end
end



%
% contours
N_cont=size(fieldnames(n_rs.StructureSetROISequence),1);
%
N_Cat=0;
for i=1:size(fieldnames(n_plan.Private_300f_1000.Item_1.ROIContourSequence),1)
  if strcmp(n_plan.Private_300f_1000.Item_1.ROIContourSequence.(sprintf('Item_%d',i)).ContourSequence.Item_1.ContourGeometricType,'OPEN_NONPLANAR')==1 
    N_Cat=N_Cat+1;
    Cat_id(N_Cat)=i;
  end
end

% Add cat information to RS file
for i=1:N_Cat
  n_rs.StructureSetROISequence.(sprintf('Item_%d',N_cont+i)).ROINumber = N_cont+i+10;
  n_rs.StructureSetROISequence.(sprintf('Item_%d',N_cont+i)).ROIName   = ['Applicator' num2str(i)]; 
  n_rs.StructureSetROISequence.(sprintf('Item_%d',N_cont+i)).ReferencedFrameOfReferenceUID=n_plan.FrameOfReferenceUID;
  n_rs.StructureSetROISequence.(sprintf('Item_%d',N_cont+i)).ROIGenerationAlgorithm   = ''; 
  %
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ROIDisplayColor = [0;255;0];
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ReferencedROINumber= N_cont+i+10;
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourGeometricType  = 'OPEN_NONPLANAR';
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.NumberOfContourPoints = ...
      n_plan.Private_300f_1000.Item_1.ROIContourSequence.(sprintf('Item_%d',Cat_id(i))).ContourSequence.Item_1.NumberOfContourPoints;
  %
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData = ...
      n_plan.Private_300f_1000.Item_1.ROIContourSequence.(sprintf('Item_%d',Cat_id(i))).ContourSequence.Item_1.ContourData;
  %
  %%
  % add 3.5 mm to the tip
  c=reshape(n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData,3,[])';
  d=diff(c([2 1],:));
  %
  if i==1
     dist_ap=sqrt(sum((n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(1:3) ...
            - n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).BrachyControlPointSequence.Item_1.ControlPoint3DPosition).^2));
     dist_ap=dist_ap+3.5;
     %
     dist_ap=11.1;
  else
     dist_ap=3.5;
  end
%
dist_ap
  z_xy=d(3)/(sum(d.^2))^0.5;
  z=z_xy*dist_ap;
  %
  xy=sqrt(1-z_xy^2)*dist_ap;
  t_xy=d(2)/d(1);
  %
  x=cos(atan(t_xy))*xy;
  y=sin(atan(t_xy))*xy;
  %
  if d(1)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(1)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(1)-abs(x);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(1)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(1)+abs(x);
  end
  %
  if d(2)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(2)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(2)-abs(y);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(2)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(2)+abs(y);
  end
  %
  if d(3)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(3)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(3)-abs(z);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(3)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(3)+abs(z);
  end
  %
%   % end
  d=diff(c(end-1:end,:));
  %
  z_xy=d(3)/(sum(d.^2))^0.5;
  z=z_xy*15;
  %
  xy=sqrt(1-z_xy^2)*15;
  t_xy=d(2)/d(1);
  %
  x=cos(atan(t_xy))*xy;
  y=sin(atan(t_xy))*xy;
  %
  if d(1)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-2)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-2)-abs(x);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-2)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-2)+abs(x);
  end
  %
  if d(2)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-1)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-1)-abs(y);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-1)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end-1)+abs(y);
  end
  %
  if d(3)<0
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end)-abs(z);
  else
      n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end)=n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourData(end)+abs(z);
  end
  
  
  
  
  %%
  n_rs.ROIContourSequence.(sprintf('Item_%d',N_cont+i)).ContourSequence.Item_1.ContourImageSequence=n_rs.ROIContourSequence.Item_11.ContourSequence.Item_1.ContourImageSequence;
  %
  %
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ObservationNumber    = N_cont+i+10;
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ReferencedROINumber  = N_cont+i+10;
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).RTROIInterpretedType = 'BRACHY_CHANNEL';
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ROIInterpreter.FamilyName = '';
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ROIInterpreter.GivenName = '';
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ROIInterpreter.MiddleName = '';
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ROIInterpreter.NamePrefix = '';
  n_rs.RTROIObservationsSequence.(sprintf('Item_%d',N_cont+i)).ROIInterpreter.NameSuffix = '';
  
  %
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ReferencedROINumber=N_cont+i+10;
  
end
%%
%
n_rs.DeviceSerialNumber='78480665410';
n_rs=rmfield(n_rs,'ImplementationVersionName');
n_rs=rmfield(n_rs,'InstitutionalDepartmentName');
n_rs=rmfield(n_rs,'Private_3007_1070');
n_rs=rmfield(n_rs,'Private_3007_1071');
n_rs=rmfield(n_rs,'Private_3007_10xx_Creator');
%
n_rs.Manufacturer='Varian Medical Systems';
n_rs.ManufacturerModelName='ARIA RadOnc';
n_rs.SoftwareVersion='11.0.47';
n_rs.StationName='DS-VMSRS-01';
n_plan=rmfield(n_plan,'ImplementationVersionName');
n_plan=rmfield(n_plan,'TimezoneOffsetFromUTC');
n_plan=rmfield(n_plan,'InstitutionalDepartmentName');
n_plan=rmfield(n_plan,'Private_0021_1000');
n_plan=rmfield(n_plan,'Private_0021_10xx_Creator');
n_plan=rmfield(n_plan,'Private_3005_1008');
n_plan=rmfield(n_plan,'Private_3005_100d'); 
n_plan=rmfield(n_plan,'Private_3005_10xx_Creator');
n_plan=rmfield(n_plan,'Private_3007_1000');
n_plan=rmfield(n_plan,'Private_3007_1002');
n_plan=rmfield(n_plan,'Private_3007_1004');
n_plan=rmfield(n_plan,'Private_3007_100b');
n_plan=rmfield(n_plan,'Private_3007_100e');
n_plan=rmfield(n_plan,'Private_3007_100f');
n_plan=rmfield(n_plan,'Private_3007_1011');
n_plan=rmfield(n_plan,'Private_3007_1013');
n_plan=rmfield(n_plan,'Private_3007_1014');
n_plan=rmfield(n_plan,'Private_3007_10xx_Creator');
n_plan=rmfield(n_plan,'Private_300b_1035');
n_plan=rmfield(n_plan,'Private_300b_1036'); 
%n_plan=rmfield(n_plan,'Private_300b_1037');
%n_plan=rmfield(n_plan,'Private_300b_10xx_Creator');
n_plan=rmfield(n_plan,'Private_300f_1000');
n_plan=rmfield(n_plan,'Private_300f_10xx_Creator');
n_plan=rmfield(n_plan,'ReviewDate');
n_plan=rmfield(n_plan,'ReviewTime');
n_plan.TreatmentMachineSequence.Item_1=rmfield(n_plan.TreatmentMachineSequence.Item_1,'Private_300b_10xx_Creator'); 
n_plan.TreatmentMachineSequence.Item_1=rmfield(n_plan.TreatmentMachineSequence.Item_1,'Private_300b_1005'); 
n_plan.ApplicationSetupSequence.Item_1=rmfield(n_plan.ApplicationSetupSequence.Item_1,'Private_300b_10xx_Creator');
n_plan.ApplicationSetupSequence.Item_1=rmfield(n_plan.ApplicationSetupSequence.Item_1,'Private_300b_1002');
n_plan.ApplicationSetupSequence.Item_1=rmfield(n_plan.ApplicationSetupSequence.Item_1,'Private_300b_1009');
n_plan.ApplicationSetupSequence.Item_1=rmfield(n_plan.ApplicationSetupSequence.Item_1,'Private_300b_1030');
n_plan.ApplicationSetupSequence.Item_1=rmfield(n_plan.ApplicationSetupSequence.Item_1,'Private_300b_103b');
%
n_plan.TransferSyntaxUID      = '1.2.840.10008.1.2';
n_plan.ImplementationClassUID = '1.2.246.352.70.2.1.7';
n_plan.Manufacturer           = 'Varian Medical Systems';
n_plan.StationName            = 'DS-VMSRS-01';
n_plan.SeriesDescription      = 'ARIA RadOnc Plans';
n_plan.ApprovalStatus         = 'UNAPPROVED';
n_plan.DeviceSerialNumber     = '78480665410';
n_plan.ManufacturerModelName  = 'ARIA RadOnc';
n_plan.PlanIntent             = 'CURATIVE';
n_plan.SoftwareVersion        = '11.0.47';
n_plan.ApplicationSetupSequence.Item_1.ApplicationSetupType   = 'OTHER';
n_plan.ApplicationSetupSequence.Item_1.ApplicationSetupNumber = 1;
n_plan.ApplicationSetupSequence.Item_1.TotalReferenceAirKerma = 5.652777777382270e+02;
n_plan.ApplicationSetupSequence.Item_1.ApplicationSetupName   = n_plan.RTPlanLabel;
n_plan.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1.ReferencedBrachyApplicationSetupNumber=1;
n_plan.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1.Private_3249_10xx_Creator='Varian Medical Systems VISION 3249';
n_plan.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1=rmfield(n_plan.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1,'BrachyApplicationSetupDose');

%% --------------------------------------------------------------------------------------------
% the part above should be fine regardlees the tps configuration 
% the lines below may change - still need to identify where

n_plan.TreatmentMachineSequence.Item_1.Manufacturer      = 'Varian Medical Systems';
n_plan.TreatmentMachineSequence.Item_1.InstitutionName   = 'MAASTRO clinic';
n_plan.TreatmentMachineSequence.Item_1.InstitutionAddress= 'Maastricht';
n_plan.TreatmentMachineSequence.Item_1.ManufacturerModelName = 'GammaMedPlus';
n_plan.TreatmentMachineSequence.Item_1.DeviceSerialNumber= '640869';
n_plan.TreatmentMachineSequence.Item_1.TreatmentMachineName= 'Dublin';
%
%
for i=1:size(fieldnames(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence),1)
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ChannelLength=1300;
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).SourceApplicatorNumber=i;
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).SourceApplicatorID=['Applicator' num2str(i)];
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'SourceApplicatorName');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'TransferTubeLength');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_10xx_Creator');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_1000');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_100e');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_1013');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_1027');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i))=rmfield(n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)),'Private_300b_1032');
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).SourceApplicatorLength=1300;
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).TransferTubeNumber=[];
  n_plan.ApplicationSetupSequence.Item_1.ChannelSequence.(sprintf('Item_%d',i)).ReferencedSourceNumber=1;
  %
end
%
%
n_plan = orderfields(n_plan);
%rp_v   = orderfields(rp_v);
%rs_v=orderfields(rs_v);
n_rs=orderfields(n_rs);
%
%
n_plan.SourceSequence.Item_1.SourceNumber=1;
n_plan.SourceSequence.Item_1.SourceIsotopeName='GammaMed Plus HDR source 0.9 mm';
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'SourceStrengthUnits');
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'Private_300b_10xx_Creator');
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'Private_300b_1006');
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'Private_300b_1007');
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'Private_300b_1008');
n_plan.SourceSequence.Item_1=rmfield(n_plan.SourceSequence.Item_1,'Private_300b_100c');
n_plan.SourceSequence.Item_1.SourceManufacturer='Varian Medical Systems';
n_plan.SourceSequence.Item_1.SourceStrengthReferenceTime='000000';
n_plan.SourceSequence.Item_1.ReferenceAirKermaRate=40700;
n_plan.SourceSequence.Item_1.ActiveSourceDiameter=1;
n_plan.SourceSequence.Item_1.ActiveSourceLength=3.5;
%
%
%
%
% ApplicationSetupSequence
% DoseReferenceSequence
% rp_v.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1.Private_3249_1010
% %
%
%dicomwrite([],[Folder 'RS_11_new.dcm'],n_rs,'CreateMode','Create','WritePrivate',1);
dicomwrite([],[Folder 'RS_11_new.dcm'],n_rs,'CreateMode','Copy','WritePrivate',1);
%
n_rs=dicominfo([Folder 'RS_11_new.dcm']);
%
%
n_plan.ReferencedStructureSetSequence.Item_1.ReferencedSOPClassUID    = n_rs.MediaStorageSOPClassUID;
n_plan.ReferencedStructureSetSequence.Item_1.ReferencedSOPInstanceUID = n_rs.MediaStorageSOPInstanceUID;

dicomwrite([],[Folder 'RP_11_new.dcm'],n_plan,'CreateMode','Copy','WritePrivate',1);

