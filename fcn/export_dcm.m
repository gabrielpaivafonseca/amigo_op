handles = guihandles(findobj('Tag','amb_interface')); TPlan=getappdata(handles.amb_interface,'TPlan');
%
%
[p,~] = listdlg('PromptString','Export dose:',  'Selectionmode','multiple','ListString',handles.dose_menu.String(2:end));
p     = p+1;
%
%
TPlan.dose.DCM_info.ImagePositionPatient   =   TPlan.image.ImagePositionPatient;
TPlan.dose.DCM_info.PixelSpacing(1:2)      =   TPlan.image.Resolution(1:2);
TPlan.dose.DCM_info.SliceThickness         =   TPlan.image.Resolution(3);
%
%
[file, folder] = uiputfile('*.dcm');
%
for i=1:size(p,2)
    D                                          = TPlan.dose.doses.(sprintf(handles.dose_menu.String{p(i)})); 
    GridFrameVectorMax                         = size(D,3)-1;
    TPlan.dose.DCM_info.GridFrameOffsetVector  = 0:GridFrameVectorMax;
    fc                                         = (2^32/max(D(:)));
    D                                          = D*fc;
    TPlan.dose.DCM_info.DoseGridScaling        = 1/fc;
    D                                          = uint32(D);
    D                                          = reshape(D,[size(D,1) size(D,2) 1 size(D,3)]);
    %
    dicomwrite(D,[folder file(1:end-4) '_' (sprintf(handles.dose_menu.String{p(i)})) '.dcm'],TPlan.dose.DCM_info,'CreateMode','copy','MultiframeSingleFile', 'true');
end

%