classdef import_treatmentplan_V2 
   properties
      image      = [];
      plan       = [];
      dose       = []; 
      struct     = [];
      parameters = [];
      DECT       = [];
      CT4D       = [];
      Studies    = [];
      IrIS       = [];
   end
   methods
      function obj = import_treatmentplan_V2(varargin)
         % user select one DICOM or IMA file within the folder and AMIGO tries to open
         % the whole folder.
         if nargin == 0
            [file,nfolder]=uigetfile({'*.dcm;*.AMB_plan;*.IMA;*.egsphant;*.g4dcm;*.bin;*.inp;*.o'},'Select one file to open the whole folder'); 
            
            if isnumeric(nfolder)==1; obj.image=-99; return; end
         elseif nargin == 1
             nfolder=varargin{1};
         elseif nargin == 2 && strcmp(varargin{2},'4DCT')==1
             % select mutiple folders 4DCT
             
             %
         elseif nargin == 2 && strcmp(varargin{2},'IrIS')==1
             tic
             obj = read_IrIS_amb(obj);
             toc
             return;
         end
         %
         % UPDATE wait bar
         w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
         set(w_bar.hand,'Visible',1);
         
         % Check individual files first
         %
         % AMIGO project
         if  size(file,2)>8 && strcmp(file(end-7:end),'AMB_plan')==1
             set(w_bar.hand,'Value',25);
             load([nfolder file],'-mat');
             obj.image                 = TPlan.image;
             obj.plan                  = TPlan.plan;
             obj.dose                  = TPlan.dose;
             obj.struct                = TPlan.struct;
             obj.image.load.ed_all_mat = ed_all_mat;
             obj.image.load.mat_menu   = mat_menu;
             obj.image.load.mat_limits = mat_limits;
             setappdata(gcf,'phantom_region',phantom_pos);
             setappdata(gcf,'score_region',score_pos);
             set(w_bar.hand,'Value',75);
             if isfield(TPlan.dose,'Dose_TPS')==1
                 obj.dose.doses.Dose_TPS=TPlan.dose.Dose_TPS;
                 obj.dose=rmfield(obj.dose,'Dose_TPS');
             end
             return;
         %
         % EGSPhant - material and density map used by some MC codes
         elseif size(file,2)>8 && strcmp(file(end-7:end),'egsphant')
             set(w_bar.hand,'Value',25);
             obj=import_egsphant(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % G4DCM - material and density map used by some MC codes
         elseif size(file,2)>5 && strcmp(file(end-4:end),'g4dcm')
             set(w_bar.hand,'Value',25);
             obj=import_g4dcm(obj,nfolder);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % MOBY phantom    
         elseif strcmp(file(end-2:end),'bin')         % Moby
             set(w_bar.hand,'Value',25);
             obj=import_bin(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % MCNP input or output
         elseif (size(file,2)>2 && strcmp(file(end-1:end),'.o')==1) || ...
            (size(file,2)>8 && strcmp(file(end-3:end),'.inp')==1)
         %
            set(w_bar.hand,'Value',25);
            obj=import_mcinp(obj,[nfolder file]);
            set(w_bar.hand,'Value',75);
            obj.struct.contours.(sprintf('Item_%.0f',1)).Name = 'Edit';
            obj.struct.contours.(sprintf('Item_%.0f',1)).Mask = logical(zeros(obj.image.Nvoxels));
            return;  
         end
        % -------------------------------------------------------------------------------------------
        %%  Check the content of the folder (DCM, Series, RTStruct, RTPlan, etc)
        %
        set(findobj('Tag','text4'),'String','Checking file headers ... Can take up to 1 min for proton plans'); drawnow;
        Files = check_nfolder(nfolder);
        %        Check the content of the folder (DCM, Series, RTStruct, RTPlan,
        %        etc)  
        %
        % check if field names are valid ... TPS is more flexible than
        % matlab
        PT      = unique(Files(:,2));         % number of patients
        % 
        %
        for k =1:size(PT,1)
           IDX     = find(ismember(Files(:,2),PT{k})); 
           %
           % is valid name? 
           if isvarname(['P' PT{k}])==0
               name = PT{k};
               name = strrep(name,' ','');
               name = strrep(name,':','_');
               name = strrep(name,'-','_');
               name = strrep(name,'.','_');
               name = strrep(name,'(','_');
               name = strrep(name,')','_');
               name = strrep(name,'[','_');
               name = strrep(name,']','_');
               % 
               % fixed?
               if isvarname(['P' name])==0 % no :(
                   name = ['Ser_' num2str(k)];
               end
               %
               for i = 1:length(IDX)
                   Files{i,2} = name;
               end
           end
           %
           
           % 
        end
        % ------------------------------------------------------------------------------------------
        %% -----------------------------------------------------------------------------------------
        % Read image files
        set(findobj('Tag','text4'),'String','Opening images ...'); drawnow;
        obj = read_images(obj,Files,nfolder);
        %
        set(findobj('Tag','text4'),'String','Opening RTPLAN ... '); drawnow;
        obj = read_rtplan(obj,Files);
        %  
        set(findobj('Tag','text4'),'String','Opening RTSTRUCT ...'); drawnow;
        obj = read_struct_files(obj,Files);
        %
        set(findobj('Tag','text4'),'String','Opening RTDoses ... Almost there'); drawnow;
        obj = read_dose_files(obj,Files);
        %
        % Get the first studie to display ... AMIGO will give the user the
        % option to select a different study.
        st = fieldnames(obj.Studies);
        if isfield(obj.Studies.(sprintf(st{1})),'image')==1
           obj.image  = obj.Studies.(sprintf(st{1})).image;
        end
        %
        if isfield(obj.Studies.(sprintf(st{1})),'struct')==1
            obj.struct = obj.Studies.(sprintf(st{1})).struct;
        end
        %
        if isfield(obj.Studies.(sprintf(st{1})),'plan')==1
            obj.plan  = obj.Studies.(sprintf(st{1})).plan;
        end
        %
        if isfield(obj.Studies.(sprintf(st{1})),'dose')==1
            obj.dose  = obj.Studies.(sprintf(st{1})).dose;
        end
        % If there is only one study ... make it default and delete the
        % information from studies 
        if length(st) == 1
            obj.Studies = [];
        end
      % -----------------------------------------------------------------------------------------------------------------------
      % Create PointVox and Dwell voxes to display catheters and dw pos
          if isfield(obj.plan,'catheter')==1
             ncat=fieldnames(obj.plan.catheter);
             for i=1:length(ncat)  
               if isfield(obj.plan.catheter.(sprintf('%s',ncat{i})),'Points')==1
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = bsxfun(@minus,  obj.plan.catheter.(sprintf('%s',ncat{i})).Points, ... 
                                                                                           obj.image.ImagePositionPatient);
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = bsxfun(@rdivide,obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox, ...
                                                                                           obj.image.Resolution);
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox = obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox+1;
                     obj.plan.catheter.(sprintf('%s',ncat{i})).PointsEd  = obj.plan.catheter.(sprintf('%s',ncat{i})).PointsVox;                                                                  
                end
             end
         end
         % dwell voxels coordinates
         if isfield(obj.plan,'dwells')==1
             obj.plan.dwellVox        = bsxfun(@minus,  obj.plan.dwells(:,1:3), obj.image.ImagePositionPatient);
             obj.plan.dwellVox        = bsxfun(@rdivide,obj.plan.dwellVox,      obj.image.Resolution); 
             obj.plan.dwellVox        = obj.plan.dwellVox+1;
             obj.plan.dwellVox(:,4:5) = obj.plan.dwells(:,4:5);
         end
      % -----------------------------------------------------------------------------------------------------------------------
 
      end
   
      %
      %
       function obj = import_egsphant(obj,filename)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% EGSPhantFile - check
            % verify the number of materials
            fid=fopen(filename);
            Mat = fscanf(fid, '%d', [1 1]);
            % get material list
            for i=1:Mat+1
               Mat_names{i,1}=fgetl(fid); %#ok<*NASGU>
            end
            % ignore the next lines (1 values)
            fgetl(fid);
            %
            N_voxel=str2num(fgetl(fid));
            %
            def_pos = ftell(fid);
            %
            % there are two types of Egsphant  (with and without space between the material numbers) 
            %
            if   isempty(fgetl(fid))==1 % egsphant withoput space
                Eg_type=0;
            else
                %
                fseek(fid,def_pos,'bof');
                Eg_type=1;
                %
            end
            while size(N_voxel,2)~=3
                N_voxel=str2num(fgetl(fid));
            end

            % Chenge reference to AMB_Format
            obj.image.Nvoxels(1:3)=[N_voxel(2) N_voxel(1) N_voxel(3)]; 
            %
            % get the voxel resolution
            R = fscanf(fid, '%f %f', [1 2]); fgetl(fid);
            obj.image.Resolution(1)=(R(1,2)-R(1,1))*10;     % mm
            %
            obj.image.ImagePositionPatient(1)=R(1,1)*10;  
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(2)=(R(1,2)-R(1,1))*10;     % mm
            obj.image.ImagePositionPatient(2)=R(1,1)*10;
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.ImagePositionPatient(3)=R(1,1)*10;  
            obj.image.Resolution(3)=(R(1,2)-R(1,1))*10;     % mm
            %
          obj.plan.Mat_HU=zeros(obj.image.Nvoxels);  
          for k=1:N_voxel(3)
            for i=1:N_voxel(2)
             a=fgetl(fid);
             if isinf(str2num(a))==1 || Eg_type==0
               obj.plan.Mat_HU(i,:,k)=str2num(a(:));
             else
               obj.plan.Mat_HU(i,:,k)=str2num(a);  
             end  
            end
            fgetl(fid);
          end
          %
          %
          obj.plan.Density=zeros(obj.image.Nvoxels); 
          for k=1:N_voxel(3)
            for i=1:N_voxel(2)   
             obj.plan.Density(i,:,k)=str2num(fgetl(fid));
            end
            fgetl(fid);
          end
          fclose('all');
          obj.plan.Mat_HU  = flip(obj.plan.Mat_HU,1);
          obj.plan.Density = flip(obj.plan.Density,1);
          obj.image.Image  = obj.plan.Mat_HU;
        end
      %%
        function   obj = import_bin(obj,fname)
            fid = fopen(fname,'r');
            s   = dir(fname);
            %
            obj.image.Nvoxels(1:2) = [256 256];
            obj.image.Nvoxels(3)   = (s.bytes/(256*256*4)*4)/4;
            %
            prompt      = {'Enter matrix size (separeted with spaces):','Pixel Slice (cm):'};
            dlg_title   = 'Input';
            num_lines   = 1;
            defaultans  = {['256 256 ' num2str(obj.image.Nvoxels(3))],'0.0145 0.0145'};
            answer      = inputdlg(prompt,dlg_title,num_lines,defaultans);
            %
            obj.image.Nvoxels(1:3)         = str2num(answer{1});
            %
            Res = str2num(answer{2});
            %
            obj.image.Resolution(1)        = Res(1);
            obj.image.Resolution(2)        = Res(1);
            obj.image.Resolution(3)        = Res(2);
            obj.image.ImagePositionPatient = [0,0,0];
            %
            obj.image.Image                = zeros(obj.image.Nvoxels);
            for i=1:obj.image.Nvoxels(3)
               obj.image.Image(:,:,i)  = fread(fid,[256 256],'*real*4','l');
            end
            fclose(fid);
        end
      %% 
       function   obj = import_mcinp(obj,fname)
            fid = fopen(fname,'r');
            while ~feof(fid)
                a=fgetl(fid);
                % find image size
                if contains(a,'fill ')==1
                    a(1:strfind(a,'fill ')+4) = [];
                    a                         = strrep(a,':',' ');
                    dim                       = str2num(a(1:end-1));
                    obj.image.Nvoxels(1)      = dim(2)-dim(1)+1;
                    obj.image.Nvoxels(2)      = dim(4)-dim(3)+1;
                    obj.image.Nvoxels(3)      = dim(6)-dim(5)+1;
                    % Create variables
                    MCNP.Uni                  = zeros(obj.image.Nvoxels);
                    obj.plan.Mat_HU           = zeros(obj.image.Nvoxels);
                    obj.plan.Density          = zeros(obj.image.Nvoxels);
                    % read all voxels
                    cont=1; % voxel ID
                    while cont<=obj.image.Nvoxels(1)*obj.image.Nvoxels(2)*obj.image.Nvoxels(3)
                        a=fgetl(fid); a=strsplit(a);
                        for i=1:size(a,2)
                            if isempty(a{i})==0 && strcmp(a{i}(end),'r')==0 && strcmp(a{i}(end),'&')==0 ...
                                    && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0) % new uni
                                uni=str2num(a{i});
                                MCNP.Uni(cont)=uni;
                                cont=cont+1;
                            elseif isempty(a{i})==0 && strcmp(a{i}(end),'r')==1  ...
                                    && (strcmp(a{i}(end),'c')==0 || strcmp(a{i}(end),'&')==0)                           % repetition
                                r = str2num(a{i}(1:end-1));
                                MCNP.Uni(cont:cont+r-1)=uni;
                                cont=cont+r;
                            end       
                        end
                    end
                    %
                    fgetl(fid);fgetl(fid);
                    % read universes - mat and dens
                    a=fgetl(fid); a=strsplit(a);
                    while isempty(a{1})==1
                       uni=str2num(a{11}(3:end));
                       mat=str2num(a{3});
                       den=abs(str2num(a{4}));
                       obj.plan.Mat_HU(MCNP.Uni==uni) =mat;
                       obj.plan.Density(MCNP.Uni==uni)=den;
                       a=fgetl(fid); a=strsplit(a);
                    end
                end
                %
                if contains(a,'c  Lattice')==1
                  for i=1:3
                    a = fgetl(fid); b(1) = str2num(a(11:end));
                    a = fgetl(fid); b(2) = str2num(a(11:end));
                    %
                    obj.image.ImagePositionPatient(i) = b(1);
                    obj.image.Resolution(i)           = b(2)-b(1);
                    %
                  end
                  break;
                end
                
                %
            end
            %
            %
            obj.image.Image = obj.plan.Mat_HU;
            %
            fclose(fid);
        end
      
      %%
        function obj = import_g4dcm(obj,folder)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% G4dcm isabel's code
        fi=dir([folder filesep '*.g4dcm']);
        %
        for sli=1:size(fi,1)
          % verify the number of materials
          fid=fopen([folder filesep fi(sli).name]);
          if sli==1
            Mat = fscanf(fid, '%d', [1 1]);
            % get material list
            fgetl(fid);
            for i=1:Mat
               Mat_names{i,1}=fgetl(fid); %#ok<*NASGU>
            end
            %
            N_voxel=str2num(fgetl(fid));
            while size(N_voxel,2)~=3
                N_voxel=str2num(fgetl(fid));
            end
            %
            N_voxel(3)=size(fi,1);
            %
            % Chenge reference to AMB_Format
            obj.image.Nvoxels(1:3)=[N_voxel(2) N_voxel(1) N_voxel(3)]; 
            %
            % get the voxel resolution
            R = fscanf(fid, '%f %f', [1 2]); fgetl(fid);
            obj.image.Resolution(1)=diff(R)/N_voxel(1);     % mm
            %
            obj.image.ImagePositionPatient(1)=R(1,1);  
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(2)=diff(R)/N_voxel(2);     % mm
            obj.image.ImagePositionPatient(2)=R(1,1);
            %
            R = fscanf(fid, '%f %f', [1 2]);fgetl(fid);
            obj.image.Resolution(3)=diff(R);     % mm
            obj.image.ImagePositionPatient(3)=R(1,1);  
            %
            Im=zeros(N_voxel(1)*2,N_voxel(1),N_voxel(3));
          else
            for i=1:Mat+5
                fgetl(fid);
            end
          end
          Im(:,:,sli)=cell2mat(textscan(fid,repmat('%f',[1,N_voxel(2)]),'CollectOutput',1));
          fclose('all');
        end
         % obj.plan.Mat_HU  = flip(obj.plan.Mat_HU,1);
         % obj.plan.Density = flip(obj.plan.Density,1);
          obj.plan.Mat_HU  = Im(1:size(Im,1)/2,:,:);
          obj.plan.Density = Im(size(Im,1)/2+1:end,:,:);
          obj.image.Image  = obj.plan.Mat_HU;
          clear Im;
        end
        %
        % 
   end
end