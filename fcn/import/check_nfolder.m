% check all files in the folder ... dicominfo ... returns a table with
% relevant information
function Files = check_nfolder(nfolder)
     list_01 = dir([nfolder '*.dcm']); 
     list_02 = dir([nfolder '*.ima']);
     list    = [list_01',list_02']';
     Files   =cell(length(list),36);
     %
     if isempty(findobj('Tag','amb_interface'))==0
        w_bar.hand = getappdata(findobj('Tag','amb_interface'),'waitbarjava');
        set(w_bar.hand,'Visible',1,'Value',0);
        
     else
        w_bar=[];
     end
     %
     for i=1:length(list)
         if isempty(w_bar)==0
            set(w_bar.hand,'Visible',1,'Value',i/length(list)*100);
         end
         info = dicominfo([nfolder list(i).name]);
         %
         Files{i,1}  = [list(i).name];
         %
         if isfield(info,'PatientID')==1;                 Files{i,2}  = info.PatientID;                                                   end
         if isfield(info,'Modality')==1;                  Files{i,3}  = info.Modality;                                                    end
         if isfield(info,'StudyID')==1                    Files{i,4}  = info.StudyID;                                                     end
         if isfield(info,'SeriesNumber')==1               Files{i,5}  = info.SeriesNumber;                                                end
         if isfield(info,'ImagePositionPatient')==1;      Files{i,6}  = info.ImagePositionPatient;                                        end
         if isfield(info,'StudyDescription')==1;          Files{i,7}  = info.StudyDescription;                                            end
         if isfield(info,'SeriesDescription')==1;         Files{i,8}  = info.SeriesDescription;                                           end              
         if isfield(info,'SeriesInstanceUID') == 1        Files{i,9}  = info.SeriesInstanceUID;                                           end 
         if isfield(info,'StudyInstanceUID')  == 1        Files{i,10} = info.StudyInstanceUID;                                            end 
         if isfield(info,'SOPClassUID')==1;               Files{i,11} = info.SOPClassUID;                                                 end
         if isfield(info,'MediaStorageSOPInstanceUID')==1 Files{i,12} = info.MediaStorageSOPInstanceUID;                                  end
         if isfield(info,'FrameOfReferenceUID')==1        Files{i,13} = info.FrameOfReferenceUID;                                         end
         if isfield(info,'Manufacturer')==1;              Files{i,14} = info.Manufacturer;                                                end
         % Image info
         if isfield(info,'SliceThickness')==1;            Files{i,15} = info.SliceThickness;                                              end
         if isfield(info,'Width')==1;                     Files{i,16} = info.Width;                                                       end
         if isfield(info,'Height')==1;                    Files{i,17} = info.Height;                                                      end
         if isfield(info,'RescaleIntercept')==1;          Files{i,18} = info.RescaleIntercept;                                            end
         if isfield(info,'RescaleSlope')==1;              Files{i,19} = info.RescaleSlope;                                                end
         if isfield(info,'InstanceNumber')==1;            Files{i,20} = info.InstanceNumber;                                              end
         if isfield(info,'ImageOrientationPatient')==1;   Files{i,21} = info.ImageOrientationPatient;                                     end
         if isfield(info,'KVP')==1;                       Files{i,22} = info.KVP;                                                         end
         if isfield(info,'XrayTubeCurrent')==1;           Files{i,23} = info.XrayTubeCurrent;                                             end
         if isfield(info,'FilterType')==1;                Files{i,24} = info.FilterType;                                                  end
         if isfield(info,'ConvolutionKernel')==1;         Files{i,25} = info.ConvolutionKernel;                                           end
         if isfield(info,'ReconstructionDiameter')==1;    Files{i,26} = info.ReconstructionDiameter;                                      end
         if isfield(info,'PatientPosition')==1;           Files{i,27} = info.PatientPosition;                                             end
         if isfield(info,'TableSpeed')==1;                Files{i,28} = info.TableSpeed;                                                  end
         if isfield(info,'SpiralPitchFactor')==1;         Files{i,29} = info.SpiralPitchFactor;                                           end
         %
         if isfield(info,'PixelSpacing')==1;              Files{i,30} = info.PixelSpacing;                                                end
         if isfield(info,'SliceThickness')==1;            Files{i,31} = info.SliceThickness;                                              end
         if isfield(info,'SpacingBetweenSlices')==1;      Files{i,32} = info.SpacingBetweenSlices;                                        end
         if isfield(info,'FractionGroupSequence')==1      Files{i,33} = info.FractionGroupSequence.Item_1.NumberOfFractionsPlanned;       end
         %
         if isfield(info,'DoseSummationType') == 1        Files{i,34} = info.DoseSummationType;                                           end
         if isfield(info,'NumberOfFrames') == 1           Files{i,35} = info.NumberOfFrames;                                              end
         %
         % store plan info to process later ... no need to read info 2x
         if strcmpi(Files{i,3},'RTPlan') ==1             Files{i,36}  = info;                                                             end
         % store struct info to process later ... no need to read info 2x
         if strcmpi(Files{i,3},'RTStruct') ==1           Files{i,36}  = info;                                                             end
         % store dose info to process later ... no need to read info 2x
         if strcmpi(Files{i,3},'RTDOSE') ==1             Files{i,36}  = info;                                                             end
         %
         Files{i,37}  = nfolder;      
     end    
end