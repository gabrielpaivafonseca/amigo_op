function obj = read_rtplan(obj,Files)
%   Check how many studies and series 
        PT      = unique(Files(:,2));         % number of patients
        %
        %% ---------------------------------------------------------------------------------------------------------------
        % RS plan might come from aria and with different ID so it won't
        % match ... this is a workaround that assumes the user is loading
        % just one patient and one plan
        %
        if isempty(find(ismember(Files(:,3),'RTPLAN')))==0 && length(find(ismember(Files(:,3),'RTPLAN')))==1 && strcmpi(Files{find(ismember(Files(:,3),'RTPLAN')),14},'RaySearch Laboratories')==1 && size(PT,1) == 2 && size(find(ismember(Files(:,3),'RTPLAN')),1)==1
          PT(find(ismember(PT,Files{find(ismember(Files(:,3),'RTPLAN')),2})))=[];
          Files{find(ismember(Files(:,3),'RTPLAN')),2}                       = PT{1};
        end
        %%
        %
        % Read plan files
        for k =1:size(PT,1) % 
            % 
            % get only files related within the same study
            IDX = find(ismember(Files(:,2),PT{k})); 
            st  = Files(IDX,:);
            % add additional "for" in case of multiple series
            % SE  = unique(cell2mat(Files(:,5))); % number of series per study
            %
            % Check modality and open STRUCT images
            IDX = find(ismember(st(:,3),'RTPLAN'));
            %
            if isempty(IDX)==1; continue; end 
            %
            info = st{IDX,36};
            %
            for i = 1:length(IDX)
              %  
              % Brachy plan ? 
              if isfield(info,'SourceSequence') 
               obj.Studies.(sprintf(['P' PT{k}])).plan = read_brachy_file(st{IDX,36});
              end
              % 
              % EBRT - Photons plan
              if isfield(info,'BeamSequence')==1
               obj.Studies.(sprintf(['P' PT{k}])).plan = read_linac_file(st{IDX,36});
              end
              % Proton ?
              if strcmpi(Files{IDX(i),14},'RaySearch Laboratories')==1
                     obj.Studies.(sprintf(['P' PT{k}])).plan            = read_mevion_rt_plan(info,1);
                     obj.Studies.(sprintf(['P' PT{k}])).plan.modality   = 'Protons';
                     p                                                  = obj.Studies.(sprintf(['P' PT{k}]));
                     obj.Studies.(sprintf(['P' PT{k}]))                 = combine_plan(p);
                     obj.Studies.(sprintf(['P' PT{k}])).plan.pat_model  = struct;
                     %
                     if isfield(info,'DoseReferenceSequence')==1
                       obj.Studies.(sprintf(['P' PT{k}])).plan.presc_dose = info.DoseReferenceSequence.Item_1.TargetPrescriptionDose;
                     end
                     %
                     if isfield(info,'FractionGroupSequence')==1
                       obj.Studies.(sprintf(['P' PT{k}])).plan.fractions = info.FractionGroupSequence.Item_1.NumberOfFractionsPlanned;
                     end
                     %
               end
                  %
              %
            end
        end
        % 
        
        
end   

%% EBRT - Photons
     %% import linac plan
     function plan =  read_linac_file(EB_plan)
            %
            % import patient model
            load('model_pat.mat');
            plan.pat_model.face=face;
            plan.pat_model.vertex=vertex;
            % Number of beam
            %
            plan.NBeams  =  size(fieldnames(EB_plan.BeamSequence),1);
            cont=1;
            for i=1:plan.NBeams
             %
             plan.BEAM_info{cont}=['Beam ' num2str(i)]; 
             cont=cont+1;
             %
             plan.(sprintf('Beam_%d',i)).BeamNumber                    =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamNumber;
             plan.(sprintf('Beam_%d',i)).BeamName                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamName;
             %
             plan.(sprintf('Beam_%d',i)).BeamType                      =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamType;
             plan.(sprintf('Beam_%d',i)).NumberOfControlPoints         =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).NumberOfControlPoints;
             %
             plan.(sprintf('Beam_%d',i)).ControlPointSequence          =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence;
             %
             plan.(sprintf('Beam_%d',i)).SourceAxisDistance            =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).SourceAxisDistance;
             %
             if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence,'Item_3')==1
                plan.(sprintf('Beam_%d',i)).LeafPositionBoundaries     =  EB_plan.BeamSequence.(sprintf('Item_%d',i)).BeamLimitingDeviceSequence.Item_3.LeafPositionBoundaries;
             end
             %
             plan.(sprintf('Beam_%d',i)).PatientPosition               =  EB_plan.PatientSetupSequence.(sprintf('Item_%d',i)).PatientPosition;
             plan.(sprintf('Beam_%d',i)).SetupTechnique                =  EB_plan.PatientSetupSequence.(sprintf('Item_%d',i)).SetupTechnique;
             %
             for j=1:plan.(sprintf('Beam_%d',i)).NumberOfControlPoints
                  %
                  plan.BEAM_info{cont}=['  Control Point   ' num2str(j)]; cont=cont+1;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw01   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_1.LeafJawPositions;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Jaw03   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_2.LeafJawPositions;
                  %
                  if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence,'Item_3')==1
                    plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).MLC   = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDevicePositionSequence.Item_3.LeafJawPositions;
                  end
                  %
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).NominalBeamEnergy          = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.NominalBeamEnergy;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).DoseRateSet                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.DoseRateSet;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryAngle                = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryAngle;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).GantryRotationDirection    = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.GantryRotationDirection;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).CollimatorAngle            = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.BeamLimitingDeviceAngle;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).Isocenter                  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.IsocenterPosition';
                  %
                  % Table
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopEccentricAngle       = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopEccentricAngle;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopVerticalPosition     = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopVerticalPosition;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopLongitudinalPosition = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopLongitudinalPosition;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopLateralPosition      = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopLateralPosition;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopPitchAngle           = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopPitchAngle;
                  plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).TableTopRollAngle            = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.TableTopRollAngle;
                  %
                  if isfield(EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1,'SourceToSurfaceDistance' )==1
                    plan.(sprintf('Beam_%d',i)).(sprintf('CP_%d',j)).SourceToSurfaceDistance  = EB_plan.BeamSequence.(sprintf('Item_%d',i)).ControlPointSequence.Item_1.SourceToSurfaceDistance;
                  end
                  %
             end
            end
            %
            plan.isocenter = [0,0,0];
           % caculate MCNP TR cards
           for k=1:plan.NBeams
           % check each control point
             for j=1:plan.(sprintf('Beam_%d',k)).NumberOfControlPoints
                 JAW_X      = plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw01;
                 JAW_Y      = plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Jaw03;
                 Gantry     = plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).GantryAngle;
                 Collimator = plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).CollimatorAngle;
                 %
                 % TRCL = o1 o2 o3 xx' yx' zx' xy' yy' zy' xz' yz' zz'
                 %
                 % LINAC HEAD TR1 - Accounts for gantry 
                 TR_1(1:3)   = [0,                 0,             -100]; % reference corrdinate for rotation - isocenter  
                 TR_1(4:6)   = [Gantry,           90,        90+Gantry];
                 TR_1(7:9)   = [90,                0,               90];
                 TR_1(10:12) = [90-Gantry,        90,           Gantry];
                 TR_1(13)    = -1;
                 %
                 % Collimator
                 TR_2(1:3)   = [0,                         0,    0];    % reference corrdinate for rotation - isocenter  
                 TR_2(4:6)   = [Collimator,    90-Collimator,   90];
                 TR_2(7:9)   = [90+Collimator,    Collimator,   90];
                 TR_2(10:12) = [90,                       90,    0];
                 TR_2(13)    = -1;
                 % JAW X  - TR2 and TR3
                 JAW_X =atand(JAW_X/1000);                              % mm  - converting to deg to find rotation
                 % Positive side
                 TR_3(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
                 TR_3(4:6)   = [JAW_X(2),           90,   90-JAW_X(2)];
                 TR_3(7:9)   = [90,                  0,            90];
                 TR_3(10:12) = [90+JAW_X(2),        90,      JAW_X(2)];
                 TR_3(13)    = -1;
                 % Negative side
                 TR_4(1:3)   = [0,                   0,         36.88]; % reference corrdinate for rotation - isocenter  
                 TR_4(4:6)   = [JAW_X(1),           90,   90-JAW_X(1)];
                 TR_4(7:9)   = [90,                  0,            90];
                 TR_4(10:12) = [90+JAW_X(1),        90,      JAW_X(1)];
                 TR_4(13)    = -1;
                 % JAW Y  - TR4 and TR5
                 JAW_Y       = atand(JAW_Y/1000);                       % mm  - converting to deg to find rotation
                 % Positive side
                 TR_5(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
                 TR_5(4:6)   = [0,            90,         90.00];
                 TR_5(7:9)   = [90,     JAW_Y(2),   90-JAW_Y(2)];
                 TR_5(10:12) = [90,  90+JAW_Y(2),      JAW_Y(2)];
                 TR_5(13)    = -1;
                 % Negative side
                 TR_6(1:3)   = [0,             0,         28.16]; % reference corrdinate for rotation - isocenter  
                 TR_6(4:6)   = [0,            90,         90.00];
                 TR_6(7:9)   = [90,     JAW_Y(1),   90-JAW_Y(1)];
                 TR_6(10:12) = [90,  90+JAW_Y(1),      JAW_Y(1)];
                 TR_6(13)    = -1;
                 %
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_1=TR_1;
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_2=TR_2;
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_3=TR_3;
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_4=TR_4;
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_5=TR_5;
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_6=TR_6;
                 %
                 % check if there is a MLC field ...
                 if isfield(plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)),'MLC')==1
                     MLC = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).MLC;
                     for i=1:size(MLC,1)
                     end
                 end   
                 %
                 % SOURCE TR 7
                 TR_7(1:3)   = [0,                 0,            -46.3]; % reference corrdinate for rotation - isocenter  
                 TR_7(4:6)   = [Gantry,           90,        90+Gantry];
                 TR_7(7:9)   = [90,                0,               90];
                 TR_7(10:12) = [90-Gantry,        90,           Gantry];
                 TR_7(13)    = -1;
                 %
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_7 = TR_7;
                 %
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1:3)  = obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).Isocenter([1 3 2]);
                 % different orientation for X
                 plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1)    =-obj.plan.(sprintf('Beam_%d',k)).(sprintf('CP_%d',j)).TR_8(1);
             end
           end
           %
     end
     %


%% Brachy
function plan = read_brachy_file(info_dcm)
     %
     if isfield(info_dcm,'StationName')
       plan.StationName               =  info_dcm.StationName;
     end
     if isfield(info_dcm,'BrachyTreatmentTechnique')
       plan.BrachyTreatmentTechnique  =  info_dcm.BrachyTreatmentTechnique;  
     end
     if isfield(info_dcm,'SourceSequence')
       plan.source                    =  info_dcm.SourceSequence.Item_1; 
       plan.TotalAirKerma             =  info_dcm.SourceSequence.Item_1.ReferenceAirKermaRate;
     end
     if isfield(info_dcm,'TreatmentMachineSequence')
       plan.machine                   =  info_dcm.TreatmentMachineSequence.Item_1;
     end
     if isfield(info_dcm,'ApplicationSetupSequence')
       plan.NCat                      =  size(fieldnames(info_dcm.ApplicationSetupSequence.Item_1.ChannelSequence),1);
     end
     %
     if isfield(info_dcm,'FractionGroupSequence') ==1 && ...
        isfield(info_dcm.FractionGroupSequence.Item_1,'ReferencedBrachyApplicationSetupSequence')==1 && ...
        isfield(info_dcm.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1,'BrachyApplicationSetupDose')==1
        plan.presc_dose             =  info_dcm.FractionGroupSequence.Item_1.ReferencedBrachyApplicationSetupSequence.Item_1.BrachyApplicationSetupDose;
     else
        plan.presc_dose             =  0;
     end
     %
     if isfield(info_dcm,'ApplicationSetupSequence')==1
        plan=get_dwell_pos(plan,info_dcm.ApplicationSetupSequence.Item_1.ChannelSequence);
     end
     %
     if isfield(info_dcm,'Private_300f_1000')==1                % nucletron
        plan=get_cat_pos(plan,info_dcm);
     end
     %
     if isfield(info_dcm,'DoseReferenceSequence')==1 && length(fieldnames(info_dcm.DoseReferenceSequence)) > 1
        for i=1:length(fieldnames(info_dcm.DoseReferenceSequence))
            if isfield(info_dcm.DoseReferenceSequence.(sprintf('Item_%d',i)),'DoseReferencePointCoordinates')==1
                plan.ref_points.(sprintf(info_dcm.DoseReferenceSequence.(sprintf('Item_%d',i)).DoseReferenceDescription))=info_dcm.DoseReferenceSequence.(sprintf('Item_%d',i)).DoseReferencePointCoordinates;
            end
        end 
     end
     %
   end


   %
   function plan = get_dwell_pos(plan,channels)
       ch=fieldnames(channels);
       plan.dwells=[];
       for i=1:size(ch,1)
           ch_number=channels.(sprintf('%s',ch{i})).ChannelNumber;
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelLength               = channels.(sprintf('%s',ch{i})).ChannelLength;
         if isfield(channels.(sprintf('%s',ch{i})),'FinalCumulativeTimeWeight')==1
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).FinalCumulativeTimeWeight   = channels.(sprintf('%s',ch{i})).FinalCumulativeTimeWeight;
         else
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).FinalCumulativeTimeWeight   = 1;  
         end
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelTotalTime            = channels.(sprintf('%s',ch{i})).ChannelTotalTime;
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).SourceApplicatorStepSize    = channels.(sprintf('%s',ch{i})).SourceApplicatorStepSize;
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).SourceApplicatorLength      = channels.(sprintf('%s',ch{i})).SourceApplicatorLength;
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).ChannelLength               = channels.(sprintf('%s',ch{i})).ChannelLength;
           plan.catheter.(sprintf('Cat_%.0f',ch_number)).ReferencedROINumber         = channels.(sprintf('%s',ch{i})).ReferencedROINumber;
           %
           %   
           try   % Nucletron uses a time weight 
             ref_time=channels.(sprintf('%s',ch{i})).ChannelTotalTime/channels.(sprintf('%s',ch{i})).FinalCumulativeTimeWeight;
           catch % Varian does not
             ref_time=1;  
           end
           %
           cum_time=0;
           for j=1:size(fieldnames(channels.(sprintf('%s',ch{i})).BrachyControlPointSequence),1)
               if cum_time<channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight
                 %
                 plan.dwells(end+1,1:3) =  channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).ControlPoint3DPosition;
                 plan.dwells(end,4)     =  (channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight - cum_time)*ref_time;
                 plan.dwells(end,5)     =  i;
                 cum_time               =  channels.(sprintf('%s',ch{i})).BrachyControlPointSequence.(sprintf('Item_%.0f',j)).CumulativeTimeWeight;
               end
           end
       %
       end
   end
   %
   %
   function plan = get_cat_pos(plan,info_dcm)
       cat_inf=info_dcm.Private_300f_1000.Item_1.RTROIObservationsSequence;
       cat_pos=info_dcm.Private_300f_1000.Item_1.ROIContourSequence;
       cat_names=fieldnames(cat_pos);
       cont=1;
       for i=1:length(cat_names)
           if strcmp(cat_inf.(sprintf('Item_%d',i)).RTROIInterpretedType,'BRACHY_CHANNEL')==1
             plan.catheter.(sprintf('Cat_%d',cont)).Points=reshape(cat_pos.(sprintf('%s',cat_names{i})).ContourSequence.Item_1.ContourData,3,[])';
             cont=cont+1;
           end
       end
   end
   %