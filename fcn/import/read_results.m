function [result] = read_results(varargin)
%
if nargin==0
  [file,folder]=uigetfile({'*.dcm;*.mat;*.o'});
  if file==0; result=-1; return; end
else
  folder= [varargin{1}];
  file  =  varargin{2};  
end
%
% DICOM format
if size(file,2)>2 && strcmp(file(end-2:end),'dcm')==1
    result=-99;
    handles    = guihandles(findobj('Name','AMIGO'));
    TPlan      = getappdata(handles.amb_interface,'TPlan');
    %
    File{1,2}  = 'AMB_read_dose_resquest';
    File{1,3}  = 'RTDOSE';
    File{1,36} = dicominfo([folder file]);
    File{1,37} = [folder file];
    File{1,8}  = File{1,36}.SeriesDescription;
    %
    TPlan      = read_dose_files(TPlan,File);
    setappdata(handles.amb_interface,'TPlan',TPlan);
    return;
end
%
% script to merg condor data creates a .mat file
if size(file,2)>2 && strcmp(file(end-2:end),'mat')==1
    load([folder file]);
    result=rmfield(results,'files');
    return;
end
% mcnp repeated structures tally
if strcmp(file(end-1:end),'.o')==1
    result=open_rep_tally(folder,file);
    return;
end
% didn't match the criteria above so assume it is meshtal
% it may fail depeding on the mcnop output format
result=open_meshtal(folder,file);
end





function [result]=open_rep_tally(folder,file)
%%
fid = fopen([folder file],'r');
while ~feof(fid)
    a=fgetl(fid);
    % phantom size size
    if contains(a,'fill ')   == 1
        a(1:strfind(a,'fill ')+4) = [];
        a                         = strrep(a,':',' ');
        dim                       = str2num(a(1:strfind(a,'&')-1));
        phant.Nvoxels(1)          = dim(2)-dim(1)+1;
        phant.Nvoxels(2)          = dim(4)-dim(3)+1;
        phant.Nvoxels(3)          = dim(6)-dim(5)+1;
        result.r1.Dose            = zeros(phant.Nvoxels);
        result.r1.Unc             = zeros(phant.Nvoxels);
        result.r1.tally_number    = 8;
        result.info               = 'MCNP *F8';
        try
           w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
           set(w_bar.hand,'Visible',1,'Value',25);
        catch
           % only works if amigo is open
        end
    end
    %
    % find the position of the first voxel to define resolution
    % and check for shifts
    if contains(a,'c  Lattice') == 1
      a=fgetl(fid);   pos(1,1) = str2num(a(30:end));
      a=fgetl(fid);   pos(1,2) = str2num(a(30:end));
      a=fgetl(fid);   pos(2,1) = str2num(a(30:end));
      a=fgetl(fid);   pos(2,2) = str2num(a(30:end));
      a=fgetl(fid);   pos(3,1) = str2num(a(30:end));
      a=fgetl(fid);   pos(3,2) = str2num(a(30:end));
      result.r1.resolution     = diff(pos')*10;                   % mm
      result.r1.shift          = round(pos(:,1)./diff(pos')')+1;  % voxels
      result.r1.ref            = pos(:,1);
    end
    %
    % read the dose
    if size(a,2)>=7 && strcmp(a(1:7),' cell (')==1
        POS=str2num(a(strfind(a,'[')+1:strfind(a,']')-1))+1;
        d=str2num(fgetl(fid));
        result.r1.Dose(POS(1),POS(2),POS(3))= d(1);
        result.r1.Unc(POS(1),POS(2),POS(3)) = d(2);
    end
    %
end
fclose(fid);
try
  set(w_bar.hand,'Visible',0,'Value',0);
end
%%








end


function [result] = open_meshtal(folder,file)
tic
fid=fopen([folder file],'r');
f=textscan(fid,'%s',3,'Delimiter','\n');
result.nps=str2num(f{1}{3}(54:end));
%
out=0;
while ~feof(fid)
    f=textscan(fid,'%s',1,'Delimiter','\n');
    tally=strfind(f{1},'Tally Number','ForceCellOutput',true);
    if tally{1}>0
        out=out+1;
        result.(sprintf('r%.0f',out)).tally_number=str2num(f{1}{1}(20:end));
    end
    dim=strfind(f{1},'Tally bin boundaries:','ForceCellOutput',true);
    if dim{1}>0
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).x=str2num(f{1}{1}(17:end));
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).y=str2num(f{1}{1}(17:end));
       f=textscan(fid,'%s',1,'Delimiter','\n');
       result.(sprintf('r%.0f',out)).z=str2num(f{1}{1}(17:end));           
    end
    %
    r=strfind(f{1},'Tally Results:','ForceCellOutput',true);
    if r{1}>0
      ori=zeros(3,1);
    %
      if     isempty(strfind(f{1}{1},'X (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).x,2)-1;
         ori(1)=1;
      elseif isempty(strfind(f{1}{1},'Y (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).y,2)-1;
         ori(2)=1;
      elseif isempty(strfind(f{1}{1},'Z (across)'))==0 
         c=size(result.(sprintf('r%.0f',out)).z,2)-1;
         ori(3)=1;
      end
     %
      if     isempty(strfind(f{1}{1},'X (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).x,2)-1;
         ori(1)=1;
      elseif isempty(strfind(f{1}{1},'Y (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).y,2)-1;
         ori(2)=1;
      elseif isempty(strfind(f{1}{1},'Z (down)'))==0 
         l=size(result.(sprintf('r%.0f',out)).z,2)-1;
         ori(3)=1;
      end
      %
      if     ori(1)==0 
         slices=size(result.(sprintf('r%.0f',out)).x,2)-1;
      elseif ori(2)==0 
         slices=size(result.(sprintf('r%.0f',out)).y,2)-1;
      elseif ori(3)==0
         slices=size(result.(sprintf('r%.0f',out)).z,2)-1;
      end
      f=textscan(fid,'%s',1,'Delimiter','\n'); 
      [result.(sprintf('r%.0f',out)).Result,result.(sprintf('r%.0f',out)).Uncert] ...
                                     = read_meshtal(fid,l,c,slices);
    end        
end
%
fclose('all');
toc
end

function [tally,uncer] = read_meshtal(fid,l,c,slices)
%
try
   w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
   set(w_bar.hand,'Visible',1,'Value',10);
catch
   % only works if amigo is open
end

tally=zeros(l,c+1,slices);
uncer=zeros(l,c+1,slices);
for k=1:slices
   tally(:,:,k)=cell2mat(textscan(fid,repmat('%f',[1,c+1]),'CollectOutput',1));
   textscan(fid,'%s',2,'Delimiter','\n');
   uncer(:,:,k)=cell2mat(textscan(fid,repmat('%f',[1,c+1]),'CollectOutput',1));
   if k~=slices
    textscan(fid,'%s',4,'Delimiter','\n');
   end
   try
     set(w_bar.hand,'Value',k/slices*100-5);
   end
end
%
tally(:,1,:)=[];
uncer(:,1,:)=[];
try
  set(w_bar.hand,'Visible',0,'Value',0);
end
end






