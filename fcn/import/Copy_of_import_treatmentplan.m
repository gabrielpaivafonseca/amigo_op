classdef import_treatmentplan_V2 
   properties
      image      = [];
      plan       = [];
      dose       = []; 
      struct     = [];
      parameters = [];
      DECT       = [];
      CT4D       = [];
      Studies    = [];
   end
   methods
      function obj = import_treatmentplan(varargin)
         % user select one DICOM or IMA file within the folder and AMIGO tries to open
         % the whole folder.
         if nargin == 0
            [file,nfolder]=uigetfile({'*.dcm;*.AMB_plan;*.IMA;*.egsphant;*.g4dcm;*.bin;*.inp;*.o'},'Select one file to open the whole folder'); 
            if isnumeric(nfolder)==1; return; end
         else
             nfolder=varargin{1};
         end
         %
         % UPDATE wait bar
         w_bar.hand =getappdata(findobj('Tag','amb_interface'),'waitbarjava');
         set(w_bar.hand,'Visible',1);
         %
         % Check individual files first
         %
         % AMIGO project
         if  size(file,2)>8 && strcmp(file(end-7:end),'AMB_plan')==1
             set(w_bar.hand,'Value',25);
             load([nfolder file],'-mat');
             obj.image                 = TPlan.image;
             obj.plan                  = TPlan.plan;
             obj.dose                  = TPlan.dose;
             obj.struct                = TPlan.struct;
             obj.image.load.ed_all_mat = ed_all_mat;
             obj.image.load.mat_menu   = mat_menu;
             obj.image.load.mat_limits = mat_limits;
             setappdata(gcf,'phantom_region',phantom_pos);
             setappdata(gcf,'score_region',score_pos);
             set(w_bar.hand,'Value',75);
             if isfield(TPlan.dose,'Dose_TPS')==1
                 obj.dose.doses.Dose_TPS=TPlan.dose.Dose_TPS;
                 obj.dose=rmfield(obj.dose,'Dose_TPS');
             end
             return;
         %
         % EGSPhant - material and density map used by some MC codes
         elseif size(file,2)>8 && strcmp(file(end-7:end),'egsphant')
             set(w_bar.hand,'Value',25);
             obj=import_egsphant(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % G4DCM - material and density map used by some MC codes
         elseif size(file,2)>5 && strcmp(file(end-4:end),'g4dcm')
             set(w_bar.hand,'Value',25);
             obj=import_g4dcm(obj,nfolder);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % MOBY phantom    
         elseif strcmp(file(end-2:end),'bin')         % Moby
             set(w_bar.hand,'Value',25);
             obj=import_bin(obj,[nfolder file]);
             set(w_bar.hand,'Value',75);
             obj.struct.contours.(sprintf('Item_%.0f',1)).Name='Edit';
             obj.struct.contours.(sprintf('Item_%.0f',1)).Mask=logical(zeros(obj.image.Nvoxels));
             return;
         % MCNP input or output
         elseif (size(file,2)>2 && strcmp(file(end-1:end),'.o')==1) || ...
            (size(file,2)>8 && strcmp(file(end-3:end),'.inp')==1)
         %
            set(w_bar.hand,'Value',25);
            obj=import_mcinp(obj,[nfolder file]);
            set(w_bar.hand,'Value',75);
            obj.struct.contours.(sprintf('Item_%.0f',1)).Name = 'Edit';
            obj.struct.contours.(sprintf('Item_%.0f',1)).Mask = logical(zeros(obj.image.Nvoxels));
            return;  
         end
        % -------------------------------------------------------------------------------------------
        %%  Check the content of the folder (DCM, Series, RTStruct, RTPlan, etc)
                 Files = check_nfolder(nfolder);
        %        Check the content of the folder (DCM, Series, RTStruct, RTPlan,
        %        etc)  
        % ------------------------------------------------------------------------------------------
        %% -----------------------------------------------------------------------------------------
        
        %   Check how many studies and series 
        PT      = unique(Files(:,2));         % number of patients
        %
        for k =1:size(PT,1) % 
            % 
            % get only files related within the same study
            st  = {}; 
            IDX = find(ismember(Files(:,2),PT{k})); 
            st  = Files(IDX,:);
            % add additional "for" in case of multiple series
            % SE  = unique(cell2mat(Files(:,5))); % number of series per study
            %
            % Check modality and open CT images
            IDX = find(ismember(st(:,3),'CT'));
            %
            if isempty(IDX)==1; continue; end 
            %  
            for i = 1:length(IDX)
                if i==1
                  obj.Studies.(sprintf(['P' PT{k}])).image.SliceThickness            = st{i,31};
                  obj.Studies.(sprintf(['P' PT{k}])).image.SpacingBetweenSlices      = st{i,32};
                  obj.Studies.(sprintf(['P' PT{k}])).image.KVP                       = st{i,22};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ReconstructionDiameter    = st{i,26};     
                  obj.Studies.(sprintf(['P' PT{k}])).image.XrayTubeCurrent           = st{i,23};
                  obj.Studies.(sprintf(['P' PT{k}])).image.FilterType                = st{i,24};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ConvolutionKernel         = st{i,25};
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient      = st{i,6}';
                  % Find min (z)
                  z = cell2mat(Files(:,6)); 
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient(3)   = min(z(3:3:end));
                  %
                  obj.Studies.(sprintf(['P' PT{k}])).image.ImageOrientationPatient   = st{i,21}';  
                  obj.Studies.(sprintf(['P' PT{k}])).image.TableSpeed                = st{i,16};
                  obj.Studies.(sprintf(['P' PT{k}])).image.SpiralPitchFactor         = st{i,29};
                  obj.Studies.(sprintf(['P' PT{k}])).image.RescaleSlope              = st{i,19};
                  obj.Studies.(sprintf(['P' PT{k}])).image.RescaleIntercept          = st{i,18};
                  obj.Studies.(sprintf(['P' PT{k}])).image.Nvoxels                   = [st{i,17}  st{i,16} length(IDX)];
                  obj.Studies.(sprintf(['P' PT{k}])).image.Resolution                = [st{i,30}' st{i,31}];
                end
                % Read images
                % if instance number is missing calculate it
                %
                %
                if isempty(st{i,20})==1 || st{i,20}==0
                  Int_pos     = st{i,6}'-obj.Studies.(sprintf(['P' PT{k}])).image.ImagePositionPatient;
                  st{i,20} = round(Int_pos(3) / obj.Studies.(sprintf(['P' PT{k}])).image.Resolution(3)) + 1;
                end
                obj.Studies.(sprintf(['P' PT{k}])).image.Images(:,:,st{i,20})   = dicomread([nfolder Files{i,1}])/Files{i,19}+Files{i,18};
            end 
            %
            % Some images needs to be flipped depending on the manufacturer 
            Pos         = reshape(cell2mat(st(:,6)),3,[])';
            [~,IDX_max] = max(Pos(:,3));
            [~,IDX_min] = min(Pos(:,3));
            %
            if st{IDX_min,20} > st{IDX_max,20}
                obj.Studies.(sprintf(['P' PT{k}])).image.Images = flip(obj.Studies.(sprintf(['P' PT{k}])).image.Images,3);
                disp('Image was flipped to match coordinate system');
            end
            %
        end
        %% ---------------------------------------------------------------------------------------------------------------
        % Read struct files
        for k =1:size(PT,1) % 
            % 
            % get only files related within the same study
            st  = {}; 
            IDX = find(ismember(Files(:,2),PT{k})); 
            st  = Files(IDX,:);
            % add additional "for" in case of multiple series
            % SE  = unique(cell2mat(Files(:,5))); % number of series per study
            %
            % Check modality and open STRUCT images
            IDX = find(ismember(st(:,3),'RTSTRUCT'));
            %
            if isempty(IDX)==1; continue; end 
            %  
            for i = 1:length(IDX)
            end
        end
        % 
      end

         
      % ------------------------------------------------------------------------------------------------------------------------
      % ------------------------------------------------------------------------------------------------------------------------
      function rts = read_struct_dcm(rts,info_dcm)
         % are there structures ? 
         if isfield(info_dcm,'StructureSetROISequence')==0; warndlg('No contours available in the RTStruct file'); return; end
         %
         if isfield(info_dcm,'RTROIObservationsSequence')==1 
             cont=fieldnames(info_dcm.RTROIObservationsSequence);
             for i=1:size(cont,1)
                 %
                 if isfield(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})),'Private_3007_10xx_Creator') == 1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'ORGAN')==1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'CTV')==1 ...
                     || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'EXTERNAL')==1 ... % nucletron
                     || strcmp(info_dcm.Manufacturer,'Nucletron')==1  % nucletron
                     %
                     rts=import_struct_nuc_var(rts,info_dcm,i,cont);
                     %
                 elseif strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType,'BRACHY_CHANNEL')==1
                     % varian
                     rts=import_cat_cont_varian(rts,info_dcm,i,cont);
                     %
                 else
                     info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{i})).RTROIInterpretedType;  
                     rts=import_struct_nuc_var(obj,info_dcm,i,cont);
                 end
                 %
             end
         else
             cont=fieldnames(info_dcm.ROIContourSequence);
             for i=1:size(cont,1)
                 %
                 if strcmp(info_dcm.ROIContourSequence.(sprintf('Item_%d',i)).ContourSequence.Item_1.ContourGeometricType,'CLOSED_PLANAR')==1  
                     %
                     obj=import_struct_nuc_var(obj,info_dcm,i,cont);
                 else
                     try
                       % varian
                        obj=import_cat_cont_varian(obj,info_dcm,i,cont);
                       %
                     catch
                         disp('Structure misidentified as a catheter is the RS file - Warning'); 
                     end
                 end
                 %
             end    
         end
         %
       end
       %
       function obj = import_cat_cont_varian(obj,info_dcm,i,cont)
        % if isfield(obj,'plan')==1 && isfield(obj.plan,'catheter')==1
              c_lis=fieldnames(obj.plan.catheter);
              for j=1:length(c_lis)
                if  obj.plan.catheter.(sprintf('%s',c_lis{j})).ReferencedROINumber == ...
                        info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ReferencedROINumber
                   %
                   obj.plan.catheter.(sprintf('%s',c_lis{j})).Points=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.Item_1.ContourData,3,[])';
                   return;
                end
              end
       end
       %
       function obj=import_struct_nuc_var(obj,info_dcm,i,cont)
         if isfield(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)),'ContourSequence')==0
             return;
         end
         items=fieldnames(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence);
         %
         if isempty(obj.struct)==1
             con=1;
         else
             con=length(fieldnames(obj.struct.contours))+1;
         end
         %
         obj.struct.contours.(sprintf('Item_%.0f',con)).Name      = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIName;
         obj.struct.contours.(sprintf('Item_%.0f',con)).Algorithm = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIGenerationAlgorithm;  
         obj.struct.contours.(sprintf('Item_%.0f',con)).Points=[];
         for j=1:size(items)
             if j==1
                 obj.struct.contours.(sprintf('Item_%.0f',con)).Type = ...
                     info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourGeometricType;
             end
              POINTS=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourData,3,[])';
              obj.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1,1:3)=[9999 9999 9999];
              obj.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1:end+size(POINTS,1),:)=POINTS;
         end
             %
       end     
       %
       function obj = create_contour_masks(obj)
         if isfield(obj.struct,'contours')==0; return; end
         h = warndlg({'AMIGO will import all the contours and create masks',...
                      'To improve speed during the execution ',...
                      'It may take few minutes depending on the number of contours'},'Contours','replace');
         items=fieldnames(obj.struct.contours);
         for i=1:size(items,1) 
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@minus,obj.struct.contours.(sprintf('%s',items{i})).Points, ...
                                                                                obj.image.ImagePositionPatient);
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@rdivide,obj.struct.contours.(sprintf('%s',items{i})).PointsIm, ...
                                                                                obj.image.Resolution);   
           obj.struct.contours.(sprintf('%s',items{i})).Mask=zeros(obj.image.Nvoxels); 
           %
           [r,~]=find(obj.struct.contours.(sprintf('%s',items{i})).Points(:,3)==9999);
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,1)=9999;
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,2)=9999;
           obj.struct.contours.(sprintf('%s',items{i})).PointsIm(r,3)=9999;
           %
           %
           P=obj.struct.contours.(sprintf('%s',items{i})).PointsIm;
           for j=1:size(r,1)
             if j<size(r,1)  
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:r(j+1)-1,:); 
             elseif r(j)<size(P,1)
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:end,:);
             else 
              break   
             end
             mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(obj.image.Nvoxels(1)),double(obj.image.Nvoxels(2)));
             obj.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=obj.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)+mask;
           end           
            obj.struct.contours.(sprintf('%s',items{i})).Mask=logical(obj.struct.contours.(sprintf('%s',items{i})).Mask);
         end
         %
         if isvalid(h)==1; close(h); end
         %
       end   
      
      
      
      
      
      
      
      
      
      % -----------------------------------------------------------------------------------------------------------------------
      % -----------------------------------------------------------------------------------------------------------------------
        % check all files in the folder ... dicominfo ... returns a table with
        % relevant information
        function Files = check_nfolder(nfolder)
             list=dir([nfolder '*.dcm']);  
             %
             Files=cell(length(list),36);
             %
             for i=1:length(list)
                 info = dicominfo([nfolder list(i).name]);
                 %
                 Files{i,1}  = [list(i).name];
                 %
                 if isfield(info,'PatientID')==1;                 Files{i,2}  = info.PatientID;                                                   end
                 if isfield(info,'Modality')==1;                  Files{i,3}  = info.Modality;                                                    end
                 if isfield(info,'StudyID')==1                    Files{i,4}  = info.StudyID;                                                     end
                 if isfield(info,'SeriesNumber')==1               Files{i,5}  = info.SeriesNumber;                                                end
                 if isfield(info,'ImagePositionPatient')==1;      Files{i,6}  = info.ImagePositionPatient;                                        end
                 if isfield(info,'StudyDescription')==1;          Files{i,7}  = info.StudyDescription;                                            end
                 if isfield(info,'SeriesDescription')==1;         Files{i,8}  = info.SeriesDescription;                                           end              
                 if isfield(info,'SeriesInstanceUID') == 1        Files{i,9}  = info.SeriesInstanceUID;                                           end 
                 if isfield(info,'StudyInstanceUID')  == 1        Files{i,10} = info.StudyInstanceUID;                                            end 
                 if isfield(info,'SOPClassUID')==1;               Files{i,11} = info.SOPClassUID;                                                 end
                 if isfield(info,'MediaStorageSOPInstanceUID')==1 Files{i,12} = info.MediaStorageSOPInstanceUID;                                  end
                 if isfield(info,'FrameOfReferenceUID')==1        Files{i,13} = info.FrameOfReferenceUID;                                         end
                 if isfield(info,'Manufacturer')==1;              Files{i,14} = info.Manufacturer;                                                end
                 % Image info
                 if isfield(info,'SliceThickness')==1;            Files{i,15} = info.SliceThickness;                                              end
                 if isfield(info,'Width')==1;                     Files{i,16} = info.Width;                                                       end
                 if isfield(info,'Height')==1;                    Files{i,17} = info.Height;                                                      end
                 if isfield(info,'RescaleIntercept')==1;          Files{i,18} = info.RescaleIntercept;                                            end
                 if isfield(info,'RescaleSlope')==1;              Files{i,19} = info.RescaleSlope;                                                end
                 if isfield(info,'InstanceNumber')==1;            Files{i,20} = info.InstanceNumber;                                              end
                 if isfield(info,'ImageOrientationPatient')==1;   Files{i,21} = info.ImageOrientationPatient;                                     end
                 if isfield(info,'KVP')==1;                       Files{i,22} = info.KVP;                                                         end
                 if isfield(info,'XrayTubeCurrent')==1;           Files{i,23} = info.XrayTubeCurrent;                                             end
                 if isfield(info,'FilterType')==1;                Files{i,24} = info.FilterType;                                                  end
                 if isfield(info,'ConvolutionKernel')==1;         Files{i,25} = info.ConvolutionKernel;                                           end
                 if isfield(info,'ReconstructionDiameter')==1;    Files{i,26} = info.ReconstructionDiameter;                                      end
                 if isfield(info,'PatientPosition')==1;           Files{i,27} = info.PatientPosition;                                             end
                 if isfield(info,'TableSpeed')==1;                Files{i,28} = info.TableSpeed;                                                  end
                 if isfield(info,'SpiralPitchFactor')==1;         Files{i,29} = info.SpiralPitchFactor;                                           end
                 %
                 if isfield(info,'PixelSpacing')==1;              Files{i,30} = info.PixelSpacing;                                                end
                 if isfield(info,'SliceThickness')==1;            Files{i,31} = info.SliceThickness;                                              end
                 if isfield(info,'SpacingBetweenSlices')==1;      Files{i,32} = info.SpacingBetweenSlices;                                        end
                 if isfield(info,'FractionGroupSequence')==1      Files{i,33} = info.FractionGroupSequence.Item_1.NumberOfFractionsPlanned;       end
                 %
                 if isfield(info,'DoseSummationType') == 1        Files{i,34} = info.DoseSummationType;                                           end
                 if isfield(info,'NumberOfFrames') == 1           Files{i,35} = info.NumberOfFrames;                                              end
                 %
                 % store plan info to process later ... no need to read info 2x
                 if strcmpi(Files{i,3},'RTPlan') ==1             Files{i,36}  = info;                                                             end
                 % store struct info to process later ... no need to read info 2x
                 if strcmpi(Files{i,3},'RTStruct') ==1           Files{i,36}  = info;                                                             end
                 %
             end    
        end