function read_additional_struct_dcm
     [File,Folder] = uigetfile('*.dcm');
     info_dcm=dicominfo([Folder File]);
     %
     handles = guihandles;
     TPlan   = getappdata(handles.amb_interface,'TPlan');
     %
     %
     if isfield(info_dcm,'StructureSetROISequence')==0; return; end
     %
       %
         % replace previous or add
         answer = questdlg('Would you like to replace or add nex contours?', ...
	                       'Import contour', ...
	                       'Add','Replace','Add');
         % Handle response
          switch answer
              case 'Replace'
               TPlan.struct.contours=[];
          end
         %
         % 
     %
     if isfield(info_dcm,'RTROIObservationsSequence')==1 
         %
         cont=fieldnames(info_dcm.RTROIObservationsSequence);
         %
         if isempty(TPlan.struct.contours)==1
             l(1)=1; l(2)=size(cont,1);
         else
             l(1) = size(fieldnames(TPlan.struct.contours),1)+1;
             l(2) = size(fieldnames(TPlan.struct.contours),1)+size(cont,1);
         end
         %
         c=1;
         %
         for i=l(1):l(2)
             %
             if isfield(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})),'Private_3007_10xx_Creator') == 1 ...
                 || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})).RTROIInterpretedType,'ORGAN')==1 ...
                 || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})).RTROIInterpretedType,'CTV')==1 ...
                 || strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})).RTROIInterpretedType,'EXTERNAL')==1 ... % nucletron
                 || strcmp(info_dcm.Manufacturer,'Nucletron')==1  % nucletron
                 %
                 TPlan=import_struct_nuc_var(TPlan,info_dcm,c,cont);
                 %
             elseif strcmp(info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})).RTROIInterpretedType,'BRACHY_CHANNEL')==1
                 % catheter
             else
                 info_dcm.RTROIObservationsSequence.(sprintf('%s',cont{c})).RTROIInterpretedType;  
               % TPlan=inport_struct_catheter_varian(TPlan,info_dcm,i,cont); 
                 TPlan=import_struct_nuc_var(TPlan,info_dcm,c,cont);
             end
             %
             c=c+1;
         end
     else
         cont=fieldnames(info_dcm.ROIContourSequence);
         %
         if isempty(TPlan.struct)==1
             l(1)=1; l(2)=size(cont,1);
         else
             l(1) = size(fieldnames(TPlan.struct.contours),1)+1;
             l(2) = size(fieldnames(TPlan.struct.contours),1)+1+size(cont,1);
         end
         %
         c=1;
         for i=l(1):l(2)
             %
             if strcmp(info_dcm.ROIContourSequence.(sprintf('Item_%d',c)).ContourSequence.Item_1.ContourGeometricType,'CLOSED_PLANAR')==1  
                 %
                 TPlan=import_struct_nuc_var(TPlan,info_dcm,c,cont);
             else
                 try
                   % varian
                    TPlan=import_cat_cont_varian(TPlan,info_dcm,c,cont,i);
                   %
                 catch
                     disp('Structure misidentified as a catheter is the RS file - Warning'); 
                 end
             end
             %
             c=c+1;
         end    
     end
     %
     TPlan = create_contour_masks(TPlan);
     setappdata(handles.amb_interface,'TPlan',TPlan);
     %
     % contour list
    if isfield(TPlan.struct,'contours')==1
        str={};
        c_names=fieldnames(TPlan.struct.contours);
        for i=1:length(c_names)
           str{end+1}=['<html><font color="black" size="6">' TPlan.struct.contours.(sprintf('%s',c_names{i})).Name '</font></html>'];
        end
        set(handles.cont_pop_menu,'String',str','Value',1,'Visible','on');
        set(handles.cont_pop_menu,'Userdata',zeros(length(c_names),4));
    else
        set(handles.cont_pop_menu,'String','No contours','Value',1,'Visible','on');
    end 
    %
   end
   %
   %
   function TPlan=import_struct_nuc_var(TPlan,info_dcm,i,cont)
     if isfield(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)),'ContourSequence')==0
         return;
     end
     items=fieldnames(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence);
     %
     if isempty(TPlan.struct.contours)==1
         con=1;
     else
         con=length(fieldnames(TPlan.struct.contours))+1;
     end
     %
     TPlan.struct.contours.(sprintf('Item_%.0f',con)).Name      = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIName;
     TPlan.struct.contours.(sprintf('Item_%.0f',con)).Algorithm = info_dcm.StructureSetROISequence.(sprintf('%s',cont{i})).ROIGenerationAlgorithm;  
     TPlan.struct.contours.(sprintf('Item_%.0f',con)).Points=[];
     for j=1:size(items)
         if j==1
             TPlan.struct.contours.(sprintf('Item_%.0f',con)).Type = ...
                 info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourGeometricType;
         end
          POINTS=reshape(info_dcm.ROIContourSequence.(sprintf('Item_%.0f',i)).ContourSequence.(sprintf('%s',items{j})).ContourData,3,[])';
          TPlan.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1,1:3)=[9999 9999 9999];
          TPlan.struct.contours.(sprintf('Item_%.0f',con)).Points(end+1:end+size(POINTS,1),:)=POINTS;
     end
         %
   end     
   %
   function TPlan = create_contour_masks(TPlan)
     if isfield(TPlan.struct,'contours')==0; return; end
     h = warndlg({'AMIGO will import all the contours and create masks',...
                  'To improve speed during the execution ',...
                  'It may take few minutes depending on the number of contours'},'Contours','replace');
     items=fieldnames(TPlan.struct.contours);
     %
     
     %
     for i=1:size(items,1) 
       if   isfield(TPlan.struct.contours.(sprintf('%s',items{i})),'Points')==1
           TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@minus,TPlan.struct.contours.(sprintf('%s',items{i})).Points, ...
                                                                                TPlan.image.ImagePositionPatient);
           TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm=bsxfun(@rdivide,TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm, ...
                                                                                TPlan.image.Resolution);   
           TPlan.struct.contours.(sprintf('%s',items{i})).Mask=zeros(TPlan.image.Nvoxels); 
           %
           [r,~]=find(TPlan.struct.contours.(sprintf('%s',items{i})).Points(:,3)==9999);
           TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm(r,1)=9999;
           TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm(r,2)=9999;
           TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm(r,3)=9999;
           %
           %
           P=TPlan.struct.contours.(sprintf('%s',items{i})).PointsIm;
           for j=1:size(r,1)
             if j<size(r,1)  
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:r(j+1)-1,:); 
             elseif r(j)<size(P,1)
              C_POINTS=[]; C_POINTS(:,1:3)=P(r(j)+1:end,:); 
             else 
              break   
             end
             mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
             TPlan.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=TPlan.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)+mask;
           end
    %            while isempty(P)==0
    %                C_POINTS=[]; C_POINTS(:,1:3)=P(P(:,3)==P(1,3),:); P(P(:,3)==P(1,3),:)=[];
    %                mask=poly2mask(C_POINTS(:,1)+1,C_POINTS(:,2)+1,double(TPlan.image.Nvoxels(1)),double(TPlan.image.Nvoxels(2)));
    %                TPlan.struct.contours.(sprintf('%s',items{i})).Mask(:,:,round(C_POINTS(1,3))+1)=mask;
    %            end
            TPlan.struct.contours.(sprintf('%s',items{i})).Mask=logical(TPlan.struct.contours.(sprintf('%s',items{i})).Mask);
    %            %
           % h = warndlg(['Name: ' TPlan.struct.contours.(sprintf('%s',items{i})).Name '  Contour ' num2str(i) ' of ' num2str(size(items,1))],'Contours','replace');
           %
       end
     end
     %
     if isvalid(h)==1; close(h); end
     %
   end  