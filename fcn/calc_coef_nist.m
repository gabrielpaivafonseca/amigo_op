function [Coef_NIST] = calc_coef_nist(Ele,Frac,Coef_NIST)
% Adipose tissue Nist
% Ele   = [1 6 7 8 11 16 17];
% Frac  = [0.114 0.598 0.007 0.278 0.001 0.001 0.001];
%
% Adipose tissue Ana
% Ele   = [1 6 7 8 11 16 17];
% Frac  = [0.116 0.681 0.002 0.198 0.001 0.001 0.001];
%
if size(Ele,2)~=size(Frac,2)
    disp('Vectors do not match');
    return;
end
% normalized ?
if sum(Frac)~=1
    disp('Comp. not normalized --- check the values')
    Frac=Frac/sum(Frac);
end
%
%
% Average coeficients
Coef_NIST.Coef=zeros(size(Coef_NIST.Int.(sprintf('Z%d',Ele(1)))));
for i=1:size(Ele,2)
    if i==1
      Coef_NIST.Coef(:,1)=Coef_NIST.Int.(sprintf('Z%d',Ele(i)))(:,1);
    end
    Coef_NIST.Coef(:,2)=Coef_NIST.Coef(:,2)+Coef_NIST.Int.(sprintf('Z%d',Ele(i)))(:,2)*Frac(i);
    Coef_NIST.Coef(:,3)=Coef_NIST.Coef(:,3)+Coef_NIST.Int.(sprintf('Z%d',Ele(i)))(:,3)*Frac(i); 
end
%
%
end

%